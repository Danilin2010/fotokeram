<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 05.03.2019
 * Time: 22:04
 */
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_WITH_ON_AFTER_EPILOG', true);
define('BX_NO_ACCELERATOR_RESET', true);
define('STOP_STATISTICS', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application;
use \local\Services\PhotoCart;

$request = Application::getInstance()->getContext()->getRequest();
$res=[];
if($request->isPost()){
    $id=$request->getPost("id");
    if($id>0){
        $res['success']=PhotoCart::chekPhoto($id);
        PhotoCart::delPhoto($id);
    }
}
$res['count']=count(PhotoCart::getPhoto());
echo json_encode($res);