<?php

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_WITH_ON_AFTER_EPILOG', true);
define('BX_NO_ACCELERATOR_RESET', true);
define('STOP_STATISTICS', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application;
use local\Services\CountMedia;

$request = Application::getInstance()->getContext()->getRequest();

$res=[];

if($request->isPost()){
    $id=$request->getPost("id");
    if($id>0)
        CountMedia::setCountViewElement($id);
}
$res['success']='Y';
echo json_encode($res);