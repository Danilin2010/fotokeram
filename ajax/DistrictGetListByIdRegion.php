<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 17.02.2019
 * Time: 2:24
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_WITH_ON_AFTER_EPILOG', true);
define('BX_NO_ACCELERATOR_RESET', true);
define('STOP_STATISTICS', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application;
use \local\Domain\Repository\DistrictRepository;


$request = Application::getInstance()->getContext()->getRequest();

$result=[];

if($request->isPost()){
    $parentId=$request->getPost("parentId");
    if(strlen($parentId)>0){
        $DistrictRepository=new DistrictRepository();
        $res=$DistrictRepository->getListByIdRegion($parentId);
        foreach ($res as $r)
            $result[]=$r->toArray();
    }
}

echo json_encode($result);