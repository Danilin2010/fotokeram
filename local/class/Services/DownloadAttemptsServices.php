<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 07.07.2019
 * Time: 3:12
 */

namespace local\Services;

use local\Domain\Repository\DownloadAttemptsRepository;
use local\Domain\Entity\DownloadAttempts;
use DateTime;
use \Bitrix\Main\Type;

class DownloadAttemptsServices
{
    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function addAttempts(){
        global $USER;
        $UserId=$USER->GetID();
        if($UserId>0){
            $DownloadAttemptsRepository=new DownloadAttemptsRepository();
            $DownloadAttempts=new DownloadAttempts();
            $DownloadAttempts->setUser($UserId);
            $DownloadAttempts->setDate(new DateTime());
            $DownloadAttemptsRepository->Add($DownloadAttempts);
        }
    }

    /**
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     * @throws \Exception
     */
    public static function chekAttempts(){
        global $USER;
        $UserId=$USER->GetID();
        $DateTime=new Type\DateTime();
        $DateTime->add("-1 day");
        if($UserId>0){
            $DownloadAttemptsRepository=new DownloadAttemptsRepository();
            $res=$DownloadAttemptsRepository->getList([],[
                '>=UF_DATE'=>$DateTime,
            ]);
            //print_r(count($res));
            if(count($res)>5)
                return false;
        }
        return true;
    }

}