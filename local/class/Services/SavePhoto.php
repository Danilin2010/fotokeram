<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 07.07.2019
 * Time: 4:45
 */

namespace local\Services;

use local\Domain\Repository\PhotoRepository;
use local\Domain\Entity\Photo;
use Bitrix\Main\Application;
use Bitrix\Main\IO;


class SavePhoto
{

    /**
     * @throws \Bitrix\Main\LoaderException
     * @throws \Exception
     */
    static public function addAllPhoto(){
        $RepositoryPhoto=new PhotoRepository();
        $list=$RepositoryPhoto->getList();
        foreach ($list as $element)
            self::savePhoto($element);
    }

    /**
     * @param $id
     * @throws \Bitrix\Main\LoaderException
     * @throws \Exception
     */
    static public function addPhoto($id){

        $RepositoryPhoto=new PhotoRepository();

        $element=$RepositoryPhoto->getById($id);

        self::savePhoto($element);

    }

    /**
     * @param Photo $element
     */
    static private function savePhoto($element){

        $photo=$element->getPhoto();

        $ext=pathinfo($photo->getName(), PATHINFO_EXTENSION);
        $href=$photo->getPath();

        $ObjectList=$element->getObject();

        foreach ($ObjectList as $Object){

            if($Object->getYear())
                $year=$Object->getYear()->getValue();
            else
                $year="";

            $name=$Object->getName();
            $region=$Object->getAddress()->getRegion();
            foreach ($Object->getBrick() as $Brick){
                $vendor=$Brick->getVendor();
                $brand=$Brick->getBrand()->getName();

                $url=Application::getDocumentRoot()."/upload/sourse/{$brand}_{$name}/{$vendor}_{$region}_{$year}.{$element->getPhotoId()}.{$ext}";
                $fileDir = IO\Path::getDirectory($url);

                $dir = new IO\Directory($fileDir);
                if(!$dir->isExists())
                    IO\Directory::createDirectory($fileDir);
                \copy($href, $url);
            }
        }
    }

    /**
     * @param $id
     * @throws \Bitrix\Main\LoaderException
     * @throws \Exception
     */
    static public function deletePhoto($id){

        $RepositoryPhoto=new PhotoRepository();

        $element=$RepositoryPhoto->getById($id);

        $photo=$element->getPhoto();

        $ext=pathinfo($photo->getName(), PATHINFO_EXTENSION);
        $href=$photo->getPath();

        $ObjectList=$element->getObject();

        foreach ($ObjectList as $Object){
            if($Object->getYear())
                $year=$Object->getYear()->getValue();
            else
                $year="";
            $name=$Object->getName();
            $region=$Object->getAddress()->getRegion();
            foreach ($Object->getBrick() as $Brick){
                $vendor=$Brick->getVendor();
                $brand=$Brick->getBrand()->getName();

                $url=Application::getDocumentRoot()."/upload/sourse/{$brand}_{$name}/{$vendor}_{$region}_{$year}.{$element->getPhotoId()}.{$ext}";

                $File=new IO\File($url);
                $File->delete();
            }
        }
    }

}