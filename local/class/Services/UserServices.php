<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 07.04.2019
 * Time: 22:50
 */

namespace local\Services;

use CUser;

class UserServices
{
    function OnBeforeUserUpdateHandler(&$arFields)
    {
        $arFields["UF_WATERMARK"]["del"]="";
    }

    public static function getUserProp(){
        $res=[];
        global $USER;
        $UserId=$USER->GetID();
        if($UserId<=0)
            return $res;
        $rsUser = CUser::GetByID($UserId);
        if($arUser = $rsUser->Fetch()){

            $res["COMPANY"]=$arUser["WORK_COMPANY"];

            $name=[];

            if($arUser["LAST_NAME"])
                $name[]=$arUser["LAST_NAME"];

            if($arUser["NAME"])
                $name[]=$arUser["NAME"];

            if($arUser["SECOND_NAME"])
                $name[]=$arUser["SECOND_NAME"];

            if(count($name)<=0)
                $name[]=$arUser["LOGIN"];

            $res["NAME"]=implode(' ',$name);

        }

        if($USER->IsAdmin())
            $res["ROLE"]='Администратор';
        elseif(USE_USER_GROUP)
            $res["ROLE"]='Редактор';
        else
            $res["ROLE"]='Пользователь';

        return $res;

    }
}