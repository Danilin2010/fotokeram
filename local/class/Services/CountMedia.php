<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 18.07.2019
 * Time: 1:59
 */

namespace local\Services;

use local\Domain\Repository\ObjectRepository;
use local\Domain\Repository\PhotoRepository;

class CountMedia
{
    /**
     * @param $id
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public static function setCountViewElement($id){
        $PhotoRepository=new PhotoRepository();
        $ObjectRepository=new ObjectRepository();
        $list=$PhotoRepository->getList([],[
            "ID"=>$id,
            'ACTIVE'=>'Y',
            "!object_xmlid"=>false,
        ]);
        foreach ($list as $el){
            $count=(int)$el->getViewCount();
            $count++;
            $el->setViewCount($count);
            $PhotoRepository->Update($el->getId(),$el);
            $object_list=$el->getObject();
            foreach ($object_list as $object){
                $object_count=(int)$object->getViewCount();
                $object_count++;
                $object->setViewCount($object_count);
                $ObjectRepository->Update($object->getId(),$object);
            }
        }
    }

    /**
     * @return int
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getObject(){
        $ObjectRepository=new ObjectRepository();
        $Count=$ObjectRepository->getCount([
            'ACTIVE'=>'Y'
        ]);
        return $Count;
    }

    /**
     * @return int
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getPhoto(){
        $PhotoRepository=new PhotoRepository();
        $Count=$PhotoRepository->getCount([
            'ACTIVE'=>'Y',
            "!object_xmlid"=>false,
        ]);
        return $Count;
    }

}