<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 05.03.2019
 * Time: 0:47
 */

namespace local\Services;

use local\Domain\Repository\PhotoRepository;
use local\Helpers\Watermark;
use local\Services\DownloadAttemptsServices;

class PhotoCart
{

    /**
     * @return array
     */
    private static function _getPhoto(){
        if(isset($_SESSION["addPhoto"]))
            $addPhoto = $_SESSION["addPhoto"];
        else
            $addPhoto = [];
        return $addPhoto;
    }

    /**
     * @param array $addPhoto
     */
    private static function _setPhoto($addPhoto){
        $_SESSION["addPhoto"]=$addPhoto;
    }

    /**
     * @param int $id
     */
    public static function addPhoto($id){
        $addPhoto=self::_getPhoto();
        if(!in_array($id,$addPhoto) && count($addPhoto)<5)
            $addPhoto[]=$id;
        self::_setPhoto($addPhoto);
    }

    /**
     * @param $id
     * @return bool
     */
    public static function chekPhoto($id){
        $addPhoto=self::_getPhoto();
        if(in_array($id,$addPhoto))
            return true;
        if(!in_array($id,$addPhoto) && count($addPhoto)<5)
            return true;
        return false;
    }

    /**
     * @param int $id
     */
    public static function delPhoto($id){
        $addPhoto=self::_getPhoto();
        $key = array_search($id, $addPhoto);
        if($key!==false)
            unset($addPhoto[$key]);
        self::_setPhoto($addPhoto);
    }

    /**
     *
     */
    public static function clearPhoto(){
        self::_setPhoto([]);
    }

    /**
     * @return array
     */
    public static function getPhoto(){
        return self::_getPhoto();
    }

    /**
     * @throws \Bitrix\Main\LoaderException
     * @throws \Exception
     */
    public static function sendEmail(){
        $io = \CBXVirtualIo::GetInstance();

        global $USER;
        $UserId=$USER->GetID();

        DownloadAttemptsServices::addAttempts();
        $Photo=self::_getPhoto();
        if(count($Photo)>0){
            $newPhoto=[];
            $RepositoryPhoto=new PhotoRepository();
            $elements=$RepositoryPhoto->getList([],[
                "id"=>$Photo,
            ]);
            $Watermark=Watermark::GetWatermark($UserId);
            foreach ($elements as $p){
                $photoId=$p->getPhotoId();

                $arFileTmp = \CFile::ResizeImageGet(
                    $photoId,
                    //array("width"=>$CFile["WIDTH"]-100,"height"=>$CFile["HEIGHT"]-100),
                    array("width"=>0,"height"=>0),
                    BX_RESIZE_IMAGE_EXACT,
                    true,$Watermark
                );

                $url=$arFileTmp["src"];
                if($io->FileExists($_SERVER['DOCUMENT_ROOT'].$url))
                    $newPhoto[]=$_SERVER['DOCUMENT_ROOT'].$url;

            }


            $EMAIL_TO=$USER->GetEmail();

            if(strlen($EMAIL_TO)>0){
                $arEventFields = array(
                    "EMAIL_TO"=>$EMAIL_TO,
                );
                $arrSITE=[];
                $rsSites = \CSite::GetList($by="sort", $order="desc");
                while ($arSite = $rsSites->Fetch())
                    $arrSITE[]=$arSite["LID"];

                //print_r($newPhoto);

                \CEvent::Send("PHOTO_CART_MAIL", $arrSITE, $arEventFields,"Y","",$newPhoto);

                self::clearPhoto();

            }
        }
    }

}