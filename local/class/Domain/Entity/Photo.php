<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 12:44
 */

namespace local\Domain\Entity;

use Bitrix\Main\IO\File;
use JsonSerializable;

class Photo implements JsonSerializable
{
    public function jsonSerialize() {
        $result = array(
            'id' => $this->id,
            'name' => $this->name,
            'sort' => $this->sort,
            'object' => $this->object,
            'photo' => $this->photo,
            'photoId' => $this->photoId,
            'view_count' => $this->view_count,
        );
        return $result;
    }

    /**
     * @var int $view_count
     */
    private $view_count;

    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var int $sort
     */
    private $sort;

    /**
     * @var \local\Domain\Entity\Object[] $object
     */
    private $object;

    /**
     * @var File $photo
     */
    private $photo;

    /**
     * @var int $photoId
     */
    private $photoId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return \local\Domain\Entity\Object[]
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param \local\Domain\Entity\Object[] $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return File
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param File $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * @return int
     */
    public function getPhotoId()
    {
        return $this->photoId;
    }

    /**
     * @param int $photoId
     */
    public function setPhotoId($photoId)
    {
        $this->photoId = $photoId;
    }

    /**
     * @return int
     */
    public function getViewCount()
    {
        return $this->view_count;
    }

    /**
     * @param int $view_count
     */
    public function setViewCount($view_count)
    {
        $this->view_count = $view_count;
    }

}