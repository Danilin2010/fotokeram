<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 15:50
 */

namespace local\Domain\Entity;

use JsonSerializable;

class Address implements JsonSerializable
{

    public function jsonSerialize() {
        $result = array(
            'id' => $this->id,
            'name' => $this->name,
            'sort' => $this->sort,
            'index' => $this->index,
            'region' => $this->region,
            'area' => $this->area,
            'city' => $this->city,
            'address' => $this->address,
            'regionid' => $this->regionid,
            'areaid' => $this->areaid,
            'cityid' => $this->cityid,
            'coords1' => $this->coords1,
            'coords2' => $this->coords2,
        );
        return $result;
    }

    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var int $sort
     */
    private $sort;

    /**
     * @var string $index
     */
    private $index;

    /**
     * @var string $region
     */
    private $region;

    /**
     * @var string $area
     */
    private $area;

    /**
     * @var string $city
     */
    private $city;

    /**
     * @var string $address
     */
    private $address;

    /**
     * @var string $regionid
     */
    private $regionid;

    /**
     * @var string $areaid
     */
    private $areaid;

    /**
     * @var string $cityid
     */
    private $cityid;

    /**
     * @var float $coords1
     */
    private $coords1;

    /**
     * @var float $coords2
     */
    private $coords2;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return string
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param string $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param string $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getRegionId()
    {
        return $this->regionid;
    }

    /**
     * @param string $regionId
     */
    public function setRegionId($regionId)
    {
        $this->regionid = $regionId;
    }

    /**
     * @return string
     */
    public function getAreaId()
    {
        return $this->areaid;
    }

    /**
     * @param string $areaId
     */
    public function setAreaId($areaId)
    {
        $this->areaid = $areaId;
    }

    /**
     * @return string
     */
    public function getCityId()
    {
        return $this->cityid;
    }

    /**
     * @param string $cityId
     */
    public function setCityId($cityId)
    {
        $this->cityid = $cityId;
    }

    /**
     * @return float
     */
    public function getCoords1()
    {
        return $this->coords1;
    }

    /**
     * @param float $coords1
     */
    public function setCoords1($coords1)
    {
        $this->coords1 = $coords1;
    }

    /**
     * @return float
     */
    public function getCoords2()
    {
        return $this->coords2;
    }

    /**
     * @param float $coords2
     */
    public function setCoords2($coords2)
    {
        $this->coords2 = $coords2;
    }

}