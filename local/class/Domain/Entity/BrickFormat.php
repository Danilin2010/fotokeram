<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 12:25
 */

namespace local\Domain\Entity;

use JsonSerializable;

class BrickFormat implements JsonSerializable
{
    public function jsonSerialize() {
        $result = array(
            'id' => $this->id,
            'name' => $this->name,
            'sort' => $this->sort,
            'vendor' => $this->vendor,
            'color' => $this->color,
            'format' => $this->format,
            'brand' => $this->brand,
        );
        return $result;
    }
    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var int $sort
     */
    private $sort;

    /**
     * @var string $vendor
     */
    private $vendor;

    /**
     * @var BrickColor $color
     */
    private $color;

    /**
     * @var BrickFormatFormat $format
     */
    private $format;

    /**
     * @var Brand $brand
     */
    private $brand;


    /**
     * @return Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return string
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param string $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return BrickColor
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param BrickColor $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return BrickFormatFormat
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param BrickFormatFormat $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

}