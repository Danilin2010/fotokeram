<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 17.02.2019
 * Time: 1:43
 */

namespace local\Domain\Entity;

use JsonSerializable;

class City implements JsonSerializable
{
    public function jsonSerialize() {
        $result = array(
            'id' => $this->id,
            'name' => $this->name,
            'zip' => $this->zip,
            'typeShort' => $this->typeShort,
            'okato' => $this->okato,
            'contentType' => $this->contentType,
        );
        return $result;
    }
    /**
     * @var string $id
     */
    private $id;
    /**
     * @var string $name
     */
    private $name;
    /**
     * @var string $zip
     */
    private $zip;
    /**
     * @var string $type
     */
    private $type;
    /**
     * @var string $typeShort
     */
    private $typeShort;
    /**
     * @var string $okato
     */
    private $okato;
    /**
     * @var string $contentType
     */
    private $contentType;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getTypeShort()
    {
        return $this->typeShort;
    }

    /**
     * @param string $typeShort
     */
    public function setTypeShort($typeShort)
    {
        $this->typeShort = $typeShort;
    }

    /**
     * @return string
     */
    public function getOkato()
    {
        return $this->okato;
    }

    /**
     * @param string $okato
     */
    public function setOkato($okato)
    {
        $this->okato = $okato;
    }

    /**
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @param string $contentType
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
    }

    /**
     * @return array
     */
    public function toArray() {
        $b = [
            'id'=>$this->id,
            'name'=>$this->name,
            'zip'=>$this->zip,
            'type'=>$this->type,
            'typeShort'=>$this->typeShort,
            'okato'=>$this->okato,
            'contentType'=>$this->contentType,
        ];
        return $b;
    }
}