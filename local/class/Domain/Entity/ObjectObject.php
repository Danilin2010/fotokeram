<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 12:39
 */

namespace local\Domain\Entity;

use JsonSerializable;

class ObjectObject implements JsonSerializable
{
    public function jsonSerialize() {
        $result = array(
            'id' => $this->id,
            'name' => $this->name,
            'sort' => $this->sort,
            'year' => $this->year,
            'brick' => $this->brick,
            'mix' => $this->mix,
            'seam_color' => $this->seam_color,
            'address' => $this->address,
            'author' => $this->author,
            'object_type' => $this->object_type,
            'view_count' => $this->view_count,
        );
        return $result;
    }

    /**
     * @var int $view_count
     */
    private $view_count;

    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var int $sort
     */
    private $sort;

    /**
     * @var YearList $year
     */
    private $year;

    /**
     * @var BrickFormat[] $brick
     */
    private $brick;

    /**
     * @var bool $mix
     */
    private $mix;

    /**
     * @var SeamColor $seam_color
     */
    private $seam_color;

    /**
     * @var Address $address
     */
    private $address;

    /**
     * @var int $author
     */
    private $author;

    /**
     * @var int $object_type
     */
    private $object_type;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return BrickFormat[]
     */
    public function getBrick()
    {
        return $this->brick;
    }

    /**
     * @param BrickFormat[] $brick
     */
    public function setBrick($brick)
    {
        $this->brick = $brick;
    }

    /**
     * @return bool
     */
    public function isMix()
    {
        return $this->mix;
    }

    /**
     * @param bool $mix
     */
    public function setMix($mix)
    {
        $this->mix = $mix;
    }

    /**
     * @return SeamColor
     */
    public function getSeamColor()
    {
        return $this->seam_color;
    }

    /**
     * @param SeamColor $seam_color
     */
    public function setSeamColor($seam_color)
    {
        $this->seam_color = $seam_color;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param int $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return YearList
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param YearList $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return int
     */
    public function getObjectType()
    {
        return $this->object_type;
    }

    /**
     * @param int $object_type
     */
    public function setObjectType($object_type)
    {
        $this->object_type = $object_type;
    }

    /**
     * @return int
     */
    public function getViewCount()
    {
        return $this->view_count;
    }

    /**
     * @param int $view_count
     */
    public function setViewCount($view_count)
    {
        $this->view_count = $view_count;
    }
}