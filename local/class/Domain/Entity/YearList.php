<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 27.02.2019
 * Time: 22:49
 */

namespace local\Domain\Entity;

use JsonSerializable;

class YearList implements JsonSerializable
{
    public function jsonSerialize() {
        $result = array(
            'id' => $this->id,
            'name' => $this->name,
            'sort' => $this->sort,
            'propertyId' => $this->propertyId,
            'value' => $this->value,
            'def' => $this->def,
            'xmlId' => $this->xmlId,
            'code' => $this->code,
            'iblockId' => $this->iblockId,
        );
        return $result;
    }
    /**
     * @var int $id
     */
    private $id;

    /**
     * @var int $propertyId
     */
    private  $propertyId;

    /**
     * @var int $value
     */
    private  $value;

    /**
     * @var string $def
     */
    private  $def;

    /**
     * @var int $sort
     */
    private  $sort;

    /**
     * @var int $xmlId
     */
    private  $xmlId;

    /**
     * @var string $code
     */
    private  $code;

    /**
     * @var int $iblockId
     */
    private  $iblockId;

    /**
     * @var string $name
     */
    private  $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * @param int $propertyId
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getDef()
    {
        return $this->def;
    }

    /**
     * @param string $def
     */
    public function setDef($def)
    {
        $this->def = $def;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getXmlId()
    {
        return $this->xmlId;
    }

    /**
     * @param int $xmlId
     */
    public function setXmlId($xmlId)
    {
        $this->xmlId = $xmlId;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getIblockId()
    {
        return $this->iblockId;
    }

    /**
     * @param int $iblockId
     */
    public function setIblockId($iblockId)
    {
        $this->iblockId = $iblockId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}