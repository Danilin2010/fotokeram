<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 07.07.2019
 * Time: 1:54
 */

namespace local\Domain\Entity;

use DateTime;
use JsonSerializable;

class DownloadAttempts implements JsonSerializable
{

    public function jsonSerialize()
    {
        $param=
            [
                'id' => $this->id,
                'user' => $this->user,
            ];

        if($this->date)
            $param['date']=$this->date->format('d.m.Y');

        return $param;
    }

    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $user
     */
    private $user;

    /**
     * @var DateTime $date
     */
    private $date;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

}