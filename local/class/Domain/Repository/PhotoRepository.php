<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 11.02.2019
 * Time: 0:14
 */

namespace local\Domain\Repository;

use Bitrix\Main\Loader;
use local\content\fotokeram\photo\photoTable as PhotoTable;
use local\Domain\Entity\Photo;
use local\Domain\Factory\PhotoFactory;
use Bitrix\Main\Entity;
use local\Domain\FactoryConverter\FactoryConverter;
use local\content\fotokeram\object\objectTable as ObjectTable;
use local\content\fotokeram\brand\brandTable as BrandTable;
use local\content\fotokeram\address\addressTable as AddressTable;
use local\content\fotokeram\brickformat\brickformatTable as BrickFormatTable;
use local\Services\SavePhoto;

class PhotoRepository
{
    /**
     * BrandRepository constructor.
     * @param int $iblockId
     * @throws \Bitrix\Main\LoaderException
     */
    public function __construct($iblockId=0)
    {
        Loader::includeModule('balamarket.orm');
    }

    /**
     * @param array $filter
     * @return int
     * @throws \Exception
     */
    public function getCount($filter=[]){
        $list=$this->_getList(false,$filter,[],false,false,[
            "CNTD"
        ]);
        if(count($list)==1)
            return $list[0]['cntd'];
        else
            return 0;
    }

    /**
     * @param int $id
     * @return bool|Photo
     * @throws \Exception
     */
    public function getById($id)
    {
        $filter=["ID"=>$id];
        $list=$this->getList([],$filter);
        if(count($list)==1)
            return $list[0];
        return false;
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return Photo[]
     * @throws \Exception
     */
    public function getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        return PhotoFactory::createFromCollection($this->_getList($order,$filter,$group,$limit,$offset,$select));
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return array
     * @throws \Exception
     */
    public function _getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        if($select==[])
            $select=[
                'id',
                'name',
                'sort',
                'photo',
                'object_id',
                'view_count',
            ];

        if($order===[])
            $order=[
                'sort'=>'asc',
                'name'=>'asc',
            ];

        $param=[
            'runtime'=>$this->getRuntime(),
            'select'=>$select,
            'filter'=>$filter,
            'group'=>$group,
            "cache"=>["ttl"=>3600]
        ];
        if($order)
            $param['order']=$order;
        if($limit)
            $param['limit']=$limit;
        if($offset)
            $param['offset']=$offset;
        $res=PhotoTable::getList($param);
        $elements=[];
        while($ob = $res->fetch(new FactoryConverter))
        {
            $newOb=$ob;

            if($newOb["object_id"])
            {
                $BrickFormatRepository=new ObjectRepository();
                $List=$BrickFormatRepository->_getList([],[
                    'id'=>explode(',',$newOb["object_id"])
                ]);
                $newOb["object"]=$List;
            }

            $elements[]=array_change_key_case($newOb);
        }
        return $elements;
    }

    /**
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    private function getRuntime(){
        return [
            'CNTD' => new Entity\ExpressionField('CNT', 'COUNT(DISTINCT local_content_fotokeram_photo_photo.ID)'),
            'CNT' => new Entity\ExpressionField('CNT', 'COUNT(*)'),
            'photo' => new Entity\ExpressionField('photo', '%s', 'PROPERTY_SIMPLE.photo'),
            'view_count' => new Entity\ExpressionField('view_count', '%s', 'PROPERTY_SIMPLE.view_count'),
            "object_id" => array(
                "data_type" => "string",
                "expression" => array(
                    "GROUP_CONCAT(DISTINCT %s)",
                    "PROPERTY_MULTIPLE_object.VALUE"
                )
            ),

            'object_xmlid' => new Entity\ExpressionField('object_xmlid', '%s', 'PROPERTY_MULTIPLE_object.VALUE'),
            'object' => new Entity\ReferenceField(
                'object',
                ObjectTable::getEntity(),
                array(
                    '=this.object_xmlid' => 'ref.ID',
                )//,
                //array('join_type' => 'RIGHT')
            ),

            //'object_name' => new Entity\ExpressionField('object_name', '%s', 'object.NAME'),

            'object_brickformat_id' => new Entity\ExpressionField('object_brickformat_id', '%s', 'object.PROPERTY_MULTIPLE_brick.VALUE'),
            'object_brickformat' => new Entity\ReferenceField(
                'object_brickformat',
                BrickFormatTable::getEntity(),
                array(
                    '=this.object_brickformat_id' => 'ref.ID',
                )
            ),

            'object_brickformat_name' => new Entity\ExpressionField('object_brickformat_name', '%s', 'object_brickformat.NAME'),

            "object_brickformat_name_sort" => array(
                "data_type" => "string",
                "expression" => array(
                    "GROUP_CONCAT(DISTINCT %s)",
                    "object_brickformat_name"
                )
            ),

            'object_brickformat_vendor' => new Entity\ExpressionField('object_brickformat_vendor', '%s', 'object_brickformat.PROPERTY_SIMPLE.vendor'),

            'author' => new Entity\ExpressionField('author', '%s', 'object.PROPERTY_SIMPLE.author'),

            'object_brickformat_color_id' => new Entity\ExpressionField('object_brickformat_color_id', '%s', 'object_brickformat.PROPERTY_SIMPLE.color'),

            'object_brickformat_format_id' => new Entity\ExpressionField('object_brickformat_format_id', '%s', 'object_brickformat.PROPERTY_SIMPLE.format'),

            'object_brickformat_brand_id' => new Entity\ExpressionField('object_brickformat_brand_id', '%s', 'object_brickformat.PROPERTY_SIMPLE.brand'),

            'seam_color_id' => new Entity\ExpressionField('seam_color_id', '%s', 'object.PROPERTY_SIMPLE.seam_color'),

            'mix' => new Entity\ExpressionField('mix', '%s', 'object.PROPERTY_SIMPLE.mix'),

            'object_type' => new Entity\ExpressionField('object_type', '%s', 'object.PROPERTY_SIMPLE.object_type'),

            'object_address_id' => new Entity\ExpressionField('object_address_id', '%s', 'object.PROPERTY_SIMPLE.address'),

            'object_address' => new Entity\ReferenceField(
                'object_address',
                AddressTable::getEntity(),
                array(
                    '=this.object_address_id' => 'ref.ID',
                )
            ),

            'object_address_regionid' => new Entity\ExpressionField('object_address_regionid', '%s', 'object_address.PROPERTY_SIMPLE.regionid'),
            'object_address_areaid' => new Entity\ExpressionField('object_address_areaid', '%s', 'object_address.PROPERTY_SIMPLE.areaid'),
            'object_address_cityid' => new Entity\ExpressionField('object_address_cityid', '%s', 'object_address.PROPERTY_SIMPLE.cityid'),

            'object_year' => new Entity\ExpressionField('object_year', '%s', 'object.PROPERTY_SIMPLE.year'),

        ];
    }

    /**
     * @param Photo $el
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public function Add(Photo $el)
    {
        $CIBlockel = new \CIBlockElement;
        $arLoadProductArray = $this->getParamsUpdate($el);
        if ($PRODUCT_ID = $CIBlockel->Add($arLoadProductArray)) {
            SavePhoto::addPhoto($PRODUCT_ID);
            return $PRODUCT_ID;
        }else
            throw new \InvalidArgumentException(implode(', ',$CIBlockel->LAST_ERROR));
    }

    /**
     * @param int $id
     * @param Photo $el
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public function Update($id,Photo $el)
    {
        $CIBlockel = new \CIBlockElement;
        $arLoadProductArray=$this->getParamsUpdate($el);
        if($CIBlockel->Update($id,$arLoadProductArray)) {
            SavePhoto::addPhoto($id);
            return true;
        }else
            throw new \InvalidArgumentException(implode(', ',$CIBlockel->LAST_ERROR));
    }

    /**
     * @param Photo $el
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    private function getParamsUpdate(Photo $el){
        $PROP = [];

        if($el->getPhoto())
        {
            if($el->getPhotoId()>0 && $el->getId()>0)
            {
                $db_props = \CIBlockElement::GetProperty(
                    PhotoTable::getIblockId(),
                    $el->getId(),
                    ["sort" => "asc"],
                    ["CODE"=>"photo"]
                );
                if($ar_props = $db_props->Fetch())
                {
                    $VALUE = (int)($ar_props["VALUE"]);
                    if($VALUE!=$el->getPhotoId())
                    {
                        \CFile::Delete($VALUE);
                        $PROP["photo"]=\CFile::MakeFileArray($el->getPhoto()->getPath());
                    }
                }
            }else{
                $PROP["photo"]=\CFile::MakeFileArray($el->getPhoto()->getPath());
            }
        }

        if($el->getObject()){
            $ObjectArr=$el->getObject();
            $ObjectRepository=new ObjectRepository();
            $ObjectId=[];
            foreach ($ObjectArr as $Object)
            {
                if($Object->getId()>0)
                {
                    $ObjectRepository->Update($Object->getId(),$Object);
                    $ObjectId[]=$Object->getId();
                }else{
                    $Id=$ObjectRepository->Add($Object);
                    if($Id>0)
                        $ObjectId[]=$Id;
                }
            }
            $PROP["object"]=$ObjectId;
        }else{
            $PROP["object"]=false;
        }

        if($el->getViewCount()){
            $PROP["view_count"]=$el->getViewCount();
        }else{
            $PROP["view_count"]=false;
        }

        $arLoadProductArray=[
            "IBLOCK_ID"=>PhotoTable::getIblockId(),
            "ACTIVE"=>"Y",
            "NAME"=>$el->getName(),
            "PROPERTY_VALUES"=> $PROP,
        ];
        if($el->getSort()>0)
            $arLoadProductArray["SORT"]=$el->getSort();
        return $arLoadProductArray;
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function Delete($id)
    {
        if($id<=0)
            throw new \InvalidArgumentException('Invalid id');
        SavePhoto::deletePhoto($id);
        \CIBlockElement::Delete($id);
        return true;
    }
}