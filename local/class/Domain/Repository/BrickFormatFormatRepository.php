<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 18:14
 */

namespace local\Domain\Repository;

use Bitrix\Main\Loader;
use local\Domain\Entity\BrickFormatFormat;
use local\Domain\Factory\BrickFormatFormatFactory;
use local\Domain\FactoryConverter\FactoryConverter;
use local\content\fotokeram\brickformatformat\brickformatformatTable as BrickFormatFormatTable;

class BrickFormatFormatRepository
{
    /**
     * BrandRepository constructor.
     * @param int $iblockId
     * @throws \Bitrix\Main\LoaderException
     */
    public function __construct($iblockId=0)
    {
        Loader::includeModule('balamarket.orm');
    }

    /**
     * @param int $id
     * @return bool|BrickFormatFormat
     * @throws \Exception
     */
    public function getById($id)
    {
        $filter=["ID"=>$id];
        $list=$this->getList([],$filter);
        if(count($list)==1)
            return $list[0];
        return false;
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return BrickFormatFormat[]
     * @throws \Exception
     */
    public function getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        return BrickFormatFormatFactory::createFromCollection($this->_getList($order,$filter,$group,$limit,$offset,$select));
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return array
     * @throws \Exception
     */
    public function _getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        if($select===[])
            $select=[
                'id',
                'name',
                'sort',
            ];

        if($order==[])
            $order=[
                'sort'=>'asc',
                'name'=>'asc',
            ];

        $param=[
            'runtime'=>$this->getRuntime(),
            'select'=>$select,
            'filter'=>$filter,
            'group'=>$group,
            "cache"=>["ttl"=>3600]
        ];
        if($order)
            $param['order']=$order;
        if($limit)
            $param['limit']=$limit;
        if($offset)
            $param['offset']=$offset;

        $res=BrickFormatFormatTable::getList($param);
        $elements=[];
        while($ob = $res->fetch(new FactoryConverter))
            $elements[]=array_change_key_case($ob);
        return $elements;
    }

    /**
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    private function getRuntime(){
        return [

        ];
    }

    /**
     * @param BrickFormatFormat $el
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public function Add(BrickFormatFormat $el)
    {
        $CIBlockel = new \CIBlockElement;
        $arLoadProductArray=$this->getParamsUpdate($el);
        if($PRODUCT_ID = $CIBlockel->Add($arLoadProductArray))
            return $PRODUCT_ID;
        else
            throw new \InvalidArgumentException(implode(', ',$CIBlockel->LAST_ERROR));
    }

    /**
     * @param int $id
     * @param BrickFormatFormat $el
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public function Update($id,BrickFormatFormat $el)
    {
        $CIBlockel = new \CIBlockElement;
        $arLoadProductArray=$this->getParamsUpdate($el);
        if($CIBlockel->Update($id,$arLoadProductArray))
            return true;
        else
            throw new \InvalidArgumentException(implode(', ',$CIBlockel->LAST_ERROR));
    }

    /**
     * @param BrickFormatFormat $el
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    private function getParamsUpdate(BrickFormatFormat $el){
        $arLoadProductArray=[
            "IBLOCK_ID"=>BrickFormatFormatTable::getIblockId(),
            "ACTIVE"=>"Y",
            "NAME"=>$el->getName(),
        ];
        if($el->getSort()>0)
            $arLoadProductArray["SORT"]=$el->getSort();
        return $arLoadProductArray;
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function Delete($id)
    {
        if($id<=0)
            throw new \InvalidArgumentException('Invalid id');
        \CIBlockElement::Delete($id);
        return true;
    }
}