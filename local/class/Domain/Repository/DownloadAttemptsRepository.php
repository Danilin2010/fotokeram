<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 15:42
 */

namespace local\Domain\Repository;

use Bitrix\Main\Loader;
use local\Domain\Entity\DownloadAttempts;
use local\Domain\Factory\DownloadAttemptsFactory;
use local\Domain\FactoryConverter\FactoryConverter;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\ORM\Data\DataManager;
use Exception;
use Bitrix\Main\Entity;

class DownloadAttemptsRepository
{

    /**
     * @var DataManager
     */
    private $class;

    /**
     * DownloadAttemptsRepository constructor.
     * @param int $iblockId
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function __construct($iblockId=0)
    {
        Loader::includeModule('highloadblock');
        $hlblock = HL\HighloadBlockTable::getById(HIGHLOAD_DOWNLOADATTEMPTS)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $this->class = $entity->getDataClass();
    }

    /**
     * @param int $id
     * @return bool|DownloadAttempts
     * @throws \Exception
     */
    public function getById($id)
    {
        $filter=["ID"=>$id];
        $list=$this->getList([],$filter);
        if(count($list)==1)
            return $list[0];
        return false;
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return DownloadAttempts[]
     * @throws \Exception
     */
    public function getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        return DownloadAttemptsFactory::createFromCollection($this->_getList($order,$filter,$group,$limit,$offset,$select));
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return array
     * @throws \Exception
     */
    public function _getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        if($select==[])
            $select=[
                'ID',
                'user',
                'date',
            ];

          $param=[
            'runtime'=>$this->getRuntime(),
            'select'=>$select,
            'filter'=>$filter,
            'group'=>$group,
            "cache"=>["ttl"=>3600]
        ];
        if($order)
            $param['order']=$order;
        if($limit)
            $param['limit']=$limit;
        if($offset)
            $param['offset']=$offset;
        $class=$this->class;
        $res=$class::getList($param);
        $elements=[];
        while($ob = $res->fetch(new FactoryConverter))
            $elements[]=array_change_key_case($ob);
        return $elements;
    }

    /**
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    private function getRuntime(){
        return [
            'CNT' => new Entity\ExpressionField('CNT', 'COUNT(*)'),


            'user' => new Entity\ExpressionField('user', '%s', 'UF_USER'),
            'date' => new Entity\ExpressionField('date', '%s', 'UF_DATE'),

        ];
    }

    /**
     * @param DownloadAttempts $el
     * @return array|int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     * @throws Exception
     */
    public function Add(DownloadAttempts $el)
    {
        $class=$this->class;
        $arLoadProductArray=$this->getParamsUpdate($el);
        $result=$class::add($arLoadProductArray);
        if($result->isSuccess())
            return $result->getId();
        else
            throw new \InvalidArgumentException(implode(', ',$result->getErrorMessages()));
    }

    /**
     * @param int $id
     * @param DownloadAttempts $el
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     * @throws Exception
     */
    public function Update($id,DownloadAttempts $el)
    {
        $class=$this->class;
        $arLoadProductArray=$this->getParamsUpdate($el);
        $result=$class::update($id,$arLoadProductArray);
        if($result->isSuccess())
            return true;
        else
            throw new \InvalidArgumentException(implode(', ',$result->getErrorMessages()));
    }

    /**
     * @param DownloadAttempts $el
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    private function getParamsUpdate(DownloadAttempts $el){
        $arLoadProductArray=[];
        if($el->getUser()>0)
            $arLoadProductArray["UF_USER"]=$el->getUser();
        if($el->getDate()>0)
            $arLoadProductArray["UF_DATE"]=$el->getDate()->format('d.m.Y H:i:s');
        return $arLoadProductArray;
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function Delete($id)
    {
        if($id<=0)
            throw new \InvalidArgumentException('Invalid id');
        $class=$this->class;
        $class::delete($id);
        return true;
    }
}