<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 18:14
 */

namespace local\Domain\Repository;

use Bitrix\Main\Loader;
use local\content\fotokeram\brickformat\brickformatTable as BrickFormatTable;
use local\Domain\Entity\BrickFormat;
use local\Domain\Factory\BrickFormatFactory;
use Bitrix\Main\Entity;
use local\Domain\FactoryConverter\FactoryConverter;
use local\content\fotokeram\brickcolor\brickcolorTable as BrickColorTable;
use local\content\fotokeram\brickformatformat\brickformatformatTable as BrickFormatFormatTable;
use local\content\fotokeram\brand\brandTable as BrandTable;

class BrickFormatRepository
{
    /**
     * BrandRepository constructor.
     * @param int $iblockId
     * @throws \Bitrix\Main\LoaderException
     */
    public function __construct($iblockId=0)
    {
        Loader::includeModule('balamarket.orm');
    }

    /**
     * @param int $id
     * @return bool|BrickFormat
     * @throws \Exception
     */
    public function getById($id)
    {
        $filter=["ID"=>$id];
        $list=$this->getList([],$filter);
        if(count($list)==1)
            return $list[0];
        return false;
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return BrickFormat[]
     * @throws \Exception
     */
    public function getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        return BrickFormatFactory::createFromCollection($this->_getList($order,$filter,$group,$limit,$offset,$select));
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return array
     * @throws \Exception
     */
    public function _getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        if($select==[])
            $select=[
                'id',
                'name',
                'sort',
                'vendor',
                'format',
                'color_id',
                'color_name',
                'color_sort',
                'format_id',
                'format_name',
                'format_sort',
                'brand_id',
                'brand_name',
                'brand_sort',
            ];

        if($order===[])
            $order=[
                'sort'=>'asc',
                'name'=>'asc',
            ];

        $param=[
            'runtime'=>$this->getRuntime(),
            'select'=>$select,
            'filter'=>$filter,
            'group'=>$group,
            "cache"=>["ttl"=>3600]
        ];
        if($order)
            $param['order']=$order;
        if($limit)
            $param['limit']=$limit;
        if($offset)
            $param['offset']=$offset;

        $res=BrickFormatTable::getList($param);
        $elements=[];
        while($ob = $res->fetch(new FactoryConverter))
        {
            $newOb=$ob;
            if($newOb["color_id"])
            {
                $newOb["color"]["id"]=$newOb["color_id"];
                unset($newOb["color_id"]);
            }
            if($newOb["color_name"])
            {
                $newOb["color"]["name"]=$newOb["color_name"];
                unset($newOb["color_name"]);
            }
            if($newOb["color_sort"])
            {
                $newOb["color"]["sort"]=$newOb["color_sort"];
                unset($newOb["color_sort"]);
            }

            if($newOb["format_id"])
            {
                $newOb["format"]["id"]=$newOb["format_id"];
                unset($newOb["format_id"]);
            }
            if($newOb["format_name"])
            {
                $newOb["format"]["name"]=$newOb["format_name"];
                unset($newOb["format_name"]);
            }
            if($newOb["format_sort"])
            {
                $newOb["format"]["sort"]=$newOb["format_sort"];
                unset($newOb["format_sort"]);
            }

            if($newOb["brand_id"])
            {
                $newOb["brand"]["id"]=$newOb["brand_id"];
                unset($newOb["brand_id"]);
            }
            if($newOb["brand_name"])
            {
                $newOb["brand"]["name"]=$newOb["brand_name"];
                unset($newOb["brand_name"]);
            }
            if($newOb["brand_sort"])
            {
                $newOb["brand"]["sort"]=$newOb["brand_sort"];
                unset($newOb["brand_sort"]);
            }

            $elements[]=array_change_key_case($newOb);
        }
        return $elements;
    }

    /**
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    private function getRuntime(){
        return [
            'vendor' => new Entity\ExpressionField('vendor', '%s', 'PROPERTY_SIMPLE.vendor'),

            'format_id' => new Entity\ExpressionField('color_id', '%s', 'PROPERTY_SIMPLE.format'),
            'format' => new Entity\ReferenceField(
                'format',
                BrickFormatFormatTable::getEntity(),
                array(
                    '=this.format_id' => 'ref.ID',
                )
            ),
            'format_name' => new Entity\ExpressionField('format_name', '%s', 'format.NAME'),
            'format_sort' => new Entity\ExpressionField('format_sort', '%s', 'format.SORT'),

            'color_id' => new Entity\ExpressionField('color_id', '%s', 'PROPERTY_SIMPLE.color'),
            'color' => new Entity\ReferenceField(
                'color',
                BrickColorTable::getEntity(),
                array(
                    '=this.color_id' => 'ref.ID',
                )
            ),
            'color_name' => new Entity\ExpressionField('color_name', '%s', 'color.NAME'),
            'color_sort' => new Entity\ExpressionField('color_sort', '%s', 'color.SORT'),

            'brand_id' => new Entity\ExpressionField('brand_id', '%s', 'PROPERTY_SIMPLE.brand'),
            'brand' => new Entity\ReferenceField(
                'brand',
                BrandTable::getEntity(),
                array(
                    '=this.brand_id' => 'ref.ID',
                )
            ),
            'brand_name' => new Entity\ExpressionField('brand_name', '%s', 'brand.NAME'),
            'brand_sort' => new Entity\ExpressionField('brand_sort', '%s', 'brand.SORT'),
        ];
    }

    /**
     * @param BrickFormat $el
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public function Add(BrickFormat $el)
    {
        $CIBlockel = new \CIBlockElement;
        $arLoadProductArray=$this->getParamsUpdate($el);
        if($PRODUCT_ID = $CIBlockel->Add($arLoadProductArray))
            return $PRODUCT_ID;
        else
            throw new \InvalidArgumentException(implode(', ',$CIBlockel->LAST_ERROR));
    }

    /**
     * @param int $id
     * @param BrickFormat $el
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public function Update($id,BrickFormat $el)
    {
        $CIBlockel = new \CIBlockElement;
        $arLoadProductArray=$this->getParamsUpdate($el);
        if($CIBlockel->Update($id,$arLoadProductArray))
            return true;
        else
            throw new \InvalidArgumentException(implode(', ',$CIBlockel->LAST_ERROR));
    }

    /**
     * @param BrickFormat $el
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    private function getParamsUpdate(BrickFormat $el){
        $PROP = [];
        if($el->getVendor())
            $PROP["vendor"]=$el->getVendor();
        else
            $PROP["vendor"]=false;

        if($el->getFormat()){
            $Format=$el->getFormat();
            $BrickColorRepository=new BrickFormatFormatRepository();
            if($Format->getId()>0)
            {
                $BrickColorRepository->Update($Format->getId(),$Format);
                $PROP["format"]=$Format->getId();
            }else{
                $Id=$BrickColorRepository->Add($Format);
                if($Id>0)
                    $PROP["format"]=$Id;
            }
        }else{
            $PROP["format"]=false;
        }

        if($el->getColor()){
            $Color=$el->getColor();
            $BrickColorRepository=new BrickColorRepository();
            if($Color->getId()>0)
            {
                $BrickColorRepository->Update($Color->getId(),$Color);
                $PROP["color"]=$Color->getId();
            }else{
                $Id=$BrickColorRepository->Add($Color);
                if($Id>0)
                    $PROP["color"]=$Id;
            }
        }else{
            $PROP["color"]=false;
        }

        if($el->getBrand()){
            $Brand=$el->getBrand();
            $BrandRepository=new BrandRepository();
            if($Brand->getId()>0)
            {
                $BrandRepository->Update($Brand->getId(),$Brand);
                $PROP["brand"]=$Brand->getId();
            }else{
                $Id=$BrandRepository->Add($Brand);
                if($Id>0)
                    $PROP["brand"]=$Id;
            }
        }else{
            $PROP["brand"]=false;
        }

        $arLoadProductArray=[
            "IBLOCK_ID"=>BrickFormatTable::getIblockId(),
            "ACTIVE"=>"Y",
            "NAME"=>$el->getName(),
            "PROPERTY_VALUES"=> $PROP,
        ];
        if($el->getSort()>0)
            $arLoadProductArray["SORT"]=$el->getSort();
        return $arLoadProductArray;
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function Delete($id)
    {
        if($id<=0)
            throw new \InvalidArgumentException('Invalid id');
        \CIBlockElement::Delete($id);
        return true;
    }
}