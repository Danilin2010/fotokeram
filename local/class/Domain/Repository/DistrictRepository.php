<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 17.02.2019
 * Time: 1:25
 */

namespace local\Domain\Repository;

use local\Domain\Entity\District;
use local\Domain\Factory\DistrictFactory;
use Bitrix\Main\Data\Cache;
use local\Kladr;

class DistrictRepository
{

    /**
     * @param string $ParentId
     * @return District[]
     */
    public function getListByIdRegion($ParentId){

        $cache = Cache::createInstance();
        $cacheTime = 30*60;
        $cacheId = 'DistrictgetListByIdRegion'.$ParentId;
        $cacheDir = 'districtgetlistbyidregion';
        $arResult = [];

        if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
            $arResult = $cache->getVars();
        } elseif ($cache->startDataCache()) {

            $api = new Kladr\Api('','');
            $query = new Kladr\Query();
            $query->ParentType = Kladr\ObjectType::Region;
            $query->ParentId = $ParentId;
            $query->ContentType = Kladr\ObjectType::District;
            $arResult = $api->QueryToArray($query);

            $cache->endDataCache($arResult);
        }

        return DistrictFactory::createFromCollection($arResult);

    }

    /**
     * @param string $Id
     * @return bool|District
     */
    public function getById($Id){
        $cache = Cache::createInstance();
        $cacheTime = 30*60;
        $cacheId = 'DistrictgetListById'.$Id;
        $cacheDir = 'districtgetlistbyid';
        $arResult = [];

        if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
            $arResult = $cache->getVars();
        } elseif ($cache->startDataCache()) {

            $api = new Kladr\Api('','');
            $query = new Kladr\Query();
            $query->ParentType = Kladr\ObjectType::District;
            $query->ParentId = $Id;
            $query->ContentType = Kladr\ObjectType::District;
            $arResult = $api->QueryToArray($query);

            $cache->endDataCache($arResult);
        }
        if(count($arResult)==1)
            return DistrictFactory::createFromArray($arResult[0]);
        else
            return false;
    }

}