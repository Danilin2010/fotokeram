<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 27.02.2019
 * Time: 23:23
 */

namespace local\Domain\Repository;

use Bitrix\Iblock\PropertyEnumerationTable;
use local\content\fotokeram\object\objectTable as ObjectTable;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Entity;
use local\Domain\FactoryConverter\FactoryConverter;
use local\Domain\Factory\YearListFactory;
use local\Domain\Entity\YearList;

class YearListRepository
{

    /**
     * YearListRepository constructor.
     * @throws \Bitrix\Main\LoaderException
     */
    public function __construct()
    {
        Loader::includeModule('iblock');
    }

    /**
     * @return array
     */
    private function getYearSelect(){
        return [
            'ID',
            'PROPERTY_ID',
            'VALUE',
            'DEF',
            'SORT',
            'XML_ID',
            'CODE',
            'IBLOCK_ID',
            'NAME',
        ];
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    private function getYearRuntime(){
        return [
            'PROPERTY' => new Entity\ReferenceField(
                'PROPERTY',
                PropertyTable::getEntity(),
                array(
                    '=this.PROPERTY_ID' => 'ref.ID',
                )
            ),
            'CODE' => new Entity\ExpressionField('CODE', '%s', 'PROPERTY.CODE'),
            'IBLOCK_ID' => new Entity\ExpressionField('IBLOCK_ID', '%s', 'PROPERTY.IBLOCK_ID'),
            'NAME' => new Entity\ExpressionField('NAME', '%s', 'PROPERTY.NAME'),
        ];
    }

    /**
     * @param int $id
     * @return bool|YearList
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getById($id)
    {
        $res=PropertyEnumerationTable::getList([
            'select'=>$this->getYearSelect(),
            'runtime'=>$this->getYearRuntime(),
            'filter'=>[
                "ID"=>$id,
                "CODE" => "year",
                'IBLOCK_ID'=>ObjectTable::getIblockId(),
            ],
        ]);
        if($ob = $res->fetch(new FactoryConverter))
            return YearListFactory::createFromArray(array_change_key_case($ob));
        return false;
    }

    /**
     * @param array $filter
     * @return YearList[]
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getYearList($filter=[]){
        $res=PropertyEnumerationTable::getList([
            'order'=>['VALUE'=>'DESC'],
            'select'=>$this->getYearSelect(),
            'runtime'=>$this->getYearRuntime(),
            'filter'=>array_merge([
                "CODE" => "year",
                'IBLOCK_ID'=>ObjectTable::getIblockId(),
            ],$filter),
        ]);
        $elements=[];
        while($ob = $res->fetch(new FactoryConverter))
            $elements[]=array_change_key_case($ob);
        return YearListFactory::createFromCollection($elements);
    }
}