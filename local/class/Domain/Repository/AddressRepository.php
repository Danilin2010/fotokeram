<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 17:48
 */

namespace local\Domain\Repository;

use Bitrix\Main\Loader;
use local\content\fotokeram\address\addressTable as AddressTable;
use local\Domain\Entity\Address;
use local\Domain\Factory\AddressFactory;
use Bitrix\Main\Entity;
use local\Domain\FactoryConverter\FactoryConverter;

class AddressRepository
{
    /**
     * BrandRepository constructor.
     * @param int $iblockId
     * @throws \Bitrix\Main\LoaderException
     */
    public function __construct($iblockId=0)
    {
        Loader::includeModule('balamarket.orm');
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return Address[]
     * @throws \Exception
     */
    public function getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        return AddressFactory::createFromCollection($this->_getList($order,$filter,$group,$limit,$offset,$select));
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return array
     * @throws \Exception
     */
    public function _getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        if($select==[])
            $select=[
                'id',
                'name',
                'sort',
                'index',
                'region',
                'area',
                'city',
                'address',
                'regionid',
                'areaid',
                'cityid',
                'coords1',
                'coords2',
            ];

        if($order===[])
            $order=[
                'sort'=>'asc',
                'name'=>'asc',
            ];
        $param=[
            'runtime'=>$this->getRuntime(),
            'select'=>$select,
            'filter'=>$filter,
            'group'=>$group,
            "cache"=>["ttl"=>3600]
        ];
        if($order)
            $param['order']=$order;
        if($limit)
            $param['limit']=$limit;
        if($offset)
            $param['offset']=$offset;
        $res=AddressTable::getList($param);
        $elements=[];
        while($ob = $res->fetch(new FactoryConverter))
            $elements[]=array_change_key_case($ob);
        return $elements;
    }

    /**
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    private function getRuntime(){
        return [
            'CNT' => new Entity\ExpressionField('CNT', 'COUNT(*)'),
            'index' => new Entity\ExpressionField('index', '%s', 'PROPERTY_SIMPLE.index'),
            'region' => new Entity\ExpressionField('region', '%s', 'PROPERTY_SIMPLE.region'),
            'area' => new Entity\ExpressionField('area', '%s', 'PROPERTY_SIMPLE.area'),
            'city' => new Entity\ExpressionField('city', '%s', 'PROPERTY_SIMPLE.city'),
            'address' => new Entity\ExpressionField('address', '%s', 'PROPERTY_SIMPLE.address'),
            'regionid' => new Entity\ExpressionField('regionid', '%s', 'PROPERTY_SIMPLE.regionid'),
            'areaid' => new Entity\ExpressionField('areaid', '%s', 'PROPERTY_SIMPLE.areaid'),
            'cityid' => new Entity\ExpressionField('cityid', '%s', 'PROPERTY_SIMPLE.cityid'),
            'coords1' => new Entity\ExpressionField('coords1', '%s', 'PROPERTY_SIMPLE.coords1'),
            'coords2' => new Entity\ExpressionField('coords2', '%s', 'PROPERTY_SIMPLE.coords2'),
        ];
    }

    /**
     * @param Address $el
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public function Add(Address $el)
    {
        $CIBlockel = new \CIBlockElement;
        $arLoadProductArray=$this->getParamsUpdate($el);
        if($PRODUCT_ID = $CIBlockel->Add($arLoadProductArray))
            return $PRODUCT_ID;
        else
            throw new \InvalidArgumentException(implode(', ',$CIBlockel->LAST_ERROR));
    }

    /**
     * @param int $id
     * @param Address $el
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public function Update($id,Address $el)
    {
        $CIBlockel = new \CIBlockElement;
        $arLoadProductArray=$this->getParamsUpdate($el);
        if($CIBlockel->Update($id,$arLoadProductArray))
            return true;
        else
            throw new \InvalidArgumentException(implode(', ',$CIBlockel->LAST_ERROR));
    }

    /**
     * @param Address $el
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    private function getParamsUpdate(Address $el){
        $PROP = [];
        if($el->getIndex())
            $PROP["index"]=$el->getIndex();
        if($el->getRegion())
            $PROP["region"]=$el->getRegion();
        if($el->getArea())
            $PROP["area"]=$el->getArea();
        if($el->getCity())
            $PROP["city"]=$el->getCity();
        if($el->getAddress())
            $PROP["address"]=$el->getAddress();

        if($el->getRegionId())
            $PROP["regionid"]=$el->getRegionId();
        if($el->getAreaId())
            $PROP["areaid"]=$el->getAreaId();
        if($el->getCityId())
            $PROP["cityid"]=$el->getCityId();

        if($el->getCoords1())
            $PROP["coords1"]=$el->getCoords1();
        if($el->getCoords2())
            $PROP["coords2"]=$el->getCoords2();

        $arLoadProductArray=[
            "IBLOCK_ID"=>AddressTable::getIblockId(),
            "ACTIVE"=>"Y",
            "NAME"=>$el->getName(),
            "PROPERTY_VALUES"=> $PROP,
        ];
        if($el->getSort()>0)
            $arLoadProductArray["SORT"]=$el->getSort();
        return $arLoadProductArray;
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function Delete($id)
    {
        if($id<=0)
            throw new \InvalidArgumentException('Invalid id');
        \CIBlockElement::Delete($id);
        return true;
    }
}