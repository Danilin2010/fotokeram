<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 18:48
 */

namespace local\Domain\Repository;

use Bitrix\Main\Loader;
use local\content\fotokeram\object\objectTable as ObjectTable;
use local\Domain\Entity\ObjectObject;
use local\Domain\Factory\ObjectFactory;
use Bitrix\Main\Entity;
use local\Domain\FactoryConverter\FactoryConverter;
use local\content\fotokeram\seamcolor\seamcolorTable as SeamColorTable;
use local\content\fotokeram\address\addressTable as AddressTable;
use local\content\fotokeram\brickformat\brickformatTable as BrickFormatTable;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Iblock\PropertyEnumerationTable;

class ObjectRepository
{
    /**
     * BrandRepository constructor.
     * @param int $iblockId
     * @throws \Bitrix\Main\LoaderException
     */
    public function __construct($iblockId=0)
    {
        Loader::includeModule('balamarket.orm');
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return ObjectObject[]
     * @throws \Exception
     */
    public function getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        return ObjectFactory::createFromCollection($this->_getList($order,$filter,$group,$limit,$offset,$select));
    }

    /**
     * @param int $id
     * @return bool|ObjectObject
     * @throws \Exception
     */
    public function getById($id)
    {
        $filter=["ID"=>$id];
        $list=$this->getList([],$filter);
        if(count($list)==1)
            return $list[0];
        return false;
    }

    /**
     * @param array $filter
     * @return int
     * @throws \Exception
     */
    public function getCount($filter=[]){
        $list=$this->_getList(["CNT"],$filter,[],false,false,["CNT"]);
        if(count($list)==1)
            return $list[0]['cnt'];
        else
            return 0;
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return array
     * @throws \Exception
     */
    public function _getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        if($select==[])
            $select=[
                'id',
                'name',
                'sort',
                'mix',
                'author',
                'seam_color_id',
                'seam_color_name',
                'seam_color_sort',
                'address_id',
                'address_name',
                'address_sort',
                'address_index',
                'address_region',
                'address_area',
                'address_city',
                'address_address',
                'brick_id',
                'address_regionid',
                'address_areaid',
                'address_cityid',
                'address_coords1',
                'address_coords2',
                'year_id',
                'year_property_id',
                'year_value',
                'year_def',
                'year_sort',
                'year_xml_id',
                'year_code',
                'year_iblock_id',
                'year_name',
                'object_type',
                'view_count',
            ];

        if($order===[])
            $order=[
                'sort'=>'asc',
                'name'=>'asc',
            ];

        $param=[
            'runtime'=>$this->getRuntime(),
            'select'=>$select,
            'filter'=>$filter,
            'group'=>$group,
            "cache"=>["ttl"=>3600]
        ];
        if($order)
            $param['order']=$order;
        if($limit)
            $param['limit']=$limit;
        if($offset)
            $param['offset']=$offset;

        if($param["filter"]==["ACTIVE"=>"Y"] && $param['order']==["CNT"])
            $param['runtime']=["CNT"=>$param['runtime']["CNT"]];

        $res=ObjectTable::getList($param);
        $elements=[];
        while($ob = $res->fetch(new FactoryConverter))
        {
            $newOb=$ob;

            if($newOb["mix"])
                $newOb["mix"]="Y";

            if($newOb["seam_color_id"])
            {
                $newOb["seam_color"]["id"]=$newOb["seam_color_id"];
                unset($newOb["seam_color_id"]);
            }
            if($newOb["seam_color_name"])
            {
                $newOb["seam_color"]["name"]=$newOb["seam_color_name"];
                unset($newOb["seam_color_name"]);
            }
            if($newOb["seam_color_sort"])
            {
                $newOb["seam_color"]["sort"]=$newOb["seam_color_sort"];
                unset($newOb["seam_color_sort"]);
            }

            if($newOb["address_id"])
            {
                $newOb["address"]["id"]=$newOb["address_id"];
                unset($newOb["address_id"]);
            }
            if($newOb["address_name"])
            {
                $newOb["address"]["name"]=$newOb["address_name"];
                unset($newOb["address_name"]);
            }
            if($newOb["address_sort"])
            {
                $newOb["address"]["sort"]=$newOb["address_sort"];
                unset($newOb["address_sort"]);
            }
            if($newOb["address_index"])
            {
                $newOb["address"]["index"]=$newOb["address_index"];
                unset($newOb["address_index"]);
            }
            if($newOb["address_region"])
            {
                $newOb["address"]["region"]=$newOb["address_region"];
                unset($newOb["address_region"]);
            }
            if($newOb["address_area"])
            {
                $newOb["address"]["area"]=$newOb["address_area"];
                unset($newOb["address_area"]);
            }
            if(isset($newOb["address_city"]))
            {
                $newOb["address"]["city"]=$newOb["address_city"];
                unset($newOb["address_city"]);
            }
            if($newOb["address_address"])
            {
                $newOb["address"]["address"]=$newOb["address_address"];
                unset($newOb["address_address"]);
            }

            if($newOb["address_regionid"])
            {
                $newOb["address"]["regionid"]=$newOb["address_regionid"];
                unset($newOb["address_regionid"]);
            }
            if($newOb["address_areaid"])
            {
                $newOb["address"]["areaid"]=$newOb["address_areaid"];
                unset($newOb["address_areaid"]);
            }
            if($newOb["address_cityid"])
            {
                $newOb["address"]["cityid"]=$newOb["address_cityid"];
                unset($newOb["address_cityid"]);
            }

            if($newOb["address_coords1"])
            {
                $newOb["address"]["coords1"]=$newOb["address_coords1"];
                unset($newOb["address_coords1"]);
            }

            if($newOb["address_coords2"])
            {
                $newOb["address"]["coords2"]=$newOb["address_coords2"];
                unset($newOb["address_coords2"]);
            }

            if($newOb["brick_id"])
            {
                $BrickFormatRepository=new BrickFormatRepository();
                $List=$BrickFormatRepository->_getList([],[
                    'id'=>explode(',',$newOb["brick_id"])
                ]);
                $newOb["brick"]=$List;
            }


            if($newOb["year_id"])
            {
                $newOb["year"]["id"]=$newOb["year_id"];
                unset($newOb["year_id"]);
            }
            if($newOb["year_property_id"])
            {
                $newOb["year"]["property_id"]=$newOb["year_property_id"];
                unset($newOb["year_property_id"]);
            }
            if($newOb["year_value"])
            {
                $newOb["year"]["value"]=$newOb["year_value"];
                unset($newOb["year_value"]);
            }
            if($newOb["year_def"])
            {
                $newOb["year"]["def"]=$newOb["year_def"];
                unset($newOb["year_def"]);
            }
            if($newOb["year_sort"])
            {
                $newOb["year"]["sort"]=$newOb["year_sort"];
                unset($newOb["year_sort"]);
            }
            if($newOb["year_xml_id"])
            {
                $newOb["year"]["xml_id"]=$newOb["year_xml_id"];
                unset($newOb["year_xml_id"]);
            }


            if($newOb["year_name"])
            {
                $newOb["year"]["name"]=$newOb["year_name"];
                unset($newOb["year_name"]);
            }
            if($newOb["year_iblock_id"])
            {
                $newOb["year"]["iblock_id"]=$newOb["year_iblock_id"];
                unset($newOb["year_iblock_id"]);
            }
            if($newOb["year_code"])
            {
                $newOb["year"]["code"]=$newOb["year_code"];
                unset($newOb["year_code"]);
            }

            $elements[]=array_change_key_case($newOb);
        }

        return $elements;
    }

    /**
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    private function getRuntime(){
        return [
            'CNT' => new Entity\ExpressionField('CNT', 'COUNT(*)'),

            'mix' => new Entity\ExpressionField('mix', '%s', 'PROPERTY_SIMPLE.mix'),
            'author' => new Entity\ExpressionField('author', '%s', 'PROPERTY_SIMPLE.author'),
            'object_type' => new Entity\ExpressionField('author', '%s', 'PROPERTY_SIMPLE.object_type'),
            'view_count' => new Entity\ExpressionField('view_count', '%s', 'PROPERTY_SIMPLE.view_count'),

            'seam_color_id' => new Entity\ExpressionField('seam_color_id', '%s', 'PROPERTY_SIMPLE.seam_color'),
            'seam_color' => new Entity\ReferenceField(
                'seam_color',
                SeamColorTable::getEntity(),
                array(
                    '=this.seam_color_id' => 'ref.ID',
                )
            ),
            'seam_color_name' => new Entity\ExpressionField('seam_color_name', '%s', 'seam_color.NAME'),
            'seam_color_sort' => new Entity\ExpressionField('seam_color_sort', '%s', 'seam_color.SORT'),


            'address_id' => new Entity\ExpressionField('address_id', '%s', 'PROPERTY_SIMPLE.address'),
            'address' => new Entity\ReferenceField(
                'address',
                AddressTable::getEntity(),
                array(
                    '=this.address_id' => 'ref.ID',
                )
            ),
            'address_name' => new Entity\ExpressionField('address_name', '%s', 'address.NAME'),
            'address_sort' => new Entity\ExpressionField('address_sort', '%s', 'address.SORT'),
            'address_index' => new Entity\ExpressionField('address_index', '%s', 'address.PROPERTY_SIMPLE.index'),
            'address_region' => new Entity\ExpressionField('address_region', '%s', 'address.PROPERTY_SIMPLE.region'),
            'address_area' => new Entity\ExpressionField('address_area', '%s', 'address.PROPERTY_SIMPLE.area'),
            'address_city' => new Entity\ExpressionField('address_city', '%s', 'address.PROPERTY_SIMPLE.city'),
            'address_address' => new Entity\ExpressionField('address_address', '%s', 'address.PROPERTY_SIMPLE.address'),

            'address_regionid' => new Entity\ExpressionField('address_regionid', '%s', 'address.PROPERTY_SIMPLE.regionid'),
            'address_areaid' => new Entity\ExpressionField('address_areaid', '%s', 'address.PROPERTY_SIMPLE.areaid'),
            'address_cityid' => new Entity\ExpressionField('address_cityid', '%s', 'address.PROPERTY_SIMPLE.cityid'),

            'address_coords1' => new Entity\ExpressionField('address_coords1', '%s', 'address.PROPERTY_SIMPLE.coords1'),
            'address_coords2' => new Entity\ExpressionField('address_coords2', '%s', 'address.PROPERTY_SIMPLE.coords2'),

            "brick_id" => array(
                "data_type" => "string",
                "expression" => array(
                    "GROUP_CONCAT(DISTINCT %s)",
                    "PROPERTY_MULTIPLE_brick.VALUE"
                )
            ),

            'object_brickformat_id' => new Entity\ExpressionField('object_brickformat_id', '%s', 'PROPERTY_MULTIPLE_brick.VALUE'),
            'object_brickformat' => new Entity\ReferenceField(
                'object_brickformat',
                BrickFormatTable::getEntity(),
                array(
                    '=this.object_brickformat_id' => 'ref.ID',
                )
            ),
            'object_brickformat_vendor' => new Entity\ExpressionField('object_brickformat_vendor', '%s', 'object_brickformat.PROPERTY_SIMPLE.vendor'),
            'object_brickformat_color_id' => new Entity\ExpressionField('object_brickformat_color_id', '%s', 'object_brickformat.PROPERTY_SIMPLE.color'),

            'object_brickformat_brand_id' => new Entity\ExpressionField('object_brickformat_brand_id', '%s', 'object_brickformat.PROPERTY_SIMPLE.brand'),

            'object_brickformat_name' => new Entity\ExpressionField('object_brickformat_name', '%s', 'object_brickformat.NAME'),


            "object_brickformat_name_sort" => array(
                "data_type" => "string",
                "expression" => array(
                    "GROUP_CONCAT(DISTINCT %s)",
                    "object_brickformat_name"
                )
            ),

            'year_id' => new Entity\ExpressionField('year_id', '%s', 'PROPERTY_SIMPLE.year'),
            'year' => new Entity\ReferenceField(
                'year',
                PropertyEnumerationTable::getEntity(),
                array(
                    '=this.year_id' => 'ref.ID',
                )
            ),

            'year_property_id' => new Entity\ExpressionField('year_property_id', '%s', 'year.PROPERTY_ID'),
            'year_value' => new Entity\ExpressionField('year_value', '%s', 'year.VALUE'),
            'year_def' => new Entity\ExpressionField('year_def', '%s', 'year.DEF'),
            'year_sort' => new Entity\ExpressionField('year_sort', '%s', 'year.SORT'),
            'year_xml_id' => new Entity\ExpressionField('year_xml_id', '%s', 'year.XML_ID'),


            'year_property' => new Entity\ReferenceField(
                'year_property',
                PropertyTable::getEntity(),
                array(
                    '=this.year_property_id' => 'ref.ID',
                )
            ),

            'year_code' => new Entity\ExpressionField('year_code', '%s', 'year_property.CODE'),
            'year_iblock_id' => new Entity\ExpressionField('year_iblock_id', '%s', 'year_property.IBLOCK_ID'),
            'year_name' => new Entity\ExpressionField('year_name', '%s', 'year_property.NAME'),





        ];
    }

    /**
     * @param ObjectObject $el
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public function Add(ObjectObject $el)
    {
        $CIBlockel = new \CIBlockElement;
        $arLoadProductArray=$this->getParamsUpdate($el);
        if($PRODUCT_ID = $CIBlockel->Add($arLoadProductArray))
            return $PRODUCT_ID;
        else
            throw new \InvalidArgumentException(implode(', ',$CIBlockel->LAST_ERROR));
    }

    /**
     * @param int $id
     * @param ObjectObject $el
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public function Update($id,ObjectObject $el)
    {
        $CIBlockel = new \CIBlockElement;
        $arLoadProductArray=$this->getParamsUpdate($el);
        if($CIBlockel->Update($id,$arLoadProductArray))
            return true;
        else
            throw new \InvalidArgumentException(implode(', ',$CIBlockel->LAST_ERROR));
    }

    /**
     * @return bool||int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getMixYID()
    {
        Loader::includeModule('balamarket.orm');
        $property_enums = \CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array(
            "IBLOCK_ID"=>ObjectTable::getIblockId(),
            "CODE"=>"mix",
            "VALUE"=>"Y",
        ));
        if($enum_fields = $property_enums->GetNext())
            return $enum_fields["ID"];
        return false;
    }

    /**
     * @param ObjectObject $el
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    private function getParamsUpdate(ObjectObject $el){

        $PROP = [];

        if($el->getAuthor())
            $PROP["author"]=$el->getAuthor();
        else
            $PROP["author"]=false;

        if($el->getObjectType())
            $PROP["object_type"]=$el->getObjectType();
        else
            $PROP["object_type"]=false;

        if($el->isMix())
        {
            $property_enums = \CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array(
                "IBLOCK_ID"=>ObjectTable::getIblockId(),
                "CODE"=>"mix",
                "VALUE"=>"Y",
            ));
            if($enum_fields = $property_enums->GetNext())
                $PROP["mix"]=["VALUE"=>$enum_fields["ID"]];
        }else {
            $PROP["mix"] = false;
        }

        if($el->getBrick()){
            $BrickArr=$el->getBrick();
            $BrickRepository=new BrickFormatRepository();
            $BrickId=[];
            foreach ($BrickArr as $Brick)
            {
                if($Brick->getId()>0)
                {
                    $BrickRepository->Update($Brick->getId(),$Brick);
                    $BrickId[]=$Brick->getId();
                }else{
                    $Id=$BrickRepository->Add($Brick);
                    if($Id>0)
                        $BrickId[]=$Id;
                }
            }
            $PROP["brick"]=$BrickId;
        }else{
            $PROP["brick"]=false;
        }

        if($el->getAddress()){
            $Address=$el->getAddress();
            $AddressRepository=new AddressRepository();
            if($Address->getId()>0)
            {
                $AddressRepository->Update($Address->getId(),$Address);
                $PROP["address"]=$Address->getId();
            }else{
                $Id=$AddressRepository->Add($Address);
                if($Id>0)
                    $PROP["address"]=$Id;
            }
        }else{
            $PROP["address"]=false;
        }

        if($el->getSeamColor()){
            $SeamColor=$el->getSeamColor();
            $SeamColorRepository=new SeamColorRepository();
            if($SeamColor->getId()>0)
            {
                $SeamColorRepository->Update($SeamColor->getId(),$SeamColor);
                $PROP["seam_color"]=$SeamColor->getId();
            }else{
                $Id=$SeamColorRepository->Add($SeamColor);
                if($Id>0)
                    $PROP["seam_color"]=$Id;
            }
        }else{
            $PROP["seam_color"]=false;
        }

        if($el->getYear() && $el->getYear()->getId()>0){
            $PROP["year"]=$el->getYear()->getId();
        }else{
            $PROP["year"]=false;
        }

        if($el->getViewCount()){
            $PROP["view_count"]=$el->getViewCount();
        }else{
            $PROP["view_count"]=false;
        }

        $arLoadProductArray=[
            "IBLOCK_ID"=>ObjectTable::getIblockId(),
            "ACTIVE"=>"Y",
            "NAME"=>$el->getName(),
            "PROPERTY_VALUES"=> $PROP,
        ];
        if($el->getSort()>0)
            $arLoadProductArray["SORT"]=$el->getSort();

        return $arLoadProductArray;
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function Delete($id)
    {
        if($id<=0)
            throw new \InvalidArgumentException('Invalid id');
        \CIBlockElement::Delete($id);
        return true;
    }
}