<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 17.02.2019
 * Time: 1:44
 */

namespace local\Domain\Repository;

use local\Domain\Entity\City;
use local\Domain\Factory\CityFactory;
use Bitrix\Main\Data\Cache;
use local\Kladr;

class CityRepository
{
    /**
     * @param string $ParentId
     * @return City[]
     */
    public function getListByIdRegion($ParentId){

        $cache = Cache::createInstance();
        $cacheTime = 30*60;
        $cacheId = 'CitygetListByIdRegion'.$ParentId;
        $cacheDir = 'citygetlistbyidregion';
        $arResult = [];

        if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
            $arResult = $cache->getVars();
        } elseif ($cache->startDataCache()) {

            $api = new Kladr\Api('','');
            $query = new Kladr\Query();
            $query->ParentType = Kladr\ObjectType::Region;
            $query->ParentId = $ParentId;
            $query->ContentType = Kladr\ObjectType::City;
            $arResult = $api->QueryToArray($query);

            $cache->endDataCache($arResult);
        }

        return CityFactory::createFromCollection($arResult);

    }

    /**
     * @param string $ParentId
     * @return City[]
     */
    public function getListByIdDistrict($ParentId){

        $cache = Cache::createInstance();
        $cacheTime = 30*60;
        $cacheId = 'CitygetListByIdDistrict'.$ParentId;
        $cacheDir = 'citygetlistbyiddistrict';
        $arResult = [];

        if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
            $arResult = $cache->getVars();
        } elseif ($cache->startDataCache()) {

            $api = new Kladr\Api('','');
            $query = new Kladr\Query();
            $query->ParentType = Kladr\ObjectType::District;
            $query->ParentId = $ParentId;
            $query->ContentType = Kladr\ObjectType::City;
            $arResult = $api->QueryToArray($query);

            $cache->endDataCache($arResult);
        }

        return CityFactory::createFromCollection($arResult);

    }

    /**
     * @param string $Id
     * @return bool|City
     */
    public function getById($Id){
        $cache = Cache::createInstance();
        $cacheTime = 30*60;
        $cacheId = 'CitygetListById'.$Id;
        $cacheDir = 'citygetlistbyid';
        $arResult = [];

        if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
            $arResult = $cache->getVars();
        } elseif ($cache->startDataCache()) {

            $api = new Kladr\Api('','');
            $query = new Kladr\Query();
            $query->ParentType = Kladr\ObjectType::City;
            $query->ParentId = $Id;
            $query->ContentType = Kladr\ObjectType::City;
            $arResult = $api->QueryToArray($query);

            $cache->endDataCache($arResult);
        }

        if(count($arResult)==1)
            return CityFactory::createFromArray($arResult[0]);
        else
            return false;
    }
}