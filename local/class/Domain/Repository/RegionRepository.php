<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 16.02.2019
 * Time: 21:26
 */

namespace local\Domain\Repository;

use Bitrix\Main\Loader;
use local\content\compendium\region\regionTable as RegionTable;
use local\Domain\Entity\Region;
use local\Domain\Factory\RegionFactory;
use Bitrix\Main\Entity;
use local\Domain\FactoryConverter\FactoryConverter;

class RegionRepository
{
    /**
     * BrandRepository constructor.
     * @param int $iblockId
     * @throws \Bitrix\Main\LoaderException
     */
    public function __construct($iblockId=0)
    {
        Loader::includeModule('balamarket.orm');
    }

    /**
     * @param int $id
     * @return bool|Region
     * @throws \Exception
     */
    public function getByKladr($id)
    {
        $filter=["kladr"=>$id];
        $list=$this->getList([],$filter);
        if(count($list)==1)
            return $list[0];
        return false;
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return array
     * @throws \Exception
     */
    public function _getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        if($select==[])
            $select=[
                'id',
                'name',
                'sort',
                'kladr',
            ];

        if($order===[])
            $order=[
                'sort'=>'asc',
                'name'=>'asc',
            ];

        $param=[
            'runtime'=>$this->getRuntime(),
            'select'=>$select,
            'filter'=>$filter,
            'group'=>$group,
            "cache"=>["ttl"=>3600]
        ];
        if($order)
            $param['order']=$order;
        if($limit)
            $param['limit']=$limit;
        if($offset)
            $param['offset']=$offset;

        $res=RegionTable::getList($param);
        $elements=[];
        while($ob = $res->fetch(new FactoryConverter))
            $elements[]=array_change_key_case($ob);
        return $elements;
    }

    /**
     * @param string[] $order
     * @param string[] $filter
     * @param string[] $group
     * @param int|bool $limit
     * @param int|bool $offset
     * @param string[] $select
     * @return Region[]
     * @throws \Exception
     */
    public function getList($order=[],$filter=[],$group=[],$limit=false,$offset=false,$select=[])
    {
        return RegionFactory::createFromCollection($this->_getList($order,$filter,$group,$limit,$offset,$select));
    }

    /**
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    private function getRuntime(){
        return [
            'kladr' => new Entity\ExpressionField('kladr', '%s', 'PROPERTY_SIMPLE.kladr'),
        ];
    }

    /**
     * @param Region $el
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public function Add(Region $el)
    {
        $CIBlockel = new \CIBlockElement;
        $arLoadProductArray=$this->getParamsUpdate($el);
        if($PRODUCT_ID = $CIBlockel->Add($arLoadProductArray))
            return $PRODUCT_ID;
        else
            throw new \InvalidArgumentException(implode(', ',$CIBlockel->LAST_ERROR));
    }

    /**
     * @param int $id
     * @param Region $el
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    public function Update($id,Region $el)
    {
        $CIBlockel = new \CIBlockElement;
        $arLoadProductArray=$this->getParamsUpdate($el);
        if($CIBlockel->Update($id,$arLoadProductArray))
            return true;
        else
            throw new \InvalidArgumentException(implode(', ',$CIBlockel->LAST_ERROR));
    }

    /**
     * @param Region $el
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    private function getParamsUpdate(Region $el){
        $PROP = [];
        if($el->getKladr())
            $PROP["kladr"]=$el->getKladr();
        $arLoadProductArray=[
            "IBLOCK_ID"=>RegionTable::getIblockId(),
            "ACTIVE"=>"Y",
            "NAME"=>$el->getName(),
            "PROPERTY_VALUES"=> $PROP,
        ];
        if($el->getSort()>0)
            $arLoadProductArray["SORT"]=$el->getSort();
        return $arLoadProductArray;
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function Delete($id)
    {
        if($id<=0)
            throw new \InvalidArgumentException('Invalid id');
        \CIBlockElement::Delete($id);
        return true;
    }
}