<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 27.02.2019
 * Time: 22:53
 */

namespace local\Domain\Factory;

use local\Domain\Entity\YearList;

class YearListFactory
{
    /**
     * @param array $params
     * @return YearList
     */
    public static function createFromArray(array $params)
    {
        $el=new YearList();
        if((int)$params["id"]>0)
            $el->setId((int)$params["id"]);
        if($params["name"])
            $el->setName($params["name"]);
        if((int)$params["sort"]>0)
            $el->setSort((int)$params["sort"]);
        if((int)$params["property_id"]>0)
            $el->setPropertyId((int)$params["property_id"]);
        if((int)$params["value"]>0)
            $el->setValue((int)$params["value"]);
        if((int)$params["xml_id"]>0)
            $el->setXmlId((int)$params["xml_id"]);
        if((int)$params["iblock_id"]>0)
            $el->setIblockId((int)$params["iblock_id"]);
        if($params["def"])
            $el->setDef($params["def"]);
        if($params["code"])
            $el->setCode($params["code"]);
        return $el;
    }

    /**
     * @param array $records
     * @return YearList[]
     */
    public static function createFromCollection(array $records)
    {
        $output = [];
        array_map(function ($item) use (&$output) {
            $output[] = self::createFromArray($item);
        }, $records);
        return $output;
    }
}