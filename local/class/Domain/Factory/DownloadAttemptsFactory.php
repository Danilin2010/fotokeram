<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 12:48
 */

namespace local\Domain\Factory;

use local\Domain\Entity\DownloadAttempts;
use DateTime;

class DownloadAttemptsFactory
{
    /**
     * @param array $params
     * @return DownloadAttempts
     */
    public static function createFromArray(array $params)
    {
        $el=new DownloadAttempts();
        if((int)$params["id"]>0)
            $el->setId((int)$params["id"]);
        if((int)$params["user"]>0)
            $el->setUser((int)$params["user"]);
        if($params['date'])
        {
            if($params['date'] instanceof DateTime)
                $el->setDate($params['date']);
            else
                $el->setDate(new DateTime($params['date']));
        }
        return $el;
    }

    /**
     * @param array $records
     * @return DownloadAttempts[]
     */
    public static function createFromCollection(array $records)
    {
        $output = [];
        array_map(function ($item) use (&$output) {
            $output[] = self::createFromArray($item);
        }, $records);
        return $output;
    }
}