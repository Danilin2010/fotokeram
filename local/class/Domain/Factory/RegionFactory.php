<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 16.02.2019
 * Time: 21:10
 */

namespace local\Domain\Factory;

use local\Domain\Entity\Region;

class RegionFactory
{
    /**
     * @param array $params
     * @return Region
     */
    public static function createFromArray(array $params)
    {
        $el=new Region();
        if((int)$params["id"]>0)
            $el->setId((int)$params["id"]);
        if($params["name"])
            $el->setName($params["name"]);
        if((int)$params["sort"]>0)
            $el->setSort((int)$params["sort"]);
        if($params["kladr"])
            $el->setKladr($params["kladr"]);
        return $el;
    }

    /**
     * @param array $records
     * @return Region[]
     */
    public static function createFromCollection(array $records)
    {
        $output = [];
        array_map(function ($item) use (&$output) {
            $output[] = self::createFromArray($item);
        }, $records);
        return $output;
    }
}