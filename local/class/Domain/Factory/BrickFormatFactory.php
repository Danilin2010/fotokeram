<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 12:55
 */

namespace local\Domain\Factory;

use local\Domain\Entity\BrickFormat;

class BrickFormatFactory
{
    /**
     * @param array $params
     * @return BrickFormat
     */
    public static function createFromArray(array $params)
    {
        $el=new BrickFormat();
        if((int)$params["id"]>0)
            $el->setId((int)$params["id"]);
        if($params["name"])
            $el->setName($params["name"]);
        if((int)$params["sort"]>0)
            $el->setSort((int)$params["sort"]);
        if($params["vendor"])
            $el->setVendor($params["vendor"]);
        if($params["format"])
            $el->setFormat(BrickFormatFormatFactory::createFromArray($params["format"]));
        if($params["color"])
            $el->setColor(BrickColorFactory::createFromArray($params["color"]));
        if($params["brand"])
            $el->setBrand(BrandFactory::createFromArray($params["brand"]));
        return $el;
    }

    /**
     * @param array $records
     * @return BrickFormat[]
     */
    public static function createFromCollection(array $records)
    {
        $output = [];
        array_map(function ($item) use (&$output) {
            $output[] = self::createFromArray($item);
        }, $records);
        return $output;
    }
}