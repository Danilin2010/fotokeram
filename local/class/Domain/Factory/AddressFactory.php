<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 15:52
 */

namespace local\Domain\Factory;

use local\Domain\Entity\Address;

class AddressFactory
{
    /**
     * @param array $params
     * @return Address
     */
    public static function createFromArray(array $params)
    {
        $el=new Address();
        if((int)$params["id"]>0)
            $el->setId((int)$params["id"]);
        if($params["name"])
            $el->setName($params["name"]);
        if((int)$params["sort"]>0)
            $el->setSort((int)$params["sort"]);
        if($params["index"])
            $el->setIndex($params["index"]);
        if($params["region"])
            $el->setRegion($params["region"]);
        if($params["area"])
            $el->setArea($params["area"]);
        if(isset($params["city"]))
            $el->setCity($params["city"]);
        if($params["address"])
            $el->setAddress($params["address"]);
        if($params["regionid"])
            $el->setRegionId($params["regionid"]);
        if($params["areaid"])
            $el->setAreaId($params["areaid"]);
        if($params["cityid"])
            $el->setCityId($params["cityid"]);
        if($params["coords1"])
            $el->setCoords1($params["coords1"]);
        if($params["coords2"])
            $el->setCoords2($params["coords2"]);

        return $el;
    }

    /**
     * @param array $records
     * @return Address[]
     */
    public static function createFromCollection(array $records)
    {
        $output = [];
        array_map(function ($item) use (&$output) {
            $output[] = self::createFromArray($item);
        }, $records);
        return $output;
    }
}