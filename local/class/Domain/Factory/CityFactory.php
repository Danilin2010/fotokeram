<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 17.02.2019
 * Time: 1:43
 */

namespace local\Domain\Factory;

use local\Domain\Entity\City;

class CityFactory
{
    /**
     * @param array $params
     * @return City
     */
    public static function createFromArray(array $params)
    {
        $el=new City();
        if($params["id"]>0)
            $el->setId($params["id"]);
        if($params["name"])
            $el->setName($params["name"]);
        if($params["zip"])
            $el->setZip($params["zip"]);
        if($params["type"])
            $el->setType($params["type"]);
        if($params["typeShort"])
            $el->setTypeShort($params["typeShort"]);
        if($params["okato"])
            $el->setOkato($params["okato"]);
        if($params["contentType"])
            $el->setContentType($params["contentType"]);
        return $el;
    }

    /**
     * @param array $records
     * @return City[]
     */
    public static function createFromCollection(array $records)
    {
        $output = [];
        array_map(function ($item) use (&$output) {
            $output[] = self::createFromArray($item);
        }, $records);
        return $output;
    }
}