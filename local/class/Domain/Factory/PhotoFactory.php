<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 13:07
 */

namespace local\Domain\Factory;

use local\Domain\Entity\Photo;
use Bitrix\Main\IO\File;
use Bitrix\Main\Application;

class PhotoFactory
{
    /**
     * @param array $params
     * @return Photo
     */
    public static function createFromArray(array $params)
    {
        $el=new Photo();
        if((int)$params["id"]>0)
            $el->setId((int)$params["id"]);
        if($params["name"])
            $el->setName($params["name"]);
        if((int)$params["sort"]>0)
            $el->setSort((int)$params["sort"]);
        if($params["photo"])
        {
            if($params["photo"] instanceof File)
                $el->setPhoto($params["photo"]);
            elseif((int)$params["photo"]>0){
                $el->setPhoto(new File(Application::getDocumentRoot().\CFile::GetPath((int)$params["photo"])));
                $el->setPhotoId((int)$params["photo"]);
            }
        }
        if($params["object"])
            $el->setObject(ObjectFactory::createFromCollection($params["object"]));
        if((int)$params["view_count"]>0)
            $el->setViewCount((int)$params["view_count"]);
        return $el;
    }

    /**
     * @param array $records
     * @return Photo[]
     */
    public static function createFromCollection(array $records)
    {
        $output = [];
        array_map(function ($item) use (&$output) {
            $output[] = self::createFromArray($item);
        }, $records);
        return $output;
    }
}