<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 12:48
 */

namespace local\Domain\Factory;

use local\Domain\Entity\ObjectType;

class ObjectTypeFactory
{
    /**
     * @param array $params
     * @return ObjectType
     */
    public static function createFromArray(array $params)
    {
        $el=new ObjectType();
        if((int)$params["id"]>0)
            $el->setId((int)$params["id"]);
        if($params["name"])
            $el->setName($params["name"]);
        if((int)$params["sort"]>0)
            $el->setSort((int)$params["sort"]);
        return $el;
    }

    /**
     * @param array $records
     * @return ObjectType[]
     */
    public static function createFromCollection(array $records)
    {
        $output = [];
        array_map(function ($item) use (&$output) {
            $output[] = self::createFromArray($item);
        }, $records);
        return $output;
    }
}