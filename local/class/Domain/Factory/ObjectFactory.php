<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 13:00
 */

namespace local\Domain\Factory;

use local\Domain\Entity\ObjectObject;

class ObjectFactory
{
    /**
     * @param array $params
     * @return ObjectObject
     */
    public static function createFromArray(array $params)
    {
        $el=new ObjectObject();
        if((int)$params["id"]>0)
            $el->setId((int)$params["id"]);
        if($params["name"])
            $el->setName($params["name"]);
        if((int)$params["sort"]>0)
            $el->setSort((int)$params["sort"]);
        if($params["mix"]=="Y")
            $el->setMix(true);
        if((int)$params["author"]>0)
            $el->setAuthor((int)$params["author"]);
        if($params["address"])
            $el->setAddress(AddressFactory::createFromArray($params["address"]));
        if($params["seam_color"])
            $el->setSeamColor(SeamColorFactory::createFromArray($params["seam_color"]));
        if($params["brick"])
            $el->setBrick(BrickFormatFactory::createFromCollection($params["brick"]));
        if($params["year"])
            $el->setYear(YearListFactory::createFromArray($params["year"]));
        if((int)$params["object_type"]>0)
            $el->setObjectType((int)$params["object_type"]);
        if((int)$params["view_count"]>0)
            $el->setViewCount((int)$params["view_count"]);
        return $el;
    }

    /**
     * @param array $records
     * @return ObjectObject[]
     */
    public static function createFromCollection(array $records)
    {
        $output = [];
        array_map(function ($item) use (&$output) {
            $output[] = self::createFromArray($item);
        }, $records);
        return $output;
    }
}