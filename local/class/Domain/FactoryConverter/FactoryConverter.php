<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.02.2019
 * Time: 18:05
 */

namespace local\Domain\FactoryConverter;

class FactoryConverter extends \Bitrix\Main\Text\Converter
{
    public function encode($text, $textType = "")
    {
        $unserializeText=unserialize($text);
        if($unserializeText)
        {
            if($unserializeText["TEXT"])
                return $unserializeText["TEXT"];
            return $unserializeText;
        }else
            return $text;
    }

    public function decode($text, $textType = "")
    {
        return $text;
    }
}