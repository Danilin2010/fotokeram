<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 16.02.2019
 * Time: 13:19
 */
namespace local\Kladr;
/**
 * Перечисление типов объектов
 */
class ObjectType
{
    /**
     * Регион
     */
    const Region = 'region';

    /**
     * Район
     */
    const District = 'district';

    /**
     * Населённый пункт
     */
    const City = 'city';

    /**
     * Улица
     */
    const Street = 'street';

    /**
     * Строение
     */
    const Building = 'building';
}
