<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 02.03.2019
 * Time: 20:04
 */

namespace local\Helpers\Supporting;

use local\Domain\Repository\ObjectRepository;

class SupportingFilter
{

    /**
     * @param $Inbox
     * @param $ClearName
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getFilter($Inbox,$ClearName=false){
        if($ClearName)
            unset($Inbox[$ClearName]);
        $filter=[];
        foreach ($Inbox as $key=>$val){
            if(count($val)==1 && !$val[0])
                continue;
            switch ($key) {
                case "ObjectId":
                    $filter["object_xmlid"]=$val;
                    break;
                case "Brand":
                    $filter["object_brickformat_brand_id"]=$val;
                    break;
                case "BrickFormatVendor":
                    $filter["object_brickformat_id"]=$val;
                    break;
                case "BrickFormatFormat":
                    $filter["object_brickformat_format_id"]=$val;
                    break;
                case "BrickFormatСolor":
                    $filter["object_brickformat_color_id"]=$val;
                    break;
                case "SeamColor":
                    $filter["seam_color_id"]=$val;
                    break;
                case "RegionId":
                    $filter["object_address_regionid"]=$val;
                    break;
                case "AreaId":
                    $filter["object_address_areaid"]=$val;
                    break;
                case "CityId":
                    $filter["object_address_cityid"]=$val;
                    break;
                case "Year":
                    $filter["object_year"]=$val;
                    break;
                case "USER":
                    $filter["author"]=$val;
                    break;
                case "Mix":
                    if($val=="Y") {
                        $filter["mix"] = ObjectRepository::getMixYID();
                    }elseif($val=="N") {
                        $filter["mix"] = false;
                    }
                    break;
                case "objecttype":
                    $filter["object_type"]=$val;
                    break;
            }
        }
        $filter["!object_xmlid"]=false;
        //$filter["!object_name"]=false;
        $filter["ACTIVE"]='Y';
        return $filter;
    }

    /**
     * @param $Inbox
     * @param $ClearName
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getFilterObject($Inbox,$ClearName=false){
        if($ClearName)
            unset($Inbox[$ClearName]);
        $filter=[];
        foreach ($Inbox as $key=>$val){
            if(count($val)==1 && !$val[0])
                continue;
            switch ($key) {
                case "ObjectId":
                    $filter["id"]=$val;
                    break;
                case "Brand":
                    $filter["object_brickformat_brand_id"]=$val;
                    break;
                case "BrickFormatVendor":
                    $filter["object_brickformat_id"]=$val;
                    break;
                case "BrickFormatFormat":
                    $filter["object_brickformat_id"]=$val;
                    break;
                case "BrickFormatСolor":
                    $filter["object_brickformat_color_id"]=$val;
                    break;
                case "SeamColor":
                    $filter["seam_color_id"]=$val;
                    break;
                case "RegionId":
                    $filter["address_regionid"]=$val;
                    break;
                case "AreaId":
                    $filter["address_areaid"]=$val;
                    break;
                case "CityId":
                    $filter["address_cityid"]=$val;
                    break;
                case "Year":
                    $filter["year_id"]=$val;
                    break;
                case "USER":
                    $filter["author"]=$val;
                case "Mix":
                    if($val=="Y") {
                        $filter["mix"] = ObjectRepository::getMixYID();
                    }elseif($val=="N") {
                        $filter["mix"] = false;
                    }
                    break;
                case "objecttype":
                    $filter["object_type"]=$val;
                    break;
            }
        }
        return $filter;
    }

}