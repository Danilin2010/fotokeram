<?php

namespace local\Helpers;

use Bitrix\Main\EventManager;

class SetEvents
{

    public static function init()
    {

        EventManager::getInstance()->addEventHandlerCompatible (
            "main",
            "OnBeforeUserUpdate",
            array(
                "local\\Services\\UserServices",
                "OnBeforeUserUpdateHandler"
            )
        );

        EventManager::getInstance()->addEventHandlerCompatible (
            "main",
            "OnBeforeProlog",
            array(
                "local\\Helpers\\SetConst",
                "CustomConst"
            )
        );

    }

}