<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 03.06.2019
 * Time: 2:04
 */

namespace local\Helpers;

use CFile,
    CBXVirtualIo;

class Tools
{

    public static function InputFile($strFieldName, $int_field_size, $strImageID, $strImageStorePath=false, $int_max_file_size=0, $strFileType="IMAGE", $field_file="class=typefile", $description_size=0, $field_text="class=typeinput", $field_checkbox="", $bShowNotes = true, $bShowFilePath = true)
    {
        $strReturn1 = "";
        if($int_max_file_size != 0)
            $strReturn1 .= "<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"".$int_max_file_size."\" /> ";

        $db_img_arr = CFile::GetFileArray($strImageID, $strImageStorePath);

        $fileName="";

        if($db_img_arr)
        {
            if($bShowNotes)
            {
                if($bShowFilePath)
                {
                    $filePath = $db_img_arr["SRC"];
                }
                else
                {
                    $filePath = $db_img_arr['ORIGINAL_NAME'];
                }
                $io = CBXVirtualIo::GetInstance();
                if($io->FileExists($_SERVER["DOCUMENT_ROOT"].$db_img_arr["SRC"]) || $db_img_arr["HANDLER_ID"])
                {
                    $fileName .= htmlspecialcharsEx($filePath);
                    /*if(strtoupper($strFileType)=="IMAGE")
                    {
                        $intWidth = intval($db_img_arr["WIDTH"]);
                        $intHeight = intval($db_img_arr["HEIGHT"]);
                        if($intWidth>0 && $intHeight>0)
                        {
                            $strReturn2 .= "<br>&nbsp;".GetMessage("FILE_WIDTH").": $intWidth";
                            $strReturn2 .= "<br>&nbsp;".GetMessage("FILE_HEIGHT").": $intHeight";
                        }
                    }*/
                    $fileName .= ' '.GetMessage("FILE_SIZE").": ".CFile::FormatSize($db_img_arr["FILE_SIZE"]);
                }
                else
                {
                    $fileName .= GetMessage("FILE_NOT_FOUND").": ".htmlspecialcharsEx($filePath);
                }
            }
        }

        if(strlen($fileName)<=0)
            $fileName="Файл не выбран";


        $strReturn1 .= '
                                    <div class="input-file-container">
                        <p class="file-return">'.$fileName.'</p>
                        <input name = "'.$strFieldName.'" '.$field_file.' class="input-file" id="my-file" type="file">
                        <label tabindex="0" for="my-file" class="input-file-trigger">Обзор...</label>
                    </div>
         
         ';
        $strReturn2 = '<span class="bx-input-file-desc">';
        $strDescription = "";


        if($db_img_arr)
        {
            $strDescription = $db_img_arr["DESCRIPTION"];

            if(($p=strpos($strFieldName, "["))>0)
            {
                $strDelName = substr($strFieldName, 0, $p)."_del".substr($strFieldName, $p);
            }
            else
            {
                $strDelName = $strFieldName."_del";
            }

            /*if($bShowNotes)
            {
                if($bShowFilePath)
                {
                    $filePath = $db_img_arr["SRC"];
                }
                else
                {
                    $filePath = $db_img_arr['ORIGINAL_NAME'];
                }
                $io = CBXVirtualIo::GetInstance();
                if($io->FileExists($_SERVER["DOCUMENT_ROOT"].$db_img_arr["SRC"]) || $db_img_arr["HANDLER_ID"])
                {
                    $strReturn2 .= "<br>&nbsp;".GetMessage("FILE_TEXT").": ".htmlspecialcharsEx($filePath);
                    if(strtoupper($strFileType)=="IMAGE")
                    {
                        $intWidth = intval($db_img_arr["WIDTH"]);
                        $intHeight = intval($db_img_arr["HEIGHT"]);
                        if($intWidth>0 && $intHeight>0)
                        {
                            $strReturn2 .= "<br>&nbsp;".GetMessage("FILE_WIDTH").": $intWidth";
                            $strReturn2 .= "<br>&nbsp;".GetMessage("FILE_HEIGHT").": $intHeight";
                        }
                    }
                    $strReturn2 .= "<br>&nbsp;".GetMessage("FILE_SIZE").": ".CFile::FormatSize($db_img_arr["FILE_SIZE"]);
                }
                else
                {
                    $strReturn2 .= "<br>".GetMessage("FILE_NOT_FOUND").": ".htmlspecialcharsEx($filePath);
                }
            }*/
            $strReturn2 .= "
            <label for=\"".$strDelName."\" class=\"container-check\" style=\"float: inherit;max-width: 50%;\">".GetMessage("FILE_DELETE")."
                        <input ".$field_checkbox." type=\"checkbox\" id=\"".$strDelName."\" name=\"".$strDelName."\" value=\"Y\" />
                        <span class=\"checkmark\"></span>
                    </label>";
        }

        $strReturn2 .= '</span>';

        return $strReturn1.(
            $description_size > 0?
                '<br><input type="text" value="'.htmlspecialcharsbx($strDescription).'" name="'.$strFieldName.'_descr" '.$field_text.' size="'.$description_size.'" title="'.GetMessage("MAIN_FIELD_FILE_DESC").'" />'
                :''
            ).$strReturn2;
    }

}