<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 06.03.2019
 * Time: 21:06
 */

namespace local\Helpers;

use Bitrix\Main\UserTable;

class Watermark
{
    /**
     * @param int $UserId
     * @param float $coefficient
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function GetWatermark($UserId=0,$coefficient=0.90)
    {
        $arFilter_watermark=[];
        if($UserId>0){
            $rsUser = UserTable::getList(
                [
                    'filter'=>[
                        'ID'=>$UserId,
                    ],
                    "cache"=>["ttl"=>3600],
                    'select'=>[
                        'UF_WATERMARK',
                    ]
                ]
            );
            if($arUser = $rsUser->Fetch())
            {
                $WatermarkId=(int)$arUser["UF_WATERMARK"];
                if($WatermarkId>0){
                    $WatermarkPath=\CFile::GetPath($WatermarkId);
                    $arFilter_watermark[]=[
                        'fill' => 'resize',
                        'name' => 'watermark',
                        'position' => 'center',
                        'coefficient' => 0.90,
                        'type' => 'image',
                        'alpha_level' => '100',
                        'file' => $_SERVER['DOCUMENT_ROOT'].$WatermarkPath,
                    ];
                }
            }
        }else{
            $arFilter_watermark[]=[
                'fill' => 'resize',
                'name' => 'watermark',
                'position' => 'center',
                'coefficient' => 0.90,
                'type' => 'image',
                'alpha_level' => '100',
                'file' => $_SERVER['DOCUMENT_ROOT'].'/upload/watermark.png'
            ];
        }
        return $arFilter_watermark;
    }
}