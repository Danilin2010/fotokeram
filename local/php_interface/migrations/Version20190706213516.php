<?php

namespace Sprint\Migration;


class Version20190706213516 extends Version
{

    protected $description = "Тип объекта в объекте";

    public function up() {
        $helper = new HelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists('object','fotokeram');

        $helper->Iblock()->saveProperty($iblockId, array (
            'NAME' => 'Тип объекта',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'object_type',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'E',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => 'object_type',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => 'fotokeram:object_type',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));

        $helper->AdminIblock()->saveElementForm($iblockId, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'SORT' => 'Сортировка',
                    'NAME' => 'Название',
                    'PROPERTY_brick' => 'Кирпич',
                    'PROPERTY_mix' => 'МИКС',
                    'PROPERTY_year' => 'Год',
                    'PROPERTY_seam_color' => 'Цвет шва',
                    'PROPERTY_address' => 'Адрес',
                    'PROPERTY_author' => 'Автор',
                    'PROPERTY_object_type' => 'Тип объекта',
                ),
        ));

    }

    public function down() {
        $helper = new HelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists('object','fotokeram');

        $helper->Iblock()->deletePropertyIfExists($iblockId,'object_type');

        $helper->AdminIblock()->saveElementForm($iblockId, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'SORT' => 'Сортировка',
                    'NAME' => 'Название',
                    'PROPERTY_brick' => 'Кирпич',
                    'PROPERTY_mix' => 'МИКС',
                    'PROPERTY_year' => 'Год',
                    'PROPERTY_seam_color' => 'Цвет шва',
                    'PROPERTY_address' => 'Адрес',
                    'PROPERTY_author' => 'Автор',
                ),
        ));

    }

}
