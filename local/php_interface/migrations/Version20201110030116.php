<?php

namespace Sprint\Migration;


class Version20201110030116 extends Version
{

    protected $description = "Координаты адреса";

    public function up()
    {
        $helper = new HelperManager();


        $iblockId = $helper->Iblock()->getIblockIdIfExists('address', 'fotokeram');


        $helper->Iblock()->saveProperty($iblockId, array(
            'NAME' => 'coords1',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'coords1',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'N',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => NULL,
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));
        $helper->Iblock()->saveProperty($iblockId, array(
            'NAME' => 'coords2',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'coords2',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'N',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => NULL,
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));


    }

    public function down()
    {
        $helper = new HelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists('address', 'fotokeram');
        $helper->Iblock()->deletePropertyIfExists($iblockId,"coords1");
        $helper->Iblock()->deletePropertyIfExists($iblockId,"coords2");
    }

}
