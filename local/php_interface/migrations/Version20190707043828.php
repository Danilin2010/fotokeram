<?php

namespace Sprint\Migration;


class Version20190707043828 extends Version
{

    protected $description = "Редактор";

    public function up() {
        $helper = new HelperManager();

        $helper->UserGroup()->saveGroup('editor',array (
            'ACTIVE' => 'Y',
            'C_SORT' => '100',
            'ANONYMOUS' => 'N',
            'NAME' => 'Редактор',
            'DESCRIPTION' => '',
            'SECURITY_POLICY' =>
                array (
                ),
        ));
    }

    public function down() {
        $helper = new HelperManager();

        $helper->UserGroup()->deleteGroup('editor');

    }

}
