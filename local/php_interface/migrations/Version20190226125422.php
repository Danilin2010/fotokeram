<?php

namespace Sprint\Migration;


class Version20190226125422 extends Version
{

    protected $description = "Бренд в тип кирпича";

    public function up() {

        $helper = new HelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists('brick_format','fotokeram');

        $helper->Iblock()->updateIblock($iblockId,array (
            'NAME' => 'Карточка продукта',
        ));

        $helper->Iblock()->saveProperty($iblockId, array (
            'NAME' => 'Бренд',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'brand',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'E',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => 'brand',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => 'fotokeram:brand',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'Y',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));

        $helper->AdminIblock()->saveElementForm($iblockId, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'NAME' => 'Название',
                    'SORT' => 'Сортировка',
                    'PROPERTY_brand' => 'Бренд',
                    'PROPERTY_vendor' => 'Артикул',
                    'PROPERTY_format' => 'Формат кирпича',
                    'PROPERTY_color' => 'Цвет кирпича',
                ),
        ));

        $iblockId = $helper->Iblock()->getIblockIdIfExists('object','fotokeram');

        $helper->Iblock()->deletePropertyIfExists($iblockId,'brand');

        $helper->AdminIblock()->saveElementForm($iblockId, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'SORT' => 'Сортировка',
                    'NAME' => 'Название',
                    'PROPERTY_brick' => 'Кирпич',
                    'PROPERTY_mix' => 'МИКС',
                    'PROPERTY_seam_color' => 'Цвет шва',
                    'PROPERTY_address' => 'Адрес',
                    'PROPERTY_author' => 'Автор',
                ),
        ));

    }

    public function down() {

        $helper = new HelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists('brick_format','fotokeram');

        $helper->Iblock()->updateIblock($iblockId,array (
            'NAME' => 'Тип кирпича',
        ));

        $helper->Iblock()->deletePropertyIfExists($iblockId,'brand');

        $helper->AdminIblock()->saveElementForm($iblockId, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'NAME' => 'Название',
                    'SORT' => 'Сортировка',
                    'PROPERTY_vendor' => 'Артикул',
                    'PROPERTY_format' => 'Формат кирпича',
                    'PROPERTY_color' => 'Цвет кирпича',
                ),
        ));

        $iblockId = $helper->Iblock()->getIblockIdIfExists('object','fotokeram');

        $helper->Iblock()->saveProperty($iblockId, array (
            'NAME' => 'Бренд',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'brand',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'E',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => 'brand',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => 'fotokeram:brand',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'Y',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));

        $helper->AdminIblock()->saveElementForm($iblockId, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'SORT' => 'Сортировка',
                    'NAME' => 'Название',
                    'PROPERTY_brand' => 'Бренд',
                    'PROPERTY_brick' => 'Кирпич',
                    'PROPERTY_mix' => 'МИКС',
                    'PROPERTY_seam_color' => 'Цвет шва',
                    'PROPERTY_address' => 'Адрес',
                    'PROPERTY_author' => 'Автор',
                ),
        ));


    }

}
