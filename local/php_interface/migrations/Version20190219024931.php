<?php

namespace Sprint\Migration;


class Version20190219024931 extends Version
{

    protected $description = "Новые поля для Адреса";

    public function up() {
        $helper = new HelperManager();


        $iblockId = $helper->Iblock()->getIblockIdIfExists('address','fotokeram');


        $helper->Iblock()->saveProperty($iblockId, array (
            'NAME' => 'Регион КЛАДР',
            'ACTIVE' => 'Y',
            'SORT' => '210',
            'CODE' => 'regionid',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => 'regionid',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));
        $helper->Iblock()->saveProperty($iblockId, array (
            'NAME' => 'Район КЛАДР',
            'ACTIVE' => 'Y',
            'SORT' => '310',
            'CODE' => 'areaid',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => 'areaid',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));
        $helper->Iblock()->saveProperty($iblockId, array (
            'NAME' => 'Город КЛАДР',
            'ACTIVE' => 'Y',
            'SORT' => '410',
            'CODE' => 'cityid',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => 'cityid',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));

        $helper->AdminIblock()->saveElementForm($iblockId, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'NAME' => 'Название',
                    'SORT' => 'Сортировка',
                    'PROPERTY_index' => 'Индекс',
                    'PROPERTY_region' => 'Регион',
                    'PROPERTY_regionid' => 'Регион КЛАДР',
                    'PROPERTY_area' => 'Район',
                    'PROPERTY_areaid' => 'Район КЛАДР',
                    'PROPERTY_city' => 'Город',
                    'PROPERTY_cityid' => 'Город КЛАДР',
                    'PROPERTY_address' => 'Адрес',
                ),
        ));

    }

    public function down() {
        $helper = new HelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists('address','fotokeram');
        $helper->Iblock()->deletePropertyIfExists($iblockId,'regionid');
        $helper->Iblock()->deletePropertyIfExists($iblockId,'areaid');
        $helper->Iblock()->deletePropertyIfExists($iblockId,'cityid');

        $helper->AdminIblock()->saveElementForm($iblockId, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'NAME' => 'Название',
                    'SORT' => 'Сортировка',
                    'PROPERTY_index' => 'Индекс',
                    'PROPERTY_region' => 'Регион',
                    'PROPERTY_area' => 'Район',
                    'PROPERTY_city' => 'Город',
                    'PROPERTY_address' => 'Адрес',
                ),
        ));

    }

}
