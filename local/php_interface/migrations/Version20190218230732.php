<?php

namespace Sprint\Migration;


class Version20190218230732 extends Version
{

    protected $description = "Тип кирпича в справочник";

    public function up() {
        $helper = new HelperManager();

        $iblockIdBrickFormat=$helper->Iblock()->getIblockId('brick_format','fotokeram');
        $iblockIdBrickFormatFormat=$helper->Iblock()->getIblockId('brick_format_format','fotokeram');

        $Elements=$helper->Iblock()->getElements($iblockIdBrickFormat,[],[
            'PROPERTY_format',
        ]);

        $format=[];

        $helper->Iblock()->deletePropertyIfExists($iblockIdBrickFormat,'format');
        $helper->Iblock()->saveProperty($iblockIdBrickFormat, array (
            'NAME' => 'Тип кирпича',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'format',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'E',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => 'format',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => 'fotokeram:brick_format_format',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'Y',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));

        foreach ($Elements as $Element)
        {
            if(!$format[$Element["PROPERTY_FORMAT_VALUE"]])
            {
                $format[$Element["PROPERTY_FORMAT_VALUE"]]=$helper->Iblock()->addElement($iblockIdBrickFormatFormat,[
                    "NAME" => $Element["PROPERTY_FORMAT_VALUE"],
                    "SORT" => 500,
                ]);
            }
            $helper->Iblock()->updateElement($Element["ID"],["IBLOCK_ID"=>$Element["IBLOCK_ID"]],["format"=>$format[$Element["PROPERTY_FORMAT_VALUE"]]]);
        }

        $helper->AdminIblock()->saveElementForm($iblockIdBrickFormat, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'NAME' => 'Название',
                    'SORT' => 'Сортировка',
                    'PROPERTY_vendor' => 'Артикул',
                    'PROPERTY_format' => 'Тип кирпича',
                    'PROPERTY_color' => 'Цвет кирпича',
                ),
        ));

    }

    public function down() {
        $helper = new HelperManager();

        $iblockIdBrickFormat=$helper->Iblock()->getIblockId('brick_format','fotokeram');
        $iblockIdBrickFormatFormat=$helper->Iblock()->getIblockId('brick_format_format','fotokeram');

        $Elements=$helper->Iblock()->getElements($iblockIdBrickFormat,[],[
            'PROPERTY_format.NAME',
        ]);

        $helper->Iblock()->deletePropertyIfExists($iblockIdBrickFormat,'format');
        $helper->Iblock()->saveProperty($iblockIdBrickFormat, array (
            'NAME' => 'Тип кирпича',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'format',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => 'format',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'Y',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));

        foreach ($Elements as $Element)
            $helper->Iblock()->updateElement($Element["ID"],["IBLOCK_ID"=>$Element["IBLOCK_ID"]],["format"=>$Element["PROPERTY_FORMAT_NAME"]]);

        $helper->AdminIblock()->saveElementForm($iblockIdBrickFormat, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'NAME' => 'Название',
                    'SORT' => 'Сортировка',
                    'PROPERTY_vendor' => 'Артикул',
                    'PROPERTY_format' => 'Тип кирпича',
                    'PROPERTY_color' => 'Цвет кирпича',
                ),
        ));

        $ElementsFormat=$helper->Iblock()->getElements($iblockIdBrickFormatFormat,[]);
        foreach ($ElementsFormat as $Element)
            $helper->Iblock()->deleteElement($Element["ID"]);

    }
}
