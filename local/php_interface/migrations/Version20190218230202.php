<?php

namespace Sprint\Migration;


class Version20190218230202 extends Version
{

    protected $description = "Формат кирпича -> Тип кирпича";

    public function up() {
        $helper = new HelperManager();

        $iblockId=$helper->Iblock()->getIblockId('brick_format','fotokeram');
        $helper->Iblock()->updateIblock($iblockId,array (
            'NAME' => 'Тип кирпича',
        ));

    }

    public function down() {
        $helper = new HelperManager();

        $iblockId=$helper->Iblock()->getIblockId('brick_format','fotokeram');
        $helper->Iblock()->updateIblock($iblockId,array (
            'NAME' => 'Формат кирпича',
        ));

    }

}
