<?php

namespace Sprint\Migration;


class Version20190227212550 extends Version
{

    protected $description = "Добавление года постройки";

    public function up() {
        $helper = new HelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists('object','fotokeram');

        $value=[];
        for($i=1980;$i<=2030;$i++)
            $value[]=[
                'VALUE' => $i,
                'DEF' => 'N',
                'SORT' => $i,
                'XML_ID' => $i,
            ];

        $helper->Iblock()->saveProperty($iblockId, array (
            'NAME' => 'Год',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'year',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'L',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => 'year',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
            'VALUES' =>$value,
        ));

        $helper->AdminIblock()->saveElementForm($iblockId, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'SORT' => 'Сортировка',
                    'NAME' => 'Название',
                    'PROPERTY_brick' => 'Кирпич',
                    'PROPERTY_mix' => 'МИКС',
                    'PROPERTY_year' => 'Год',
                    'PROPERTY_seam_color' => 'Цвет шва',
                    'PROPERTY_address' => 'Адрес',
                    'PROPERTY_author' => 'Автор',
                ),
        ));

    }

    public function down() {
        $helper = new HelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists('object','fotokeram');

        $helper->Iblock()->deletePropertyIfExists($iblockId,'year');

        $helper->AdminIblock()->saveElementForm($iblockId, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'SORT' => 'Сортировка',
                    'NAME' => 'Название',
                    'PROPERTY_brick' => 'Кирпич',
                    'PROPERTY_mix' => 'МИКС',
                    'PROPERTY_seam_color' => 'Цвет шва',
                    'PROPERTY_address' => 'Адрес',
                    'PROPERTY_author' => 'Автор',
                ),
        ));

    }

}
