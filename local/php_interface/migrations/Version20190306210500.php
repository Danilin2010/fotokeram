<?php

namespace Sprint\Migration;


class Version20190306210500 extends Version
{

    protected $description = "Ватермарк";

    public function up() {
        $helper = new HelperManager();

        $helper->UserTypeEntity()->saveUserTypeEntity(array (
            'ENTITY_ID' => 'USER',
            'FIELD_NAME' => 'UF_WATERMARK',
            'USER_TYPE_ID' => 'file',
            'XML_ID' => 'UF_WATERMARK',
            'SORT' => '100',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' =>
                array (
                    'SIZE' => 20,
                    'LIST_WIDTH' => 200,
                    'LIST_HEIGHT' => 200,
                    'MAX_SHOW_SIZE' => 0,
                    'MAX_ALLOWED_SIZE' => 0,
                    'EXTENSIONS' =>
                        array (
                        ),
                ),
            'EDIT_FORM_LABEL' =>
                array (
                    'en' => '',
                    'ru' => 'Ватермарк',
                ),
            'LIST_COLUMN_LABEL' =>
                array (
                    'en' => '',
                    'ru' => 'Ватермарк',
                ),
            'LIST_FILTER_LABEL' =>
                array (
                    'en' => '',
                    'ru' => 'Ватермарк',
                ),
            'ERROR_MESSAGE' =>
                array (
                    'en' => '',
                    'ru' => '',
                ),
            'HELP_MESSAGE' =>
                array (
                    'en' => '',
                    'ru' => '',
                ),
        ));
    }

    public function down() {
        $helper = new HelperManager();

        $helper->UserTypeEntity()->deleteUserTypeEntityIfExists('USER','UF_WATERMARK');

    }

}
