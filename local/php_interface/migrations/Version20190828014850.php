<?php

namespace Sprint\Migration;


class Version20190828014850 extends Version
{

    protected $description = "Фото: Просмотры";

    public function up() {
        $helper = new HelperManager();


        $iblockId = $helper->Iblock()->getIblockIdIfExists('photo','fotokeram');


        $helper->Iblock()->saveProperty($iblockId, array (
            'NAME' => 'Просмотры',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'view_count',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'N',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => 'view_count',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));

        $helper->AdminIblock()->saveElementForm($iblockId, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'PROPERTY_view_count' => 'Просмотры',
                    'NAME' => 'Название',
                    'SORT' => 'Сортировка',
                    'PROPERTY_object' => 'Объект',
                    'PROPERTY_photo' => 'Файл',
                ),
        ));

    }

    public function down() {
        $helper = new HelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists('photo','fotokeram');

        $helper->Iblock()->deletePropertyIfExists($iblockId,'view_count');

        $helper->AdminIblock()->saveElementForm($iblockId, array (
            'Элемент' =>
                array (
                    'ID' => 'ID',
                    'ACTIVE' => 'Активность',
                    'NAME' => 'Название',
                    'SORT' => 'Сортировка',
                    'PROPERTY_object' => 'Объект',
                    'PROPERTY_photo' => 'Файл',
                ),
        ));

    }

}
