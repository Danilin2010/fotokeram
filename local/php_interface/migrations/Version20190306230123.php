<?php

namespace Sprint\Migration;


class Version20190306230123 extends Version
{

    protected $description = "Почтовое событие \"Выбранные фотографии\"";

    public function up() {
        $helper = new HelperManager();

        $helper->Event()->saveEventType('PHOTO_CART_MAIL', array (
            'LID' => 'ru',
            'NAME' => 'Выбранные фотографии',
            'DESCRIPTION' => '
#EMAIL_TO# - Email-адрес',
            'SORT' => '150',
        ));
        $helper->Event()->saveEventType('PHOTO_CART_MAIL', array (
            'LID' => 'en',
            'NAME' => 'Selected photos',
            'DESCRIPTION' => '
#EMAIL_TO# - email address',
            'SORT' => '150',
        ));

        $helper->Event()->saveEventMessage('PHOTO_CART_MAIL', array (
            'LID' =>
                array (
                    0 => 's1',
                ),
            'ACTIVE' => 'Y',
            'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
            'EMAIL_TO' => '#EMAIL_TO#',
            'SUBJECT' => 'Выбранные фотографии',
            'MESSAGE' => 'Выбранные фотографии',
            'BODY_TYPE' => 'text',
            'BCC' => '',
            'REPLY_TO' => '',
            'CC' => '',
            'IN_REPLY_TO' => '',
            'PRIORITY' => '',
            'FIELD1_NAME' => NULL,
            'FIELD1_VALUE' => NULL,
            'FIELD2_NAME' => NULL,
            'FIELD2_VALUE' => NULL,
            'SITE_TEMPLATE_ID' => '',
            'ADDITIONAL_FIELD' =>
                array (
                ),
            'LANGUAGE_ID' => '',
        ));

    }

    public function down() {
        $helper = new HelperManager();

        $helper->Event()->deleteEventMessage([
            'EVENT_NAME'=>'PHOTO_CART_MAIL',
            'SUBJECT'=>'Выбранные фотографии',
        ]);

        $helper->Event()->deleteEventType([
            'EVENT_NAME' => 'PHOTO_CART_MAIL',
            'LID' => 'ru',
        ]);

        $helper->Event()->deleteEventType([
            'EVENT_NAME' => 'PHOTO_CART_MAIL',
            'LID' => 'en',
        ]);

    }

}
