<?php

namespace Sprint\Migration;

use local\Domain\Repository\ObjectRepository;
use Bitrix\Main\LoaderException;
use Exception;

class Version20190407232614 extends Version
{

    protected $description = "Переименовать объекты";

    /**
     * @return bool|void
     * @throws LoaderException
     * @throws Exception
     */
    public function up() {
        $Repository=new ObjectRepository();
        $elements=$Repository->getList([
            "ID"=>"ASC",
        ]);
        $i=1;
        foreach ($elements as $element)
        {
            $element->setName($i);
            $Repository->Update($element->getId(),$element);
            $i++;
        }
    }

    public function down() {

    }

}
