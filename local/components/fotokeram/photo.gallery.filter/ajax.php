<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 03.03.2019
 * Time: 21:46
 */
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_WITH_ON_AFTER_EPILOG', true);
define('BX_NO_ACCELERATOR_RESET', true);
define('STOP_STATISTICS', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();

$user=(int)$request->get("user");

$APPLICATION->IncludeComponent(
    "fotokeram:photo.gallery.filter",
    "ajax",
    Array(
        "USER"=>$user
    )
);