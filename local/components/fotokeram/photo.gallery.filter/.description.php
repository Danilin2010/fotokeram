<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
    "NAME" => Loc::getMessage('FOTOKERAM_PHOTO_GALLERY_FILTER_DESCRIPTION_NAME'),
    "DESCRIPTION" => Loc::getMessage('FOTOKERAM_PHOTO_FILTER_GALLERY_DESCRIPTION_DESCRIPTION'),
    "SORT" => 10,
    "PATH" => array(
        "ID" => 'fotokeram',
        "NAME" => Loc::getMessage('FOTOKERAM_PHOTO_GALLERY_DESCRIPTION_GROUP'),
        "SORT" => 10,
        "CHILD" => array(
            "ID" => 'photogallery',
            "NAME" => Loc::getMessage('FOTOKERAM_PHOTO_GALLERY_DESCRIPTION_DIR'),
            "SORT" => 10
        )
    ),
);

?>