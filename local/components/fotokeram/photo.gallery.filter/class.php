<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc as Loc;

use local\Domain\Repository\BrandRepository;
use local\Domain\Repository\BrickFormatRepository;
use local\Domain\Repository\SeamColorRepository;
use local\Domain\Repository\BrickFormatFormatRepository;
use local\Domain\Repository\BrickColorRepository;
use local\Domain\Repository\PhotoRepository;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;
use local\Domain\Entity\BrickFormat;
use local\Helpers\Supporting\SupportingFilter;
use local\Domain\Entity\BrickFormatFormat;
use local\Domain\Entity\BrickColor;
use local\Domain\Entity\SeamColor;
use local\Domain\Entity\Brand;
use local\Domain\Entity\ObjectType;
use Bitrix\Main\LoaderException;
use local\Domain\Repository\AddressRepository;
use local\Domain\Repository\YearListRepository;
use local\Domain\Repository\ObjectTypeRepository;

class FotokeramPhotoGalleryFilter extends CBitrixComponent
{

	/**
	 * подключает языковые файлы
	 */
	public function onIncludeComponentLang()
	{
		$this->includeComponentLang(basename(__FILE__));
		Loc::loadMessages(__FILE__);
	}
	
    /**
     * подготавливает входные параметры
     * @param array $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }
	
	/**
	 * получение результатов
     * @throws Exception
	 */
	protected function getResult()
	{
        $filter=[];

        if($this->arParams["USER"]>0)
            $filter["USER"]=$this->arParams["USER"];

        $request = Application::getInstance()->getContext()->getRequest();

        $setFilter = $request->get("setFilter");
        if($setFilter=="Y")
        {
            $this->arResult["setFilter"]="Y";
            $filter=$request->get("filter");
        }

      /*  if($this->arParams['USE_MAP']=="Y"){
            $filter['!address_coords1']=false;
            $filter['!address_coords2']=false;
        }
*/
        //echo "<pre>";print_r($filter);echo "</pre>";

        $this->arResult["filter"]=$filter;



        $uriString = $request->getRequestUri();
        $uri = new Uri($uriString);
        $uri->deleteParams(["setFilter","filter"]);
        $this->arResult["delFilter"]=$uri->getUri();

        $this->arResult['BrickFormat']=$this->getBrickFormat($filter);
        $this->arResult['BrickFormatFormat']=$this->getBrickFormatFormat($filter);
        $this->arResult['BrickFormatСolor']=$this->getBrickFormatColor($filter);
        $this->arResult['Brand']=$this->getBrand($filter);
        $this->arResult['objecttype']=$this->getObjectType($filter);
        $this->arResult['SeamColor']=$this->getSeamColor($filter);
        $this->arResult['Mix']=$this->getMix($filter);
        $this->arResult['Region']=$this->getRegion($filter);
        $this->arResult['Area']=$this->getArea($filter);
        $this->arResult['City']=$this->getCity($filter);
        $this->arResult['Year']=$this->getYear($filter);

        if(count($filter)>0){
            global $FotokeramPhotoGalleryFilter;
            $FotokeramPhotoGalleryFilter=$filter;
        }

    }

    /**
	 * выполняет логику работы компонента
	 */
	public function executeComponent()
	{
		try
		{
            $this->getResult();
            $this->includeComponentTemplate();
        }
		catch (Exception $e)
		{
			ShowError($e->getMessage());
		}
	}


    /**
     * @param $filter
     * @return BrickFormat[]
     * @throws LoaderException
     * @throws \Exception
     */
	private function getBrickFormat($filter){
        $filter=SupportingFilter::getFilter($filter,"BrickFormatVendor");
        $PhotoRepository=new PhotoRepository();
        $brickformat=[];
        $list=$PhotoRepository->_getList(false,$filter,[
            'object_brickformat_id',
        ],false,false,[
            'object_brickformat_id',
        ]);
        foreach ($list as $el){
            if(!in_array($el["object_brickformat_id"],$brickformat))
                $brickformat[]=$el["object_brickformat_id"];
        }
        if(count($brickformat)==0)
            return [];
        $BrickFormatRepository=new BrickFormatRepository();
        return $BrickFormatRepository->getList([],[
            'id'=>$brickformat,
        ]);
    }

    /**
     * @param $filter
     * @return BrickFormatFormat[]
     * @throws LoaderException
     * @throws \Exception
     */
    private function getBrickFormatFormat($filter){
        $filter=SupportingFilter::getFilter($filter,"BrickFormatFormat");
        $PhotoRepository=new PhotoRepository();
        $brickformat=[];
        $list=$PhotoRepository->_getList(false,$filter,[
            'object_brickformat_format_id',
        ],false,false,[
            'object_brickformat_format_id',
        ]);
        foreach ($list as $el){
            if(!in_array($el["object_brickformat_format_id"],$brickformat))
                $brickformat[]=$el["object_brickformat_format_id"];
        }
        if(count($brickformat)==0)
            return [];
        $BrickFormatRepository=new BrickFormatFormatRepository();
        return $BrickFormatRepository->getList([],[
            'id'=>$brickformat,
        ]);
    }

    /**
     * @param $filter
     * @return BrickColor[]
     * @throws LoaderException
     * @throws \Exception
     */
    private function getBrickFormatColor($filter){
        $filter=SupportingFilter::getFilter($filter,"BrickFormatСolor");
        $PhotoRepository=new PhotoRepository();
        $brickformat=[];
        $list=$PhotoRepository->_getList(false,$filter,[
            'object_brickformat_color_id',
        ],false,false,[
            'object_brickformat_color_id',
        ]);
        foreach ($list as $el){
            if(!in_array($el["object_brickformat_color_id"],$brickformat))
                $brickformat[]=$el["object_brickformat_color_id"];
        }
        if(count($brickformat)==0)
            return [];
        $BrickFormatRepository=new BrickColorRepository();
        return $BrickFormatRepository->getList([],[
            'id'=>$brickformat,
        ]);
    }

    /**
     * @param $filter
     * @return SeamColor[]
     * @throws LoaderException
     * @throws \Exception
     */
    private function getSeamColor($filter){
        $filter=SupportingFilter::getFilter($filter,"SeamColor");
        $PhotoRepository=new PhotoRepository();
        $brickformat=[];
        $list=$PhotoRepository->_getList(false,$filter,[
            'seam_color_id',
        ],false,false,[
            'seam_color_id',
        ]);
        foreach ($list as $el){
            if(!in_array($el["seam_color_id"],$brickformat))
                $brickformat[]=$el["seam_color_id"];
        }
        if(count($brickformat)==0)
            return [];
        $BrickFormatRepository=new SeamColorRepository();
        return $BrickFormatRepository->getList([],[
            'id'=>$brickformat,
        ]);
    }

    /**
     * @param $filter
     * @return Brand[]
     * @throws LoaderException
     * @throws \Exception
     */
    private function getBrand($filter){
        $filter=SupportingFilter::getFilter($filter,"Brand");
        $PhotoRepository=new PhotoRepository();
        $brickformat=[];
        $list=$PhotoRepository->_getList(false,$filter,[
            'object_brickformat_brand_id',
        ],false,false,[
            'object_brickformat_brand_id',
        ]);
        foreach ($list as $el){
            if(!in_array($el["object_brickformat_brand_id"],$brickformat))
                $brickformat[]=$el["object_brickformat_brand_id"];
        }
        if(count($brickformat)==0)
            return [];
        $BrickFormatRepository=new BrandRepository();
        return $BrickFormatRepository->getList([],[
            'id'=>$brickformat,
        ]);
    }

    /**
     * @param $filter
     * @return ObjectType[]
     * @throws LoaderException
     * @throws \Exception
     */
    private function getObjectType($filter){
        $filter=SupportingFilter::getFilter($filter,"objecttype");
        $PhotoRepository=new PhotoRepository();
        $brickformat=[];
        $list=$PhotoRepository->_getList(false,$filter,[
            'object_type',
        ],false,false,[
            'object_type',
        ]);
        foreach ($list as $el){
            if(!in_array($el["object_type"],$brickformat))
                $brickformat[]=$el["object_type"];
        }
        if(count($brickformat)==0)
            return [];
        $BrickFormatRepository=new ObjectTypeRepository();
        return $BrickFormatRepository->getList([],[
            'id'=>$brickformat,
        ]);
    }

    /**
     * @param $filter
     * @return array
     * @throws LoaderException
     * @throws \Exception
     */
    private function getMix($filter){
        $filter=SupportingFilter::getFilter($filter,"Mix");
        $PhotoRepository=new PhotoRepository();
        $brickformat=[];
        $list=$PhotoRepository->_getList(false,$filter,[
            'mix',
        ],false,false,[
            'mix',
        ]);
        foreach ($list as $el){
            if($el['mix']==1){
                if(!in_array("Y",$brickformat))
                    $brickformat[]="Y";
            }else{
                if(!in_array("N",$brickformat))
                    $brickformat[]="N";
            }
        }
        asort($brickformat);
        return $brickformat;
    }

    /**
     * @param $filter
     * @return array
     * @throws LoaderException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     * @throws \Exception
     */
    private function getRegion($filter){
        $filter=SupportingFilter::getFilter($filter,"RegionId");
        $PhotoRepository=new PhotoRepository();
        $brickformat=[];
        $list=$PhotoRepository->_getList(false,$filter,[
            'object_address_regionid',
        ],false,false,[
            'object_address_regionid',
        ]);
        foreach ($list as $el)
            if(!in_array($el["object_address_regionid"],$brickformat))
                $brickformat[]=$el["object_address_regionid"];
        $AddressRepository=new AddressRepository();
        $Address=$AddressRepository->_getList(false,
            [
                'regionid'=>$brickformat,
            ],
            [
                'regionid',
            ],
            false,false,
            [
                'regionid',
                'region',
            ]
        );
        $NewBrickformat=[];
        foreach ($Address as $e)
            $NewBrickformat[$e["regionid"]]=$e["region"];
        asort($NewBrickformat);

        $NewNewBrickformat=[];
        foreach ($NewBrickformat as $key=>$value){
            if(in_array($key,[
                '9100000000000',
                '6100000000000',
                '2600000000000',
                '2300000000000',
            ])){
                $NewNewBrickformat[$key]=$value;
                unset($NewBrickformat[$key]);
            }
        }
        //$NewNewBrickformat=array_merge($NewNewBrickformat,$NewBrickformat);
        $NewNewBrickformat=$NewNewBrickformat+$NewBrickformat;
        return $NewNewBrickformat;
    }

    /**
     * @param $filter
     * @return array
     * @throws LoaderException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     * @throws \Exception
     */
    private function getArea($filter){
        $filter=SupportingFilter::getFilter($filter,"AreaId");
        $brickformat=[];
        if(!$filter["object_address_regionid"])
            return $brickformat;

        $PhotoRepository=new PhotoRepository();
        $list=$PhotoRepository->_getList(false,$filter,[
            'object_address_areaid',
        ],false,false,[
            'object_address_areaid',
        ]);
        foreach ($list as $el)
            if(!in_array($el["object_address_areaid"],$brickformat))
                $brickformat[]=$el["object_address_areaid"];
        $AddressRepository=new AddressRepository();
        $Address=$AddressRepository->_getList(false,
            [
                'areaid'=>$brickformat,
            ],
            [
                'areaid',
            ],
            false,false,
            [
                'areaid',
                'area',
            ]
        );
        $NewBrickformat=[];
        foreach ($Address as $e)
            $NewBrickformat[$e["areaid"]]=$e["area"];
        asort($NewBrickformat);
        return $NewBrickformat;
    }

    /**
     * @param $filter
     * @return array
     * @throws LoaderException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     * @throws \Exception
     */
    private function getCity($filter){
        $filter=SupportingFilter::getFilter($filter,"CityId");
        $brickformat=[];
        if(!$filter["object_address_regionid"] && !$filter["object_address_areaid"])
            return $brickformat;

        $PhotoRepository=new PhotoRepository();
        $list=$PhotoRepository->_getList(false,$filter,[
            'object_address_cityid',
        ],false,false,[
            'object_address_cityid',
        ]);
        foreach ($list as $el)
            if(!in_array($el["object_address_cityid"],$brickformat))
                $brickformat[]=$el["object_address_cityid"];
        $AddressRepository=new AddressRepository();
        $Address=$AddressRepository->_getList(false,
            [
                'cityid'=>$brickformat,
            ],
            [
                'cityid',
            ],
            false,false,
            [
                'cityid',
                'city',
            ]
        );
        $NewBrickformat=[];
        foreach ($Address as $e)
            $NewBrickformat[$e["cityid"]]=$e["city"];
        asort($NewBrickformat);
        return $NewBrickformat;
    }

    /**
     * @param $filter
     * @return array
     * @throws LoaderException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     * @throws \Exception
     */
    private function getYear($filter){
        $filter=SupportingFilter::getFilter($filter,"Year");
        $brickformat=[];

        $PhotoRepository=new PhotoRepository();
        $list=$PhotoRepository->_getList(false,$filter,[
            'object_year',
        ],false,false,[
            'object_year',
        ]);

        foreach ($list as $el)
            if(!in_array($el["object_year"],$brickformat))
                $brickformat[]=$el["object_year"];

        $YearListRepository=new YearListRepository();
        $YearList=$YearListRepository->getYearList([
            'ID'=>$brickformat,
        ]);
        return $YearList;

    }


}
?>