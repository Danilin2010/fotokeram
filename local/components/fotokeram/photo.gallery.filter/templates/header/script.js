jQuery(document).ready(function() {



    function prepareData(data) {
        //console.log(data);
        var prav=[
            {name:'Brand',id:'Brand',idfilter:'Brand'},
            {name:'BrickFormat',id:'BrickFormatVendor',idfilter:'BrickFormatVendor'},
            {name:'BrickFormatFormat',id:'BrickFormatFormat',idfilter:'BrickFormatFormat'},
            {name:'BrickFormatСolor',id:'BrickFormatСolor',idfilter:'BrickFormatСolor'},
            {name:'SeamColor',id:'SeamColor',idfilter:'SeamColor'},
            {name:'Year',id:'Year',idfilter:'Year'},
            {name:'Region',id:'Region',idfilter:'RegionId'},
            {name:'Area',id:'Area',idfilter:'AreaId'},
            {name:'City',id:'City',idfilter:'CityId'},
            {name:'Mix',id:'Mix',idfilter:'Mix'}
        ];
        for (var i=0;i<prav.length;i++)
        {
            var
                id=prav[i].id,
                name=prav[i].name,
                idfilter=prav[i].idfilter,
                value=data[name],
                el=jQuery('#'+id)
            ;
            el.empty();
            el.append($('<option value="">Выберите</option>'));

            if(value.length==0)
                el.attr('disabled', 'disabled');
            else
                el.removeAttr('disabled');

            switch (name) {
                case "BrickFormat":
                    for (var k=0;k<value.length;k++) {
                        var
                            optionName = value[k].name + ' (' + value[k].vendor + ')',
                            optionId = value[k].id,
                            option = $('<option>' + optionName + '</option>')
                        ;
                        option.attr('value', optionId);
                        if (typeof (data.filter[idfilter])!="undefined" && data.filter[idfilter][0] == optionId)
                            option.attr('selected', 'selected');
                        el.append(option);
                    }
                    break;
                case "Region":
                case "Area":
                case "City":
                    $.each(value, function(index, value){
                        var
                            optionName = value,
                            optionId = index,
                            option = $('<option>' + optionName + '</option>')
                        ;
                        option.attr('value', optionId);
                        if (typeof (data.filter[idfilter])!="undefined" && data.filter[idfilter][0] == optionId)
                            option.attr('selected', 'selected');
                        el.append(option);
                    });
                    break;
                case "Year":
                    for (var k=0;k<value.length;k++) {
                        var
                            optionName = value[k].value,
                            optionId = value[k].id,
                            option = $('<option>' + optionName + '</option>')
                        ;
                        option.attr('value', optionId);
                        if (typeof (data.filter[idfilter])!="undefined" && data.filter[idfilter][0] == optionId)
                            option.attr('selected', 'selected');
                        el.append(option);
                    }
                    break;
                case "Mix":
                    for (var k=0;k<value.length;k++) {
                        var optionId = value[k], optionName = "", option;
                        if (optionId == "Y")
                            optionName = "Да";
                        else if (optionId == "N")
                            optionName = "Нет";
                        if(optionName.length>0){
                            option = $('<option>' + optionName + '</option>');
                            option.attr('value', optionId);
                            if (typeof (data.filter[idfilter])!="undefined" && data.filter[idfilter] == optionId)
                                option.attr('selected', 'selected');
                            el.append(option);
                        }
                    }
                    break;
                default:
                    for (var k=0;k<value.length;k++) {
                        var
                            optionName = value[k].name,
                            optionId = value[k].id,
                            option = $('<option>' + optionName + '</option>')
                        ;
                        option.attr('value', optionId);
                        if (typeof (data.filter[idfilter])!="undefined" && data.filter[idfilter][0] == optionId)
                            option.attr('selected', 'selected');
                        el.append(option);
                    }
                    break;
            }

            el.trigger('refresh');
        }
    }


    var
        form=jQuery('[data-filter-form]'),
        url=form.data('filterAjaxUrl')
    ;

    form.find('select').on('change',function (e) {
        var data=form.serializeArray();

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'GET',
            data: data,
            success: function (data) {
                prepareData(data);
            }
        });
    });

});