<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<?$classColumn="col-xs-12 col-md-2"?>
<form data-filter-form data-filter-ajax-url="<?=$componentPath?>/ajax.php" class="" action="" method="get">

    <div class="row">

        <div class="<?=$classColumn?>">
            <div class="cont-column-full">
                <label>Бренд</label>
                <select id="Brand" name="filter[Brand][]" class="form-control notstyler brickformat" <?if(count($arResult["Brand"])==0){?>disabled<?}?>>
                    <option value="">Выберите</option>
                    <?foreach ($arResult["Brand"] as $brand){?>
                        <option
                            <?if(in_array($brand->getId(),$arResult["filter"]["Brand"])){?>selected<?}?>
                            value="<?=$brand->getId();?>"><?=$brand->getName();?></option>
                    <?}?>
                </select>
            </div>
        </div>
        <div class="<?=$classColumn?>">
            <div class="cont-column-full">
                <label>Название (артикул)</label>
                <select id="BrickFormatVendor" name="filter[BrickFormatVendor][]" class="form-control notstyler brickformat" <?if(count($arResult["BrickFormat"])==0){?>disabled<?}?>>
                    <option value="">Выберите</option>
                    <?foreach ($arResult["BrickFormat"] as $BrickFormat){?>
                        <option
                            <?if(in_array($BrickFormat->getId(),$arResult["filter"]["BrickFormatVendor"])){?>selected<?}?>
                            value="<?=$BrickFormat->getId();?>"><?=$BrickFormat->getName();?> (<?=$BrickFormat->getVendor();?>)</option>
                    <?}?>
                </select>
            </div>
        </div>
        <div class="<?=$classColumn?>">
            <div class="cont-column-full">
                <label>Формат</label>
                <select id="BrickFormatFormat" name="filter[BrickFormatFormat][]" class="form-control notstyler brickformat" <?if(count($arResult["BrickFormatFormat"])==0){?>disabled<?}?>>
                    <option value="">Выберите</option>
                    <?foreach ($arResult["BrickFormatFormat"] as $BrickFormat){?>
                        <option
                            <?if(in_array($BrickFormat->getId(),$arResult["filter"]["BrickFormatFormat"])){?>selected<?}?>
                            value="<?=$BrickFormat->getId();?>"><?=$BrickFormat->getName();?></option>
                    <?}?>
                </select>
            </div>
        </div>
        <div class="<?=$classColumn?>">
            <div class="cont-column-full">
                <label>Цвет кирпича</label>
                <select id="BrickFormatСolor" name="filter[BrickFormatСolor][]" class="form-control notstyler brickformat" <?if(count($arResult["BrickFormatСolor"])==0){?>disabled<?}?>>
                    <option value="">Выберите</option>
                    <?foreach ($arResult["BrickFormatСolor"] as $Color){?>
                        <option
                            <?if(in_array($Color->getId(),$arResult["filter"]["BrickFormatСolor"])){?>selected<?}?>
                            value="<?=$Color->getId();?>"><?=$Color->getName();?></option>
                    <?}?>
                </select>
            </div>
        </div>
        <div class="<?=$classColumn?>">
            <div class="cont-column-full">
                <label>Цвет шва</label>
                <select id="SeamColor" name="filter[SeamColor][]" class="form-control notstyler brickformat" <?if(count($arResult["SeamColor"])==0){?>disabled<?}?>>
                    <option value="">Выберите</option>
                    <?foreach ($arResult["SeamColor"] as $SeamColor){?>
                        <option
                            <?if(in_array($SeamColor->getId(),$arResult["filter"]["SeamColor"])){?>selected<?}?>
                            value="<?=$SeamColor->getId();?>"><?=$SeamColor->getName();?></option>
                    <?}?>
                </select>
            </div>
        </div>

        <div class="<?=$classColumn?>">
            <div class="cont-column-full">
                <label>Год</label>
                <select id="Year" name="filter[Year][]" class="form-control notstyler brickformat" <?if(count($arResult["Year"])==0){?>disabled<?}?>>
                    <option value="">Выберите</option>
                    <?foreach ($arResult["Year"] as $Year){?>
                        <option
                            <?if(in_array($Year->getId(),$arResult["filter"]["Year"])){?>selected<?}?>
                            value="<?=$Year->getId();?>"><?=$Year->getValue();?></option>
                    <?}?>
                </select>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="<?=$classColumn?>">
            <div class="cont-column-full">
                <label>Регион</label>
                <select id="Region" name="filter[RegionId][]" class="form-control notstyler brickformat" <?if(count($arResult["Region"])==0){?>disabled<?}?>>
                    <option value="">Выберите</option>
                    <?foreach ($arResult["Region"] as $key=>$Region){?>
                        <option
                            <?if(in_array($key,$arResult["filter"]["RegionId"])){?>selected<?}?>
                            value="<?=$key?>"><?=$Region?></option>
                    <?}?>
                </select>
            </div>
        </div>
        <div class="<?=$classColumn?>">
            <div class="cont-column-full">
                <label>Район</label>
                <select id="Area" name="filter[AreaId][]" class="form-control notstyler brickformat" <?if(count($arResult["Area"])==0){?>disabled<?}?>>
                    <option value="">Выберите</option>
                    <?foreach ($arResult["Area"] as $key=>$Region){?>
                        <option
                            <?if(in_array($key,$arResult["filter"]["AreaId"])){?>selected<?}?>
                            value="<?=$key?>"><?=$Region?></option>
                    <?}?>
                </select>
            </div>
        </div>
        <div class="<?=$classColumn?>">
            <div class="cont-column-full">
                <label>Город</label>
                <select id="City" name="filter[CityId][]" class="form-control notstyler brickformat" <?if(count($arResult["City"])==0){?>disabled<?}?>>
                     <option value="">Выберите</option>
                    <?foreach ($arResult["City"] as $key=>$Region){?>
                        <option
                            <?if(in_array($key,$arResult["filter"]["CityId"])){?>selected<?}?>
                            value="<?=$key?>"><?=$Region?></option>
                    <?}?>
                </select>
            </div>
        </div>
        <div class="<?=$classColumn?>">
            <div class="cont-column-full">
                <label>Микс</label>
                <select id="Mix" name="filter[Mix]" class="form-control notstyler brickformat" <?if(count($arResult["Mix"])==0){?>disabled<?}?>>
                    <option value="">Выберите</option>
                    <?foreach ($arResult["Mix"] as $Mix){?>
                        <option
                            <?if($Mix==$arResult["filter"]["Mix"]){?>selected<?}?>
                            value="<?=$Mix;?>"><?=Loc::getMessage("TEMPLATE_FILTER_MIX_".$Mix)?></option>
                    <?}?>
                </select>
            </div>
        </div>
        <?if($arResult["setFilter"]=="Y" && count($arResult["filter"])>0){?>
            <div class="<?=$classColumn?>">
                <div class="cont-column-full">
                    <label>&nbsp;</label>
                    <div class="clearfix"></div>
                    <i class="flaticon-delete16"></i><a href="<?=$arResult["delFilter"]?>">Сбросить фильтр</a>
                </div>
            </div>
        <?}?>
        <div class="<?=$classColumn?>">
            <div class="cont-column-full">
                <label>&nbsp;</label>
                <div class="clearfix"></div>
                <input type="hidden" name="setFilter" value="Y" />
                <input class="button btn medium" name="" type="submit" value="Применить">
            </div>
        </div>
    </div>
</form>