<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<form>
<div class="module-block">
    <div class="title">Фильтр</div>
    <div class="filter">

        <div class="spoiler-wrap">
            <div class="spoiler-head open folded">Бренд</div>
            <div class="spoiler-body">
                <?foreach ($arResult["Brand"] as $Brand){?>
                <label>
                    <input type="checkbox" value="<?=$Brand->getId()?>" name="filter[Brand][]"
                           <?if(in_array($Brand->getId(),$arResult["filter"]["Brand"])){?>checked<?}?> />
                    <?=$Brand->getName()?></label>
                <?}?>
            </div>
        </div>

        <div class="spoiler-wrap">
            <div class="spoiler-head open folded">Артикул</div>
            <div class="spoiler-body">
                <?foreach ($arResult["BrickFormat"] as $BrickFormat){?>
                    <label>
                        <input type="checkbox" value="<?=$BrickFormat->getId()?>" name="filter[BrickFormatVendor][]"
                               <?if(in_array($BrickFormat->getId(),$arResult["filter"]["BrickFormatVendor"])){?>checked<?}?> />
                        <?=$BrickFormat->getVendor()?></label>
                <?}?>
            </div>
        </div>

        <div class="spoiler-wrap">
            <div class="spoiler-head open folded">Формат</div>
            <div class="spoiler-body">
                <?foreach ($arResult["BrickFormat"] as $BrickFormat){?>
                    <label>
                        <input type="checkbox" value="<?=$BrickFormat->getId()?>" name="filter[BrickFormatFormat][]"
                               <?if(in_array($BrickFormat->getId(),$arResult["filter"]["BrickFormatFormat"])){?>checked<?}?> />
                        <?=$BrickFormat->getFormat()->getName()?></label>
                <?}?>
            </div>
        </div>

        <div class="spoiler-wrap">
            <div class="spoiler-head open folded">Цвет кирпича</div>
            <div class="spoiler-body">
                <?foreach ($arResult["BrickFormat"] as $BrickFormat){?>
                    <?$Color=$BrickFormat->getColor();?>
                    <label>
                        <input type="checkbox" value="<?=$Color->getId()?>" name="filter[BrickFormatСolor][]"
                               <?if(in_array($Color->getId(),$arResult["filter"]["BrickFormatСolor"])){?>checked<?}?> />
                        <?=$Color->getName()?></label>
                <?}?>
            </div>
        </div>

        <div class="spoiler-wrap">
            <div class="spoiler-head open folded">Цвет шва</div>
            <div class="spoiler-body">
                <?foreach ($arResult["SeamColor"] as $SeamColor){?>
                    <label>
                        <input type="checkbox" value="<?=$SeamColor->getId()?>" name="filter[SeamColor][]"
                               <?if(in_array($SeamColor->getId(),$arResult["filter"]["SeamColor"])){?>checked<?}?> />
                        <?=$SeamColor->getName()?></label>
                <?}?>
            </div>
        </div>

        <div class="spoiler-wrap">
            <div class="spoiler-head open folded">Несколько видов кирпича</div>
            <div class="spoiler-body">
                    <label>
                        <input type="checkbox" value="Y" name="filter[Mix]"
                               <?if($arResult["filter"]["Mix"]=="Y"){?>checked<?}?> />
                        Да</label>
            </div>
        </div>

        <div class="trashlink">
            <?if($arResult["setFilter"]=="Y" && count($arResult["filter"])>0){?>
                <i class="flaticon-delete16"></i><a href="<?=$arResult["delFilter"]?>">Сбросить фильтр</a>
            <?}?>
        </div>
        <div class="apply-btn">
            <input type="hidden" name="setFilter" value="Y" />
            <input name="" type="submit" value="Применить">
        </div>
    </div>
</div>
</form>