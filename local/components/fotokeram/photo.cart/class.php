<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \local\Services\PhotoCart;

class FotokeramPhotoCart extends \CBitrixComponent
{
	/**
	 * шаблоны путей по умолчанию
	 * @var array
	 */
	protected $defaultUrlTemplates404 = array();
	
	/**
	 * переменные шаблонов путей
	 * @var array
	 */
	protected $componentVariables = array();
	
	/**
	 * страница шаблона
	 * @var string
	 */
	protected $page = '';

	/**
	 * определяет переменные шаблонов и шаблоны путей
	 */

	/**
	 * получение результатов
	 */
	protected function getResult()
	{

        $this->arResult["PhotoCart"]=PhotoCart::getPhoto();

	}
	
	/**
	 * выполняет логику работы компонента
	 */
	public function executeComponent()
	{
		try
		{
			$this->getResult();
			$this->includeComponentTemplate($this->page);
		}
		catch (Exception $e)
		{
			ShowError($e->getMessage());
		}
	}
}
?>