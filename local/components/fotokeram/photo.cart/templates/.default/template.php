<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<div class="module-block">
    <div class="title">Корзина</div>
    <div class="banner-block">
        <div class="photocartnumvisible visible">
            Выбранно фотографий: <span id="photocartnum"><?=count($arResult["PhotoCart"])?></span>
            <br/><br/>
            <a class="btn btn-danger" data-sendemail>Отправить мне фото</a>
        </div>
        <div class="photocartnumhidden">
            Фотографии отправлены
        </div>
    </div>
</div>
