<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc as Loc;

use local\Domain\Repository\ObjectRepository;
use local\Domain\Repository\PhotoRepository;
use Bitrix\Main\UI\PageNavigation;

class FotokeramPhotoGalleryDetail extends CBitrixComponent
{

	/**
	 * кешируемые ключи arResult
	 * @var array()
	 */
	protected $cacheKeys = array("Photo","Object");
	
	/**
	 * дополнительные параметры, от которых должен зависеть кеш
	 * @var array
	 */
	protected $cacheAddon = array();
	
	/**
	 * парамтеры постраничной навигации
	 * @var array
	 */
	protected $navParams = array();

    /**
     * вохвращаемые значения
     * @var mixed
     */
	protected $returned;

    /**
     * тегированный кеш
     * @var mixed
     */
    protected $tagCache;
	
	/**
	 * подключает языковые файлы
	 */
	public function onIncludeComponentLang()
	{
		$this->includeComponentLang(basename(__FILE__));
		Loc::loadMessages(__FILE__);
	}
	
    /**
     * подготавливает входные параметры
     * @param array $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }
	
	/**
	 * определяет читать данные из кеша или нет
	 * @return bool
	 */
	protected function readDataFromCache()
	{
		global $USER;
		if ($this->arParams['CACHE_TYPE'] == 'N')
			return false;

		if (is_array($this->cacheAddon))
			$this->cacheAddon[] = $USER->GetUserGroupArray();
		else
			$this->cacheAddon = array($USER->GetUserGroupArray());
		return !($this->startResultCache(false, $this->cacheAddon, md5(serialize($this->arParams))));
	}

	/**
	 * кеширует ключи массива arResult
	 */
	protected function putDataToCache()
	{
		if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0)
		{
			$this->SetResultCacheKeys($this->cacheKeys);
		}
	}

	/**
	 * прерывает кеширование
	 */
	protected function abortDataCache()
	{
		$this->AbortResultCache();
	}

    /**
     * завершает кеширование
     * @return bool
     */
    protected function endCache()
    {
        if ($this->arParams['CACHE_TYPE'] == 'N')
            return false;

        $this->endResultCache();
    }
	
	/**
	 * проверяет подключение необходиимых модулей
	 * @throws LoaderException
	 */
	protected function checkModules()
	{

	}
	
	/**
	 * проверяет заполнение обязательных параметров
	 * @throws SystemException
	 */
	protected function checkParams()
	{

	}
	
	/**
	 * выполяет действия перед кешированием 
	 */
	protected function executeProlog()
	{

	}

    /**
     * Определяет ID инфоблока по коду, если не был задан
     */
	protected function getIblockId()
    {

    }

	/**
	 * получение результатов
     * @throws Exception
	 */
	protected function getResult()
	{
        $ObjectRepository=new ObjectRepository();
        $PhotoRepository=new PhotoRepository();
        $this->arResult["Object"]=$ObjectRepository->getById($this->arParams["ELEMENT_ID"]);
        if($this->arResult["Object"])
        {
            if($this->arResult["Object"]->getId()>0){
                $this->arResult["Photo"]=$PhotoRepository->getList([],[
                    "object_id"=>$this->arResult["Object"]->getId()
                ],[],false,false,[
                    'id',
                    'name',
                    'sort',
                    'photo',
                ]);
                $this->arResult["Brand"]=$this->arResult["Object"]->getBrand();
                $this->arResult["Address"]=$this->arResult["Object"]->getAddress();
            }
        }
	}
	
	/**
	 * выполняет действия после выполения компонента, например установка заголовков из кеша
	 */
	protected function executeEpilog()
	{
	    global $APPLICATION;
        if($this->arResult["Object"])
        {
            $APPLICATION->SetTitle($this->arResult["Object"]->getName());
            $APPLICATION->AddChainItem($this->arResult["Object"]->getName());
        }else{
            CHTTP::SetStatus("404 Not Found");
            @define("ERROR_404","Y");
        }

	}
	
	/**
	 * выполняет логику работы компонента
	 */
	public function executeComponent()
	{
		global $APPLICATION;
		try
		{
			$this->checkModules();
			$this->checkParams();
			$this->executeProlog();
			if ($this->arParams['AJAX'] == 'Y')
				$APPLICATION->RestartBuffer();
			if (!$this->readDataFromCache())
			{
			    $this->getIblockId();
				$this->getResult();
				$this->putDataToCache();
				$this->includeComponentTemplate();
			}
			$this->executeEpilog();

			if ($this->arParams['AJAX'] == 'Y')
				die();

			return $this->returned;
		}
		catch (Exception $e)
		{
			$this->abortDataCache();
			ShowError($e->getMessage());
		}
	}
}
?>