<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<div class="product-page">
    <div class="left">
        <?if(count($arResult["Photo"])>0){
            $Photo=$arResult["Photo"][0];
            $arFileTmp = CFile::ResizeImageGet(
                $Photo->getPhotoId(),
                array("width" => 480, "height" => 480),
                BX_RESIZE_IMAGE_EXACT,
                true//,GetWatermark()
            );
            $arFileTmpB = CFile::ResizeImageGet(
                $Photo->getPhotoId(),
                array("width" => 1000, "height" => 600),
                BX_RESIZE_IMAGE_EXACT,
                true//,GetWatermark()
            );
            ?>
        <div class="product-main-image"><a class="fancybox" href="<?=$arFileTmpB["src"]?>" ><img src="<?=$arFileTmp["src"]?>"></a></div>
        <?}?>
        <?if(count($arResult["Photo"])>1){?>
            <div class="scroller">
                <div class="title">Фотографии объекта</div>
                <script>
                    $(document).ready(function() {
                        var owl = $("#images-scroll");
                        owl.owlCarousel({
                            items : 4,
                            responsive : false,
                            pagination : false,
                            itemsDesktop : [1000,4],
                            itemsDesktopSmall : [900,3],
                            itemsTablet: [600,2],
                            itemsMobile : false
                        });
                        $(".next").click(function(){
                            owl.trigger('owl.next');
                        })
                        $(".prev").click(function(){
                            owl.trigger('owl.prev');
                        })
                    });
                </script>
                <div id="images-scroll" class="owl-carousel owl-theme">
                    <?
                    foreach ($arResult["Photo"] as $Photo){
                        $arFileTmp = CFile::ResizeImageGet(
                            $Photo->getPhotoId(),
                            array("width" => 80, "height" => 80),
                            BX_RESIZE_IMAGE_EXACT,
                            true//,GetWatermark()
                        );
                        $arFileTmpB = CFile::ResizeImageGet(
                            $Photo->getPhotoId(),
                            array("width" => 1000, "height" => 600),
                            BX_RESIZE_IMAGE_EXACT,
                            true//,GetWatermark()
                        );
                        ?>
                        <div class="item"><a class="fancybox" href="<?=$arFileTmpB["src"]?>"><img src="<?=$arFileTmp["src"]?>"></a></div>
                    <?}?>
                </div>
                <div class="images-scroll-navi"> <a class="btn prev"><i class="fa fa-angle-left"></i></a> <a class="btn next"><i class="fa fa-angle-right"></i></a> </div>
            </div>
        <?}?>

    </div>
    <div class="right">
        <ul class="list_info_object">
            <li> <span>Бренд</span> <span class="dotted_kist"></span> <span class="gray_span"><?=$arResult["Brand"]->getName()?></span></li>
            <?foreach ($arResult["Object"]->getBrick() as $brick){?>
                <li> <span>Артикул</span> <span class="dotted_kist"></span> <span class="gray_span"><?=$brick->getVendor()?></span></li>
                <li> <span>Формат</span> <span class="dotted_kist"></span> <span class="gray_span"><?=$brick->getFormat()->getName();?></span></li>
                <li> <span>Цвет кирпича</span> <span class="dotted_kist"></span> <span class="gray_span"><?=$brick->getColor()->getName()?></span></li>
            <?}?>

            <li> <span>Цвет шва</span> <span class="dotted_kist"></span> <span class="gray_span"><?=$arResult["Object"]->getSeamColor()->getName()?></span></li>
            <li> <span>Регион</span> <span class="dotted_kist"></span> <span class="gray_span"><?=$arResult["Address"]->getRegion()?></span></li>
            <li> <span>Район</span> <span class="dotted_kist"></span> <span class="gray_span"><?=$arResult["Address"]->getArea()?></span></li>
            <li> <span>Город</span> <span class="dotted_kist"></span> <span class="gray_span"><?=$arResult["Address"]->getCity()?></span></li>
        </ul>
    </div>
    <div class="clearfix"></div>
</div>