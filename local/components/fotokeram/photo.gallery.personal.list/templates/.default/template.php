<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?$this->SetViewTarget('afterh1'); ?>
<?
$url=$arParams["FOLDER"].$arParams["URL_TEMPLATES"]["detail"];
$url=str_replace("#ELEMENT_ID#",'add',$url);
?>
<a href="<?=$url?>" class="btn btn-danger">Добавить фото</a>
<?$this->EndViewTarget(); ?>
<?if($arResult["add_object"] && $arResult["add_object"]>0){?>
    <?ShowMessage(Array("TYPE"=>"OK", "MESSAGE"=>"Ваш объект добавлен."));?>
    <br/>
<?}?>
<div class="gallery-block col4">
    <?foreach ($arResult["photo"] as $photo){
        $Object=$photo["object"];

        $url=$arParams["FOLDER"].$arParams["URL_TEMPLATES"]["detail"];
        $url=str_replace("#ELEMENT_ID#",$Object->getId(),$url);

        ?>
        <div class="item" data-item>
            <?if(count($photo["photo"])>0){
                $PhotoId=$photo["photo"][0]->getPhotoId();
                if($PhotoId>0){
                    $arFileTmp = CFile::ResizeImageGet(
                        $PhotoId,
                        array("width" => 250, "height" => 250),
                        BX_RESIZE_IMAGE_EXACT,
                        true//,GetWatermark()
                    );
                    ?><div class="image"><a href="<?=$url?>"><img src="<?=$arFileTmp["src"]?>"></a></div><?
                }
            }?>
            <div class="title"><a href="<?=$url?>"><?=$Object->getName()?></a></div>
        </div>
    <?}?>
    <div class="clearfix" data-prev-item></div>
</div>
<?
$APPLICATION->IncludeComponent(
    "bitrix:main.pagenavigation",
    "catalog",
    array(
        "NAV_OBJECT" => $arResult["nav"],
        "SEF_MODE" => "Y",
    ),
    $component
);
?>