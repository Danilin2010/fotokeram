<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc as Loc;

use local\Domain\Repository\PhotoRepository;
use Bitrix\Main\UI\PageNavigation;
use local\Domain\Repository\ObjectRepository;
use local\Helpers\Supporting\SupportingFilter;

class FotokeramPhotoGalleryObjectList extends CBitrixComponent
{

    /**
     * @var array()
     */
    protected $filter = array();

	/**
	 * кешируемые ключи arResult
	 * @var array()
	 */
	protected $cacheKeys = array();
	
	/**
	 * дополнительные параметры, от которых должен зависеть кеш
	 * @var array
	 */
	protected $cacheAddon = array();
	
	/**
	 * парамтеры постраничной навигации
	 * @var array
	 */
	protected $navParams = array();

    /**
     * вохвращаемые значения
     * @var mixed
     */
	protected $returned;

    /**
     * тегированный кеш
     * @var mixed
     */
    protected $tagCache;
	
	/**
	 * подключает языковые файлы
	 */
	public function onIncludeComponentLang()
	{
		$this->includeComponentLang(basename(__FILE__));
		Loc::loadMessages(__FILE__);
	}
	
    /**
     * подготавливает входные параметры
     * @param array $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }
	
	/**
	 * определяет читать данные из кеша или нет
	 * @return bool
	 */
	protected function readDataFromCache()
	{
		global $USER;
		if ($this->arParams['CACHE_TYPE'] == 'N')
			return false;

		if (is_array($this->cacheAddon))
			$this->cacheAddon[] = $USER->GetUserGroupArray();
		else
			$this->cacheAddon = array($USER->GetUserGroupArray());

        $this->cacheAddon[]=$this->filter;

		return !($this->startResultCache(false, $this->cacheAddon, md5(serialize($this->arParams))));
	}

	/**
	 * кеширует ключи массива arResult
	 */
	protected function putDataToCache()
	{
		if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0)
		{
			$this->SetResultCacheKeys($this->cacheKeys);
		}
	}

	/**
	 * прерывает кеширование
	 */
	protected function abortDataCache()
	{
		$this->AbortResultCache();
	}

    /**
     * завершает кеширование
     * @return bool
     */
    protected function endCache()
    {
        if ($this->arParams['CACHE_TYPE'] == 'N')
            return false;

        $this->endResultCache();
    }
	
	/**
	 * проверяет подключение необходиимых модулей
	 * @throws LoaderException
	 */
	protected function checkModules()
	{

	}
	
	/**
	 * проверяет заполнение обязательных параметров
	 * @throws SystemException
	 */
	protected function checkParams()
	{

	}
	
	/**
	 * выполяет действия перед кешированием 
	 */
	protected function executeProlog()
	{
        global $FotokeramPhotoGalleryFilter;
        if($FotokeramPhotoGalleryFilter && is_array($FotokeramPhotoGalleryFilter) && count($FotokeramPhotoGalleryFilter)>0)
        $this->filter=$FotokeramPhotoGalleryFilter;

	}

    /**
     * Определяет ID инфоблока по коду, если не был задан
     */
	protected function getIblockId()
    {

    }

	/**
	 * получение результатов
     * @throws Exception
	 */
	protected function getResult()
	{
        $PageSize=(int)$this->arParams['PAGE_SIZE'];
        if($PageSize==0)
            $PageSize=40;

        $filter=SupportingFilter::getFilterObject($this->filter);

        $Repository=new ObjectRepository();
        $cnt = $Repository->getCount($filter);

        $RepositoryPhoto=new PhotoRepository();

        $limit=false;
        $offset=false;

        if($this->arParams['USE_MAP']!=="Y"){
            $nav = new PageNavigation("nav-list-photo");
            $nav->setPageSize($PageSize)
                ->initFromUri()
            ;
            $nav->setRecordCount($cnt);
            $this->arResult["nav"]=$nav;
            $limit=$nav->getLimit();
            $offset=$nav->getOffset();
        }

        $order=[];

        if($this->arParams['ELEMENT_SORT_FIELD'] && $this->arParams['ELEMENT_SORT_ORDER']){
            $order=[$this->arParams['ELEMENT_SORT_FIELD']=>$this->arParams['ELEMENT_SORT_ORDER']];
        }

        if($this->arParams['USE_MAP']=="Y"){
            $filter['!address_coords1']=false;
            $filter['!address_coords2']=false;
        }

        $elements=$Repository->getList($order,$filter,[],$limit,$offset);

        $elementsRes=[];
        foreach ($elements as $element)
        {
            $elementRes=[
                'object'=>$element,
                'photo'=>$RepositoryPhoto->getList([],[
                    'object_id'=>$element->getId(),
                ],[],false,false,[
                    'id',
                    'name',
                    'sort',
                    'photo',
                ])
            ];
            $elementsRes[]=$elementRes;
        }

        $this->arResult["photo"]=$elementsRes;
	}
	
	/**
	 * выполняет действия после выполения компонента, например установка заголовков из кеша
	 */
	protected function executeEpilog()
	{

	}
	
	/**
	 * выполняет логику работы компонента
	 */
	public function executeComponent()
	{
		global $APPLICATION;
		try
		{
			$this->checkModules();
			$this->checkParams();
			$this->executeProlog();
			if ($this->arParams['AJAX'] == 'Y')
				$APPLICATION->RestartBuffer();
			if (!$this->readDataFromCache())
			{
			    $this->getIblockId();
				$this->getResult();
				$this->putDataToCache();
				$this->includeComponentTemplate();
			}
			$this->executeEpilog();

			if ($this->arParams['AJAX'] == 'Y')
				die();

			return $this->returned;
		}
		catch (Exception $e)
		{
			$this->abortDataCache();
			ShowError($e->getMessage());
		}
	}
}
?>