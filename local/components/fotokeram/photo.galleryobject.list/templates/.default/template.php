<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?use local\Helpers\Watermark;?>
<div class="gallery-block col4">
    <?foreach ($arResult["photo"] as $photo){
        $Object=$photo->getObject()[0];
        $url=$arParams["FOLDER"].$arParams["URL_TEMPLATES"]["detail"];
        $url=str_replace("#ELEMENT_ID#",$Object->getId(),$url);
        $UserId=$Object->getAuthor();
        $Watermark=Watermark::GetWatermark();
        ?>
        <?if($photo->getPhotoId()>0){
            $arFileTmp = CFile::ResizeImageGet(
                $photo->getPhotoId(),
                array("width" => 250, "height" => 250),
                BX_RESIZE_IMAGE_EXACT,
                true,$Watermark
            );

            $arFileTmp2 = CFile::ResizeImageGet(
                $photo->getPhotoId(),
                array("width" => 1000, "height" => 1000),
                BX_RESIZE_IMAGE_EXACT,
                true,$Watermark
            );
        }?>

        <?

        $bricks=$Object->getBrick();
        $arrName=[];
        foreach($bricks as $brick){
            $arrName[]=$brick->getName().' ('.$brick->getVendor().')';
        }

        ?>

        <div class="item" data-item>
            <?if($photo->getPhotoId()>0){?><div class="image"><a data-photocart="<?=$photo->getId();?>" data-url="<?=$url?>" data-title-id="title-<?=$photo->getPhotoId()?>" class="fancybox" rel="image" target="_blank" rel="" href="<?=$arFileTmp2["src"]?>"><img src="<?=$arFileTmp["src"]?>"></a></div><?}?>
            <div class="title">
                <?=implode(', ',$arrName)?>
                <div class="clearfix"></div>
                <label>
                    <input type="checkbox" data-photocart="<?=$photo->getId();?>" value="<?=$photo->getId();?>" name="PhotoCart[]"
                           <?if(in_array($photo->getId(),$arParams["PHOTOCART"])){?>checked<?}?> />
                    Выбрать</label>
             </div>
            <div id="title-<?=$photo->getPhotoId()?>" class="hidden">
                <?=implode(', ',$arrName)?>
                <div class="clearfix"></div>
            </div>
        </div>
    <?}?>
    <div class="clearfix" data-prev-item></div>
</div>
<?
$APPLICATION->IncludeComponent(
    "bitrix:main.pagenavigation",
    "catalog",
    array(
        "NAV_OBJECT" => $arResult["nav"],
        //"SEF_MODE" => "Y",
    ),
    $component
);
?>

