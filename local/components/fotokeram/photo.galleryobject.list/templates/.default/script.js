jQuery(document).ready(function() {
    jQuery(document).on('change','input[data-photocart]',function (e) {
        var
            checked=$(this).prop('checked'),
            photocart=$(this).data('photocart'),
            input=$('input[data-photocart='+photocart+']').not(this),
            url='/ajax/DelPhotoCart.php'
        ;
        input.prop('checked',checked).trigger('refresh');

        if(checked)
            url='/ajax/AddPhotoCart.php';

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: {
                id: photocart
            },
            success: function (data) {
                jQuery('#photocartnum').text(data);
                jQuery('.photocartnumvisible').show();
                jQuery('.photocartnumhidden').hide();
            }
        });

    });
});