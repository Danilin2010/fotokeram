<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<form action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
    <div class="contact-page">
        <div class="contact-form">
            <div class="title">Объект</div>
            <div class="cont-column">
                <?if($arResult["input"]["id"]){?>
                    <div class="cont-column-full">
                        <label>ID</label>
                        <div class="form-control"><?=$arResult["input"]["id"]?></div>
                    </div>
                    <div class="cont-column-full">
                        <label>Название</label>
                        <div class="form-control"><?=$arResult["input"]["name"]?></div>
                    </div>
                <?}?>
                <div class="cont-column-full">
                    <label>Цвет шва</label>
                    <select name="seamcolor" class="form-control">
                        <option></option>
                        <?foreach ($arResult["SeamColor"] as $brand){?>
                            <option <?if($brand->getId()==$arResult["input"]["seamcolor"]){?>selected<?}?> value="<?=$brand->getId();?>"><?=$brand->getName();?></option>
                        <?}?>
                    </select>
                </div>

                <div class="cont-column-full">
                    <label>Тип объекта</label>
                    <select name="objecttype" class="form-control">
                        <option></option>
                        <?foreach ($arResult["ObjectType"] as $brand){?>
                            <option <?if($brand->getId()==$arResult["input"]["objecttype"]){?>selected<?}?> value="<?=$brand->getId();?>"><?=$brand->getName();?></option>
                        <?}?>
                    </select>
                </div>

                <div class="cont-column-full">
                    <label>Год</label>
                    <select name="yearlist" class="form-control">
                        <option></option>
                        <?foreach ($arResult["YearList"] as $brand){?>
                            <option <?if($brand->getId()==$arResult["input"]["year"]){?>selected<?}?> value="<?=$brand->getId();?>"><?=$brand->getValue();?></option>
                        <?}?>
                    </select>
                </div>


                <script>
                    var BrickFormat=[
                        <?foreach ($arResult["BrickFormat"] as $key=>$brand){?>
                        {
                            format: "<?=$brand->getFormat()->getName();?>",
                            color: "<?=$brand->getColor()->getName();?>",
                            brand: <?=$brand->getBrand()->getId();?>,
                            value: <?=$brand->getId();?>,
                            name: "<?=$brand->getName();?> (<?=$brand->getVendor();?>)"
                        }<?if($key<count($arResult["BrickFormat"]))?>,<?}?>
                    ];
                </script>


                <?/*<h2>Форматы кирпича</h2>*/?>

                <?

                $count=count($arResult["input"]["BrickFormat"]);
                if($count==0)
                    $count=1;
                ?>
                <?for($i=0;$i<$count;$i++){?>
                    <div class="wrapper_brickformat content">
                        <h3>Кирпич <?=$i+1?><?if($i!=0){?> <button type="button" class="button btn medium delformat">Удалить кирпич</button><?}?></h3>
                        <div class="col-xs-12 col-md-3">
                            <div class="cont-column-full">
                                <label>Бренд</label>
                                <select name="BrickFormatBrand[]" class="form-control notstyler brickformat-brand">
                                    <option>Выберите</option>
                                    <?foreach ($arResult["Brand"] as $brand){?>
                                        <option
                                            <?if($brand->getId()==$arResult["input"]["BrickFormatBrand"][$i]){?>selected<?}?>
                                            value="<?=$brand->getId();?>"><?=$brand->getName();?></option>
                                    <?}?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="cont-column-full">
                                <label>Название (артикул)</label>
                                <select name="BrickFormat[]" class="form-control notstyler brickformat" <?if($i==0){?>required<?}?>>
                                    <option>Выберите</option>
                                    <?foreach ($arResult["BrickFormat"] as $brand){?>
                                        <?if($arResult["input"]["BrickFormatBrand"][$i] && $brand->getBrand()->getId()!=$arResult["input"]["BrickFormatBrand"][$i]){continue;}?>
                                        <option
                                            <?if($brand->getId()==$arResult["input"]["BrickFormat"][$i]){?>selected<?}?>
                                            data-format="<?=$brand->getFormat()->getName();?>"
                                            data-color="<?=$brand->getColor()->getName();?>"
                                            data-brand="<?=$brand->getBrand()->getId();?>"
                                            value="<?=$brand->getId();?>"><?=$brand->getName();?> (<?=$brand->getVendor();?>)</option>
                                    <?}?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="cont-column-full">
                                <label>Формат кирпича</label>
                                <div class="form-control brickformat-format"><?=$arResult["input"]["BrickFormatFormat"][$i]?></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="cont-column-full">
                                <label>Цвет кирпича</label>
                                <div class="form-control brickformat-color"><?=$arResult["input"]["BrickFormatColor"][$i]?></div>
                            </div>
                        </div>
                    </div>
                <?}?>

                <button type="button" id="addformat" class="button btn medium">Еще кирпич</button>


                <script>
                    AddContentForm('#addformat','#addformat','' +
                        '                    <div class="wrapper_brickformat content">\n' +
                        '                        <h3>Кирпич #COUNT# <button type="button" class="button btn medium delformat">Удалить кирпич</button></h3>\n' +
                        '                        <div class="col-xs-12 col-md-3">\n' +
                        '                            <div class="cont-column-full">\n' +
                        '                                <label>Бренд</label>\n' +
                        '                                <select name="BrickFormatBrand[]" class="form-control notstyler brickformat-brand">\n' +
                        '                                   <option>Выберите</option>\n' +
                        '                                   <?foreach ($arResult["Brand"] as $brand){?>\n' +
                        '                                   <option value="<?=$brand->getId();?>"><?=$brand->getName();?></option>\n' +
                        '                                   <?}?>\n' +
                        '                                </select>\n' +
                        '                            </div>\n' +
                        '                        </div>\n' +
                        '                        <div class="col-xs-12 col-md-3">\n' +
                        '                            <div class="cont-column-full">\n' +
                        '                                <label>Название (артикул)</label>\n' +
                        '                                <select name="BrickFormat[]" class="form-control notstyler brickformat" >\n' +
                        '                                    <option>Выберите</option>\n' +
                        '                                    <?foreach ($arResult["BrickFormat"] as $brand){?>\n' +
                        '                                        <option\n' +
                        '                                                data-format="<?=$brand->getFormat()->getName();?>"\n' +
                        '                                                data-color="<?=$brand->getColor()->getName();?>"\n' +
                        '                                                data-brand="<?=$brand->getBrand()->getId();?>"\n' +
                        '                                                value="<?=$brand->getId();?>"><?=$brand->getName();?> (<?=$brand->getVendor();?>)</option>\n' +
                        '                                    <?}?>\n' +
                        '                                </select>\n' +
                        '                            </div>\n' +
                        '                        </div>\n' +
                        '                        <div class="col-xs-12 col-md-3">\n' +
                        '                            <div class="cont-column-full">\n' +
                        '                                <label>Формат кирпича</label>\n' +
                        '                                <div class="form-control brickformat-format"></div>\n' +
                        '                            </div>\n' +
                        '                        </div>\n' +
                        '                        <div class="col-xs-12 col-md-3">\n' +
                        '                            <div class="cont-column-full">\n' +
                        '                                <label>Цвет кирпича</label>\n' +
                        '                                <div class="form-control brickformat-color"></div>\n' +
                        '                            </div>\n' +
                        '                        </div>\n' +
                        '                    </div>','.wrapper_brickformat');
                </script>

<br/><br/>
                <div class="cont-column-full">
                    <label><input <?if($arResult["input"]["mix"]=="Y"){?>checked<?}?> name="mix" value="Y" type="checkbox"> МИКС</label>
                </div>

                <h2>Адрес</h2>

                <div class="cont-column-full">
                    <label>Регион</label>
                    <select name="address[regionid]" id="addressregionid" class="form-control">
                        <option value=""></option>
                        <?foreach ($arResult["Address"]["Region"] as $brand){?>
                            <option
                                <?if($brand->getKladr()==$arResult["input"]['address']["regionid"]){?>selected<?}?>
                                value="<?=$brand->getKladr();?>"><?=$brand->getName();?></option>
                        <?}?>
                    </select>
                </div>

                <div class="cont-column-full">
                    <label>Район</label>
                    <select name="address[areaid]" id="addressareaid" class="form-control">
                        <option value="">Выберите</option>
                        <?foreach ($arResult["Address"]["District"] as $brand){?>
                            <option
                                <?if($brand->getId()==$arResult["input"]['address']["areaid"]){?>selected<?}?>
                                value="<?=$brand->getId();?>"><?=$brand->getName();?> <?=$brand->getTypeShort();?></option>
                        <?}?>
                    </select>
                </div>

                <div class="cont-column-full">
                    <label>Город</label>
                    <select name="address[cityid]" id="addresscityid" class="form-control">
                        <option value="">Выберите</option>
                        <?foreach ($arResult["Address"]["City"] as $brand){?>
                            <option
                                <?if($brand->getId()==$arResult["input"]['address']["cityid"]){?>selected<?}?>
                                value="<?=$brand->getId();?>"><?=$brand->getName();?> <?=$brand->getTypeShort();?></option>
                        <?}?>
                    </select>
                </div>

                <div class="cont-column-full">
                    <label>Фотографии</label>

                    <?if(count($arResult["Photo"])>0){?>
                    <div class="gallery-block col4">
                        <?foreach ($arResult["Photo"] as $photo){?>
                            <div class="item" data-item>
                                <?
                                    $PhotoId=$photo->getPhotoId();
                                    if($PhotoId>0){
                                        $arFileTmp = CFile::ResizeImageGet(
                                            $PhotoId,
                                            array("width" => 250, "height" => 300),
                                            BX_RESIZE_IMAGE_EXACT,
                                            true//,GetWatermark()
                                        );
                                        ?><div class="image"><img src="<?=$arFileTmp["src"]?>"></div><?
                                    }
                                ?>
                                <div class="title">
                                    <div class="cont-column-full">
                                        <label><input name="delPhoto[]" value="<?=$photo->getId()?>" type="checkbox"> Удалить</label>
                                    </div>
                                </div>
                            </div>
                        <?}?>
                        <div class="clearfix" data-prev-item></div>
                    </div>
                    <?}?>

                    <div>
                        <input name="photo[]" type="file" />
                    </div>
                    <button type="button" class="button btn  medium add-file">Еще файл</button>
                </div>

                <div class="cont-column-full">
                    <input name="save" type="submit" value="Сохранить" class="btn black medium">
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
</form>