<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc as Loc;

use local\Domain\Repository\ObjectRepository;
use local\Domain\Repository\PhotoRepository;
use local\Domain\Repository\BrandRepository;
use local\Domain\Repository\SeamColorRepository;
use local\Domain\Repository\ObjectTypeRepository;
use local\Domain\Repository\BrickFormatRepository;
use Bitrix\Main\Application;
use local\Domain\Entity\ObjectObject;
use local\Domain\Entity\Address;
use local\Domain\Entity\Photo;
use Bitrix\Main\IO\File;
use local\Domain\Repository\RegionRepository;
use local\Domain\Repository\DistrictRepository;
use local\Domain\Repository\CityRepository;
use local\Domain\Repository\YearListRepository;
use local\Domain\Entity\YearList;
use Bitrix\Main\Web\Uri;

class FotokeramPhotoGalleryPersonalAdd extends CBitrixComponent
{

    private $userId=0;

	/**
	 * кешируемые ключи arResult
	 * @var array()
	 */
	protected $cacheKeys = array("Photos","Object");
	
	/**
	 * дополнительные параметры, от которых должен зависеть кеш
	 * @var array
	 */
	protected $cacheAddon = array();
	
	/**
	 * парамтеры постраничной навигации
	 * @var array
	 */
	protected $navParams = array();

    /**
     * вохвращаемые значения
     * @var mixed
     */
	protected $returned;

    /**
     * тегированный кеш
     * @var mixed
     */
    protected $tagCache;
	
	/**
	 * подключает языковые файлы
	 */
	public function onIncludeComponentLang()
	{
		$this->includeComponentLang(basename(__FILE__));
		Loc::loadMessages(__FILE__);
	}
	
    /**
     * подготавливает входные параметры
     * @param array $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }
	
	/**
	 * определяет читать данные из кеша или нет
	 * @return bool
	 */
	protected function readDataFromCache()
	{
		global $USER;
		if ($this->arParams['CACHE_TYPE'] == 'N')
			return false;

		if (is_array($this->cacheAddon))
			$this->cacheAddon[] = $USER->GetUserGroupArray();
		else
			$this->cacheAddon = array($USER->GetUserGroupArray());
		return !($this->startResultCache(false, $this->cacheAddon, md5(serialize($this->arParams))));
	}

	/**
	 * кеширует ключи массива arResult
	 */
	protected function putDataToCache()
	{
		if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0)
		{
			$this->SetResultCacheKeys($this->cacheKeys);
		}
	}

	/**
	 * прерывает кеширование
	 */
	protected function abortDataCache()
	{
		$this->AbortResultCache();
	}

    /**
     * завершает кеширование
     * @return bool
     */
    protected function endCache()
    {
        if ($this->arParams['CACHE_TYPE'] == 'N')
            return false;

        $this->endResultCache();
    }
	
	/**
	 * проверяет подключение необходиимых модулей
	 * @throws LoaderException
	 */
	protected function checkModules()
	{

	}
	
	/**
	 * проверяет заполнение обязательных параметров
	 * @throws SystemException
	 */
	protected function checkParams()
	{

	}
	
	/**
	 * выполяет действия перед кешированием 
	 */
	protected function executeProlog()
	{

	}

    /**
     * Определяет ID инфоблока по коду, если не был задан
     */
	protected function getIblockId()
    {

    }

    protected function chekUserId(){

        global $USER;
        $this->userId=$USER->getID();

        if($this->userId<=0){


            return false;
        }
        return true;
    }

    /**
     * @param ObjectObject $Object
     */
    protected function getInput(ObjectObject $Object){
        $this->arResult["input"]=[
        ];
        if($Object->getName())
            $this->arResult["input"]["name"]=$Object->getName();
        //if($Object->getBrand())
        //    $this->arResult["input"]["brand"]=$Object->getBrand()->getId();
        if($Object->getSeamColor())
            $this->arResult["input"]["seamcolor"]=$Object->getSeamColor()->getId();
        $this->arResult["input"]["mix"]=$Object->isMix()?'Y':'';

        $this->arResult["input"]["BrickFormat"]=[];
        $this->arResult["input"]["BrickFormatFormat"]=[];
        $this->arResult["input"]["BrickFormatColor"]=[];
        $this->arResult["input"]["BrickFormatBrand"]=[];
        foreach ($Object->getBrick() as $Brick)
        {
            $this->arResult["input"]["BrickFormat"][]=$Brick->getId();
            $this->arResult["input"]["BrickFormatFormat"][]=$Brick->getFormat()->getName();
            $this->arResult["input"]["BrickFormatColor"][]=$Brick->getColor()->getName();
            $this->arResult["input"]["BrickFormatBrand"][]=$Brick->getBrand()->getId();
        }
        $this->arResult["input"]["address"]=[];
        $this->arResult["input"]["address"]["regionid"]=$Object->getAddress()->getRegionId();
        $this->arResult["input"]["address"]["areaid"]=$Object->getAddress()->getAreaId();
        $this->arResult["input"]["address"]["cityid"]=$Object->getAddress()->getCityId();

        $this->arResult["input"]["address"]["coords1"]=$Object->getAddress()->getCoords1();
        $this->arResult["input"]["address"]["coords2"]=$Object->getAddress()->getCoords2();

        if($Object->getYear() && $Object->getYear()->getId())
            $this->arResult["input"]["year"]=$Object->getYear()->getId();

        if($Object->getId())
            $this->arResult["input"]["id"]=$Object->getId();

        if($Object->getObjectType())
            $this->arResult["input"]["objecttype"]=$Object->getObjectType();

    }

    /**
     * @throws \Exception
     */
    protected function getRequest(){

        $ObjectRepository=new ObjectRepository();
        $BrandRepository=new BrandRepository();
        $SeamColorRepository=new SeamColorRepository();
        $ObjectTypeRepository=new ObjectTypeRepository();
        $BrickFormatRepository=new BrickFormatRepository();
        $PhotoRepository=new PhotoRepository();
        $RegionRepository=new RegionRepository();
        $DistrictRepository=new DistrictRepository();
        $CityRepository=new CityRepository();
        $YearListRepository=new YearListRepository();

        $request = Application::getInstance()->getContext()->getRequest();

        $save = $request->getPost("save");

        $cnt = $ObjectRepository->getCount([]);

        if(strlen($save)>0){
            //$name = $request->getPost("name");
            $name=$cnt+1;
            $seamcolor = $request->getPost("seamcolor");
            $objecttype = $request->getPost("objecttype");
            $mix = $request->getPost("mix");
            $BrickFormat = $request->getPost("BrickFormat");
            $address = $request->getPost("address");

            $photo = $request->getFile("photo");

            $delPhoto = $request->getPost("delPhoto");

            $yearlist = (int)$request->getPost("yearlist");

            if((int)$this->arParams["ELEMENT_ID"]>0){
                $Object=$ObjectRepository->getById($this->arParams["ELEMENT_ID"]);
                if($this->userId!=$Object->getAuthor())
                    throw new Exception(Loc::getMessage('AUTHENTICATION_ERROR'));
            }else{
                $Object=new ObjectObject();
                $Object->setAuthor($this->userId);
                if(strlen($name)>0)
                    $Object->setName($name);
            }

            //if((int)$brand>0)
            //    $Object->setBrand($BrandRepository->getById((int)$brand));

            if((int)$seamcolor>0)
                $Object->setSeamColor($SeamColorRepository->getById((int)$seamcolor));
            if((int)$objecttype>0)
                $Object->setObjectType((int)$objecttype);

            if($mix=='Y')
                $Object->setMix(true);
            else
                $Object->setMix(false);


            if($BrickFormat){
                $newBrick=[];
                foreach ($BrickFormat as $Brick){
                    if((int)$Brick>0)
                    {
                        $BrickObject=$BrickFormatRepository->getById((int)$Brick);
                        if($BrickObject)
                            $newBrick[]=$BrickObject;
                    }
                }
                $Object->setBrick($newBrick);
            }

            if($address){
                if($Object->getAddress())
                    $AddressObject=$Object->getAddress();
                else
                    $AddressObject=new Address();
                $AddressObject->setName($Object->getName());

                $AddressAddress=[
                    ''
                ];

                $AddressObject->setIndex('');
                $AddressObject->setCityId('');
                $AddressObject->setCity('');
                $AddressObject->setRegionId('');
                $AddressObject->setRegion('');
                $AddressObject->setAreaId('');
                $AddressObject->setArea('');

                if($address["regionid"])
                {
                    $Region=$RegionRepository->getByKladr($address["regionid"]);
                    $AddressObject->setRegionId($Region->getKladr());
                    $AddressObject->setRegion($Region->getName());
                    $AddressAddress[]=$Region->getName();
                }

                if($address["areaid"])
                {
                    $District=$DistrictRepository->getById($address["areaid"]);
                    $AddressObject->setIndex($District->getZip());
                    $AddressObject->setAreaId($District->getId());
                    $AddressObject->setArea($District->getName().' '.$District->getType());
                    $AddressAddress[0]=$District->getZip();
                    $AddressAddress[]=$District->getName().' '.$District->getType();
                }

                if($address["cityid"])
                {
                    $City=$CityRepository->getById($address["cityid"]);
                    $AddressObject->setIndex($City->getZip());
                    $AddressObject->setCityId($City->getId());
                    $AddressObject->setCity($City->getName().' '.$City->getType());
                    $AddressAddress[0]=$City->getZip();
                    $AddressAddress[]=$City->getName().' '.$City->getType();
                }

                if($address["coords1"])
                {
                    $AddressObject->setCoords1($address["coords1"]);
                }

                if($address["coords2"])
                {
                    $AddressObject->setCoords2($address["coords2"]);
                }

                $AddressObject->setAddress(implode(', ',$AddressAddress));

                $Object->setAddress($AddressObject);
            }

            if($yearlist>0){
                $YearList=$YearListRepository->getById($yearlist);
                if($YearList)
                    $Object->setYear($YearList);
            }else{
                $Object->setYear(new YearList());
            }

            $this->getInput($Object);

            if($Object->getId()>0)
                $ObjectRepository->Update($Object->getId(),$Object);
            else
                $Object->setId($ObjectRepository->Add($Object));



            if(is_array($photo)){
                foreach ($photo["tmp_name"] as $key=>$name){
                    if($photo["size"][$key]>0)
                    {
                        $File=new File($name);
                        $File->rename(Application::getDocumentRoot()."/upload/tmp/photo/".$photo["name"][$key]);
                        $Photo=new Photo();
                        $Photo->setName($Object->getName());
                        $Photo->setObject([$Object]);
                        $Photo->setPhoto($File);
                        $PhotoRepository->Add($Photo);
                        $File->delete();
                    }
                }
            }

            if(is_array($delPhoto))
                foreach ($delPhoto as $del)
                    $PhotoRepository->Delete($del);

            $this->arResult["Object"]=$Object;

            $uriString=$this->arParams["LIST_URL_TEMPLATES"];
            $uriString=str_replace("#ELEMENT_ID#",$Object->getId(),$uriString);
            $uri = new Uri($uriString);
            $uri->addParams(array("add_object" => $Object->getId()));
            \LocalRedirect($uri->getUri());
        }else{
            if((int)$this->arParams["ELEMENT_ID"]>0) {
                $Object = $ObjectRepository->getById($this->arParams["ELEMENT_ID"]);
                if ($this->userId != $Object->getAuthor())
                    throw new Exception(Loc::getMessage('AUTHENTICATION_ERROR'));

                $this->arResult["Object"] = $Object;
                $this->getInput($Object);
            }
        }

        //echo "<pre>";print_r($this->arParams);echo "</pre>";

        $this->arResult["Brand"]=$BrandRepository->getList();
        $this->arResult["SeamColor"]=$SeamColorRepository->getList();
        $this->arResult["ObjectType"]=$ObjectTypeRepository->getList();
        $this->arResult["BrickFormat"]=$BrickFormatRepository->getList();
        $this->arResult["Photo"]=[];
        if($Object && $Object->getId()>0){
            $this->arResult["Photo"]=$PhotoRepository->getList([],[
                "object_id"=>$Object->getId()
            ],[],false,false,[
                'id',
                'name',
                'sort',
                'photo',
            ]);
        }


        $this->arResult["Address"]=[
            "Region"=>$RegionRepository->getList(),
            "District"=>[],
            "City"=>[],
        ];

        if($this->arResult["input"]['address']['regionid']){
            $this->arResult["Address"]["District"]=$DistrictRepository->getListByIdRegion($this->arResult["input"]['address']['regionid']);
        }

        if($this->arResult["input"]['address']['areaid']){
            $this->arResult["Address"]["City"]=$CityRepository->getListByIdDistrict($this->arResult["input"]['address']['areaid']);
        }elseif($this->arResult["input"]['address']['regionid']){
            $this->arResult["Address"]["City"]=$CityRepository->getListByIdRegion($this->arResult["input"]['address']['regionid']);
        }


        $this->arResult["YearList"]=$YearListRepository->getYearList([
            '>=VALUE'=>2000,
            '<=VALUE'=>(int)date('Y'),
        ]);

        //echo "<pre>";print_r($this->arResult["input"]);echo "</pre>";

    }



	/**
	 * получение результатов
     * @throws Exception
	 */
	protected function getResult()
	{


        $this->getRequest();


	}
	
	/**
	 * выполняет действия после выполения компонента, например установка заголовков из кеша
	 */
	protected function executeEpilog()
	{

	}
	
	/**
	 * выполняет логику работы компонента
	 */
	public function executeComponent()
	{
		global $APPLICATION;
		try
		{

            if($this->chekUserId())
            {
                $this->checkModules();
                $this->checkParams();
                $this->executeProlog();
                if ($this->arParams['AJAX'] == 'Y')
                    $APPLICATION->RestartBuffer();

                $this->getIblockId();
                $this->getResult();
                $this->includeComponentTemplate();

                $this->executeEpilog();

                if ($this->arParams['AJAX'] == 'Y')
                    die();
            }



			return $this->returned;
		}
		catch (Exception $e)
		{
			$this->abortDataCache();
			ShowError($e->getMessage());
		}
	}
}
?>