<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?use \local\Services\PhotoCart;?>
<?
global $FotokeramPhotoGalleryFilter;
$FotokeramPhotoGalleryFilter=[
    "ObjectId"=>$arResult["VARIABLES"]["ELEMENT_ID"]
];
?>

<?$APPLICATION->IncludeComponent(
    "fotokeram:photo.gallery.list",
    "",
    Array(
        "PHOTOCART"=>PhotoCart::getPhoto(),
        "PAGE_SIZE"=>500,
        "FOLDER" => $arResult["FOLDER"],
        "URL_TEMPLATES" => $arResult["URL_TEMPLATES"],
    )
);?>

