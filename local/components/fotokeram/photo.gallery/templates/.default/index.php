<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?use \local\Services\PhotoCart;?>
<?$this->SetViewTarget('headerfilter'); ?>
<?$APPLICATION->IncludeComponent(
    "fotokeram:photo.gallery.filter",
    "header",
    Array(

    )
);?>
<?$this->EndViewTarget(); ?>
<?$APPLICATION->IncludeComponent(
    "fotokeram:photo.gallery.list",
    "",
    Array(
        "PHOTOCART"=>PhotoCart::getPhoto(),
        "FOLDER" => $arResult["FOLDER"],
        "URL_TEMPLATES" => $arResult["URL_TEMPLATES"],
    )
);?>
