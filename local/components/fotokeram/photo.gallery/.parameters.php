<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__); 

try
{
	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
			'SEF_MODE' => array(
	            'index' => array(
	                'NAME' => GetMessage('STANDARD_ELEMENTS_PARAMETERS_INDEX_PAGE'),
	                'DEFAULT' => 'index.php',
	                'VARIABLES' => array()
	            ),
	            'detail' => array(
	            	"NAME" => GetMessage('STANDARD_ELEMENTS_PARAMETERS_DETAIL_PAGE'),
	                "DEFAULT" => '#ELEMENT_ID#/',
	                "VARIABLES" => array('ELEMENT_ID')
				)
	        )
		)
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e->getMessage());
}
?>