//fancybox
jQuery.ajaxSetup({complete: function(){
    jQuery("input, select").styler({
        selectSearch: true
    });
}});
jQuery(document).ready(function() {
    jQuery('.fancybox').fancybox({
        beforeLoad: function() {
            var el,
                element=$(this.element),
                id=element.data('title-id'),
                useUserGroup=element.data('useUserGroup'),
                photocart=element.data('photocart'),
                url=element.data('url')
            ;

            if (id) {
                el = $('#' + id);
                if (el.length) {
                    var input=$('input[data-photocart='+photocart+']');
                    var checked=input.prop('checked');
                    var checkedtext='';
                    if(checked)
                        checkedtext=' checked ';
                    this.title = el.html();
                    if(useUserGroup){
                        this.title+=' <label><input '+checkedtext+' data-photocart="'+photocart+'" type="checkbox" value="'+photocart+'" name="PhotoCart[]"/> Выбрать </label>'
                            +' <div class="clearfix"></div>';
                    }
                    this.title+=' <a href="'+url+'" target="_blank">Другие фото объекта</a>'
                    ;
                }
            }
        },
        afterShow:function(){
            $('.fancybox-title').find('input, select').styler({
                selectSearch: true
            });
            //console.log(this);
        },
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
});
//NivoSlider
jQuery(window).load(function() {
    jQuery('#slider').nivoSlider(
	 	{effect: 'fold,fade,sliceDown'}
	 );
});
//fixed header	
jQuery(window).scroll(function() {
    if (jQuery(window).scrollTop() > 118) {
        jQuery('.header, .wrapper').addClass('fixed');
    }
    else {
        jQuery('.header, .wrapper').removeClass('fixed');
    }
});
//input styler
(function(jQuery) {
    jQuery(function() {
        jQuery('input, select').styler({
            selectSearch: true
        });
    });
})(jQuery);
//filter spoiler
jQuery(document).ready(function() {
    jQuery('.spoiler-body').show()
    jQuery('.spoiler-head').click(function() {
        jQuery(this).toggleClass("folded").toggleClass("unfolded").next().toggle()
    })
})
jQuery(document).ready(function() {
        jQuery('.spoiler-body-closed').hide()
        jQuery('.spoiler-head-closed').click(function() {
            jQuery(this).toggleClass("folded").toggleClass("unfolded").next().toggle()
        })
    })
//price scale
jQuery(document).ready(function() {
    jQuery('.price-slider').jRange({
        from: 0,
        to: 10000,
        step: 100,
        scale: [0, 10000],
        format: '%s',
        width: 188,
        showLabels: true,
        showScale: false,
        isRange: true
    });
});
//scroll to top
jQuery(document).ready(function() {
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > 0) {
            jQuery('#scroll-to-top').fadeIn();
        }
        else {
            jQuery('#scroll-to-top').fadeOut();
        }
    });
    jQuery('#scroll-to-top').click(function() {
        jQuery('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
});














