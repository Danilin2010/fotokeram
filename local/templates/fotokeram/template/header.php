<?php
/**
 * @global \CMain $APPLICATION
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);

$asset = Asset::getInstance();

?><!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php $APPLICATION->ShowTitle() ?></title>

    <?php $APPLICATION->ShowHead() ?>

    <?

    $asset->addCss(SITE_TEMPLATE_PATH."/bootstrap-3.3.4/css/bootstrap.min.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/font-awesome-4.3.0/css/font-awesome.min.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/themes/default/default.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/nivo-slider.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/flaticon/flaticon.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/jquery.formstyler.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/style.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/fancybox/jquery.fancybox.css?v=2.1.5");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/jquery.range.css");

    $asset->addCss(SITE_TEMPLATE_PATH."/css/owl-carousel/owl.carousel.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/owl-carousel/owl.theme.css");

    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery-1.10.2.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/tabs_localStorage.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.nivo.slider.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/template.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/fancybox/jquery.fancybox.pack.js?v=2.1.5");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.formstyler.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.range-min.js");

    $asset->addJs(SITE_TEMPLATE_PATH."/js/validation/jquery.validate.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/validation/additional-methods.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/validation/localization/messages_ru.js");

    $asset->addJs(SITE_TEMPLATE_PATH."/css/owl-carousel/owl.carousel.min.js");

    ?>



</head>

<body>

<? $APPLICATION->ShowPanel() ?>

<div class="wrapper">
    <div class="content">
        <!--Header-->
        <div class="header">
            <div class="logo"><a href="/">Logo</a></div>
            <div class="contacts">
                <div class="content-panding">
                    <?$APPLICATION->ShowViewContent('headerfilter');?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="header-inside"></div>
        <!--Content-->
        <div class="content-padding inside">
            <div class="column-left">

                <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"column-left", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "column-left"
	),
	false
);?>
                <?$APPLICATION->IncludeComponent(
                    "fotokeram:photo.cart",
                    "",
                    Array()
                );?>
                <? $APPLICATION->ShowViewContent('filter'); ?>
            </div>
            <div class="column-right">

                <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
	"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
		"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
		"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
	),
	false
);?>
                <div class="page-header-name">
                    <h1><?php $APPLICATION->ShowTitle(false) ?> <? $APPLICATION->ShowViewContent('afterh1'); ?></h1>
                </div>
                <div class="clearfix"></div>

