<?php
/**
 * @global \CMain $APPLICATION
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="clearfix"></div>


</div>

<div class="clearfix"></div>
</div>
</div>
<div class="footer">
    <div class="foot-menu">

        <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
    </div>
    <div class="contacts">
        <div class="left">

        </div>
        <div class="right">

        </div>
    </div>
    <div class="copyright"><?php $APPLICATION->IncludeFile(
            "/inc/copyright.php",
            Array(),
            Array("MODE" => "php", "SHOW_BORDER" => false)
        ) ?></div>
    <div class="clearfix"></div>
</div>
</div>
<div id="scroll-to-top"></div>
</body>
</html>
