<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<div class="obj-props-form">

<?if($arParams["~AUTH_RESULT"]){?>
    <div class="photo-container row div-1520 gallery_cont">
        <div class="error_message">
            <?ShowMessage($arParams["~AUTH_RESULT"]);?>
        </div>
    </div>
<?}?>
<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) &&  $arParams["AUTH_RESULT"]["TYPE"] === "OK"):?>
    <div class="photo-container row div-1520 gallery_cont">
        <div class="error_message">
            <?echo GetMessage("AUTH_EMAIL_SENT")?>
        </div>
    </div>
<?else:?>
<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
    <div class="photo-container row div-1520 gallery_cont">
        <div class="error_message">
            <?echo GetMessage("AUTH_EMAIL_WILL_BE_SENT")?>
        </div>
    </div>
<?endif?>

    <form method="post" id="objectprops" class="form_reg" action="<?=$arResult["AUTH_URL"]?>" name="bform" enctype="multipart/form-data">
        <?
        if (strlen($arResult["BACKURL"]) > 0)
        {
            ?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?
        }
        ?>
        <input type="hidden" name="AUTH_FORM" value="Y" />
        <input type="hidden" name="TYPE" value="REGISTRATION" />
        <div class="col-md-12">
            <div class="parameters">
                <span class="title"><?=GetMessage("AUTH_REGISTER")?></span>

                <label for="USER_NAME"><?=GetMessage("AUTH_NAME")?></label>
                <input type="text" id="USER_NAME" name="USER_NAME" maxlength="50" value="<?=$arResult["USER_NAME"]?>" class="text" />

                <label for="USER_LAST_NAME"><?=GetMessage("AUTH_LAST_NAME")?></label>
                <input type="text" id="USER_LAST_NAME" name="USER_LAST_NAME" maxlength="50" value="<?=$arResult["USER_LAST_NAME"]?>" class="text" />

                <label for="USER_LOGIN"><span class="starrequired">*</span><?=GetMessage("AUTH_LOGIN_MIN")?></label>
                <input type="text" id="USER_LOGIN" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" class="text" />

                <label for="USER_PASSWORD"><span class="starrequired">*</span><?=GetMessage("AUTH_PASSWORD_REQ")?></label>
                <input type="password" id="USER_PASSWORD" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" class="text" autocomplete="off" />
                <?if($arResult["SECURE_AUTH"]):?>
                    <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
                    <noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
                    </noscript>
                    <script type="text/javascript">
                        document.getElementById('bx_auth_secure').style.display = 'inline-block';
                    </script>
                <?endif?>

                <label for="USER_CONFIRM_PASSWORD"><span class="starrequired">*</span><?=GetMessage("AUTH_CONFIRM")?></label>
                <input type="text" id="USER_CONFIRM_PASSWORD" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" class="text" autocomplete="off" />

                <p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>

                <label for="USER_EMAIL"><?if($arResult["EMAIL_REQUIRED"]):?><span class="starrequired">*</span><?endif?><?=GetMessage("AUTH_EMAIL")?></label>
                <input type="text" id="USER_EMAIL" name="USER_EMAIL" maxlength="50" value="<?=$arResult["USER_EMAIL"]?>" class="text" />

                <?// ********************* User properties ***************************************************?>
                <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
                <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
                <label><?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;
                        ?></label><?=$arUserField["EDIT_FORM_LABEL"]?>:</td><td>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:system.field.edit",
                            $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                            array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "bform"), null, array("HIDE_ICONS"=>"Y"));?></td></tr>
                <?endforeach;?>
                <?endif;?>
                <?// ******************** /User properties ***************************************************?>

                <?
                /* CAPTCHA */
                if ($arResult["USE_CAPTCHA"] == "Y")
                {
                    ?>
                    <div class="clearfix"></div>
                    <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                    <label for="captcha_word"><span class="starrequired">*</span><?=GetMessage("CAPTCHA_REGF_PROMT")?></label>
                    <img style="margin-bottom: 5px;" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                    <input style="width: auto" type="text" id="captcha_word" name="captcha_word" maxlength="50" value="" class="text" />
                    <?
                }
                /* CAPTCHA */
                ?>

                <?$APPLICATION->IncludeComponent("bitrix:main.userconsent.request", "",
                    array(
                        "ID" => COption::getOptionString("main", "new_user_agreement", ""),
                        "IS_CHECKED" => "Y",
                        "AUTO_SAVE" => "N",
                        "IS_LOADED" => "Y",
                        "ORIGINATOR_ID" => $arResult["AGREEMENT_ORIGINATOR_ID"],
                        "ORIGIN_ID" => $arResult["AGREEMENT_ORIGIN_ID"],
                        "INPUT_NAME" => $arResult["AGREEMENT_INPUT_NAME"],
                        "REPLACE" => array(
                            "button_caption" => GetMessage("AUTH_REGISTER"),
                            "fields" => array(
                                rtrim(GetMessage("AUTH_NAME"), ":"),
                                rtrim(GetMessage("AUTH_LAST_NAME"), ":"),
                                rtrim(GetMessage("AUTH_LOGIN_MIN"), ":"),
                                rtrim(GetMessage("AUTH_PASSWORD_REQ"), ":"),
                                rtrim(GetMessage("AUTH_EMAIL"), ":"),
                            )
                        ),
                    )
                );?>

                <div class="clearfix"></div>

                <p>&nbsp;</p>

                <input type="submit" class="button" name="Register" value="<?=GetMessage("AUTH_REGISTER")?>" />

                <p>&nbsp;</p>

                <p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>

                <p><a href="<?=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow"><b><?=GetMessage("AUTH_AUTH")?></b></a></p>

            </div>
        </div>
    </form>
    <script type="text/javascript">
        document.bform.USER_NAME.focus();
    </script>
</div>
<?endif?>