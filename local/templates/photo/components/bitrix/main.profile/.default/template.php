<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<div class="obj-props-form">

    <?if($arParams["strProfileError"]){?>
        <div class="photo-container row div-1520 gallery_cont">
            <div class="error_message">
                <?ShowMessage($arParams["strProfileError"]);?>
            </div>
        </div>
    <?}?>

    <?if ($arResult['DATA_SAVED'] == 'Y'){?>
        <div class="photo-container row div-1520 gallery_cont">
            <div class="error_message">
                <?ShowNote(GetMessage('PROFILE_DATA_SAVED'));?>
            </div>
        </div>
    <?}?>

    <form method="post" id="objectprops" name="form1" class="form_reg" action="<?=$arResult["AUTH_URL"]?>" enctype="multipart/form-data">
        <?=$arResult["BX_SESSION_CHECK"]?>
        <input type="hidden" name="lang" value="<?=LANG?>" />
        <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />


        <div class="col-md-12">
            <div class="parameters">

                <label for="TITLE"><?=GetMessage("main_profile_title")?></label>
                <input type="text" id="TITLE" name="TITLE" maxlength="50" value="<?=$arResult["arUser"]["TITLE"]?>" class="text" />

                <label for="NAME"><?=GetMessage("NAME")?></label>
                <input type="text" id="NAME" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" class="text" />

                <label for="LAST_NAME"><?=GetMessage("LAST_NAME")?></label>
                <input type="text" id="LAST_NAME" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" class="text" />

                <label for="SECOND_NAME"><?=GetMessage("SECOND_NAME")?></label>
                <input type="text" id="SECOND_NAME" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" class="text" />

                <label for="EMAIL"><?if($arResult["EMAIL_REQUIRED"]):?><span class="starrequired">*</span><?endif?><?=GetMessage("EMAIL")?></label>
                <input type="text" id="EMAIL" name="EMAIL" maxlength="50" value="<?=$arResult["arUser"]["EMAIL"]?>" class="text" />

                <label for="LOGIN"><span class="starrequired">*</span><?=GetMessage("LOGIN")?></label>
                <input type="text" id="LOGIN" name="LOGIN" maxlength="50" value="<?=$arResult["arUser"]["LOGIN"]?>" class="text" />

                <?if($arResult['CAN_EDIT_PASSWORD']):?>
                    <label for="NEW_PASSWORD"><?=GetMessage("NEW_PASSWORD_REQ")?></label>
                    <input type="password" id="NEW_PASSWORD" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" class="text" />
                <?if($arResult["SECURE_AUTH"]):?>
                    <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none"><div class="bx-auth-secure-icon"></div></span>
                    <noscript><span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>"><div class="bx-auth-secure-icon bx-auth-secure-unlock"></div></span></noscript>
                    <script type="text/javascript">
                        document.getElementById('bx_auth_secure').style.display = 'inline-block';
                    </script>
                <?endif?>
                    <label for="NEW_PASSWORD_CONFIRM"><?=GetMessage("NEW_PASSWORD_CONFIRM")?></label>
                    <input type="password" id="NEW_PASSWORD_CONFIRM" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" class="text" />
                <?endif?>

                <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>

                    <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>


                        <label for="LAST_NAME"><?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;?><?=$arUserField["EDIT_FORM_LABEL"]?></label>



                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:system.field.edit",
                                    $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                    array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS"=>"Y"));?>
                    <?endforeach;?>


                <?endif;?>

                <p>&nbsp;</p>

                <p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>

                <div class="clearfix"></div>

                <p>&nbsp;</p>

                <p><input type="submit" class="button" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
                    &nbsp;
                    &nbsp;
                    <input type="reset" class="button" value="<?=GetMessage('MAIN_RESET');?>"></p>
                <p>&nbsp;</p>


            </div>
        </div>
    </form>
    <?
    if($arResult["SOCSERV_ENABLED"])
    {
        $APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
            "SHOW_PROFILES" => "Y",
            "ALLOW_DELETE" => "Y"
        ),
            false
        );
    }
    ?>
    <p>&nbsp;</p>
</div>