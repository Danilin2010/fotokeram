<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="clearfix"></div>
<div class="paginator">
    <ul>
        <?foreach ($arResult["NAV"]["URL"]["SOME_PAGE"] as $key=>$url){?>
            <li <?if($arResult["NAV"]["PAGE_NUMBER"]==$key){?>class = "active"<?}?>><a href="<?=$url?>"><?=$key?></a></li>
        <?}?>
    </ul>
</div>