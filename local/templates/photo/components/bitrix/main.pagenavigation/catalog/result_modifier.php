<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// page numbers


$arResult["NAV"]["RECORD_COUNT"] = $arResult["RECORD_COUNT"];
$arResult["NAV"]["PAGE_COUNT"] = $arResult["PAGE_COUNT"];
$arResult["NAV"]["PAGE_NUMBER"] = $arResult["CURRENT_PAGE"];
$arResult["NAV"]["PAGE_SIZE"] = $arResult["PAGE_SIZE"];
$arResult["NAV"]["START_PAGE"] = $arResult["START_PAGE"];
$arResult["NAV"]["END_PAGE"] = $arResult["END_PAGE"];
$arResult["NAV"]["SHOW_ALL_MODE"] = $arResult["NavShowAll"];
$arResult["NAV"]["DO_SHOW_ALL"] = $arResult["bShowAll"];

$arResult["NAV"]["END_PAGE"] = $arResult["END_PAGE"];
$arResult["NAV"]["START_PAGE"] = $arResult["START_PAGE"];

$arResult["NavQueryString"] = str_replace('&amp;', '&', $arResult["NavQueryString"]);

parse_str($arResult["NavQueryString"], $GLOBALS["NAV"]["parsed_url"]);

// filename
$GLOBALS["NAV"]["nav_filename"] = $arResult["sUrlPath"];

$arResult["NAV"]["URL_TEMPLATE"] = $arResult["URL_TEMPLATE"];

// identificator of pager
$arResult["NAV"]["PAGER_ID"] = "PAGEN_".$arResult["NavNum"]; 
$arResult["NAV"]["SHOWALL_ID"] = "SHOWALL_".$arResult["NavNum"]; 

if(!function_exists("MakeNewNavUrl"))
{
	function MakeNewNavUrl($UrlTemplate,$arAdd)
	{
	   $nav_url = str_replace(array_keys($arAdd),array_values($arAdd),$UrlTemplate);
	   return $nav_url;
	}
}
// first page url
$arResult["NAV"]["URL"]["FIRST_PAGE"] = MakeNewNavUrl($arResult["NAV"]["URL_TEMPLATE"],array('--page--'=>$arResult["NAV"]["START_PAGE"]));

// previous page url
$arResult["NAV"]["URL"]["PREV_PAGE"]  = MakeNewNavUrl($arResult["NAV"]["URL_TEMPLATE"],array('--page--'=>$arResult["NAV"]["PAGE_NUMBER"]-1));

// last page url
$arResult["NAV"]["URL"]["LAST_PAGE"]  = MakeNewNavUrl($arResult["NAV"]["URL_TEMPLATE"],array('--page--'=>$arResult["NAV"]["PAGE_COUNT"]));

// next page url
$arResult["NAV"]["URL"]["NEXT_PAGE"] = MakeNewNavUrl($arResult["NAV"]["URL_TEMPLATE"],array('--page--'=>$arResult["NAV"]["PAGE_NUMBER"]+1));

// some page url
for ($i=$arResult["NAV"]["START_PAGE"]; $i<=$arResult["NAV"]["END_PAGE"]; $i++)
   $arResult["NAV"]["URL"]["SOME_PAGE"][$i] = MakeNewNavUrl($arResult["NAV"]["URL_TEMPLATE"],array('--page--'=>$i));


?>