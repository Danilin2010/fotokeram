<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<div class="obj-props-form">

    <?if($arParams["~AUTH_RESULT"]){?>
        <div class="photo-container row div-1520 gallery_cont">
            <div class="error_message">
                <?ShowMessage($arParams["~AUTH_RESULT"]);?>
            </div>
        </div>
    <?}?>

    <?if($arParams["ERROR_MESSAGE"]){?>
        <div class="photo-container row div-1520 gallery_cont">
            <div class="error_message">
                <?ShowMessage($arParams["ERROR_MESSAGE"]);?>
            </div>
        </div>
    <?}?>

    <form method="post" id="objectprops" target="_top" class="form_reg" action="<?=$arResult["AUTH_URL"]?>" name="form_auth">

        <input type="hidden" name="AUTH_FORM" value="Y" />
        <input type="hidden" name="TYPE" value="AUTH" />
        <?if (strlen($arResult["BACKURL"]) > 0):?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <?endif?>
        <?foreach ($arResult["POST"] as $key => $value):?>
            <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
        <?endforeach?>


        <div class="col-md-12">
            <div class="parameters">

                <span class="title"><?=GetMessage("AUTH_TITLE")?></span>

                <label for="USER_LOGIN"><span class="starrequired">*</span><?=GetMessage("AUTH_LOGIN")?></label>
                <input type="text" id="USER_LOGIN" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" class="text" />

                <label for="USER_PASSWORD"><span class="starrequired">*</span><?=GetMessage("AUTH_PASSWORD")?></label>
                <input type="password" id="USER_PASSWORD" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" class="text" autocomplete="off" />
                <?if($arResult["SECURE_AUTH"]):?>
                    <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
                    <noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
                    </noscript>
                    <script type="text/javascript">
                        document.getElementById('bx_auth_secure').style.display = 'inline-block';
                    </script>
                <?endif?>

                <?
                /* CAPTCHA */
                if ($arResult["USE_CAPTCHA"] == "Y")
                {
                    ?>
                    <div class="clearfix"></div>
                    <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                    <label for="captcha_word"><span class="starrequired">*</span><?=GetMessage("AUTH_CAPTCHA_PROMT")?></label>
                    <img style="margin-bottom: 5px;" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                    <input style="width: auto" type="text" id="captcha_word" name="captcha_word" maxlength="50" value="" class="text" />
                    <?
                }
                /* CAPTCHA */
                ?>

                <?if ($arResult["STORE_PASSWORD"] == "Y"):?>
                    <label for="USER_REMEMBER" class="container-check" style="float: inherit;max-width: 50%;"><?=GetMessage("AUTH_REMEMBER_ME")?>
                        <input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" />
                        <span class="checkmark"></span>
                    </label>
                <?endif?>

                <div class="clearfix"></div>

                <p>&nbsp;</p>

                <input type="submit" class="button" name="Login" value="<?=GetMessage("AUTH_AUTHORIZE")?>" />

                <p>&nbsp;</p>

                <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
                    <noindex>
                        <p>
                            <a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
                        </p>
                    </noindex>
                <?endif?>

                <?if($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
                    <noindex>
                        <p>
                            <a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a><br />
                            <?=GetMessage("AUTH_FIRST_ONE")?>
                        </p>
                    </noindex>
                <?endif?>

                <script type="text/javascript">
                    <?if (strlen($arResult["LAST_LOGIN"])>0):?>
                    try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
                    <?else:?>
                    try{document.form_auth.USER_LOGIN.focus();}catch(e){}
                    <?endif?>
                </script>

                <?if($arResult["AUTH_SERVICES"]):?>
                    <?
                    $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
                        array(
                            "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                            "CURRENT_SERVICE" => $arResult["CURRENT_SERVICE"],
                            "AUTH_URL" => $arResult["AUTH_URL"],
                            "POST" => $arResult["POST"],
                            "SHOW_TITLES" => $arResult["FOR_INTRANET"]?'N':'Y',
                            "FOR_SPLIT" => $arResult["FOR_INTRANET"]?'Y':'N',
                            "AUTH_LINE" => $arResult["FOR_INTRANET"]?'N':'Y',
                        ),
                        $component,
                        array("HIDE_ICONS"=>"Y")
                    );
                    ?>
                    <p>&nbsp;</p>
                <?endif?>

            </div>
        </div>
    </form>
    <script type="text/javascript">
        document.bform.USER_LOGIN.focus();
    </script>
</div>