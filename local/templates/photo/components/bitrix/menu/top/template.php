<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <ul class="navbar-nav">
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
		continue;
?>
    <li class="nav-item active">
        <a class="nav-link <?if($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>">
            <i class="<?=$arItem["PARAMS"]["i"]?>"></i>
            <span><?=$arItem["TEXT"]?></span>
        </a>
    </li>
<?endforeach?>
</ul>
<?endif?>
