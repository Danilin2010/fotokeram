<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="obj-props-form">

    <?if($arParams["~AUTH_RESULT"]){?>
        <div class="photo-container row div-1520 gallery_cont">
            <div class="error_message">
                <?ShowMessage($arParams["~AUTH_RESULT"]);?>
            </div>
        </div>
    <?}?>

    <form method="post" id="objectprops" target="_top" class="form_reg" action="<?=$arResult["AUTH_URL"]?>" name="bform">
        <?
        if (strlen($arResult["BACKURL"]) > 0)
        {
            ?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?
        }
        ?>
        <input type="hidden" name="AUTH_FORM" value="Y">
        <input type="hidden" name="TYPE" value="SEND_PWD">


        <div class="col-md-12">
            <div class="parameters">

                <p><?=GetMessage("AUTH_FORGOT_PASSWORD_1")?></p>

                <span class="title"><?=GetMessage("AUTH_GET_CHECK_STRING")?></span>

                <label for="LAST_LOGIN"><?=GetMessage("AUTH_LOGIN")?></label>
                <input type="text" id="LAST_LOGIN" name="LAST_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" class="text" />

                <p><?=GetMessage("AUTH_OR")?></p>

                <label for="USER_EMAIL"><?=GetMessage("AUTH_EMAIL")?></label>
                <input type="text" id="USER_EMAIL" name="USER_EMAIL" maxlength="50" maxlength="255" class="text" />

                <?
                /* CAPTCHA */
                if ($arResult["USE_CAPTCHA"] == "Y")
                {
                    ?>
                    <div class="clearfix"></div>
                    <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                    <label for="captcha_word"><span class="starrequired">*</span><?=GetMessage("system_auth_captcha")?></label>
                    <img style="margin-bottom: 5px;" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                    <input style="width: auto" type="text" id="captcha_word" name="captcha_word" maxlength="50" value="" class="text" />
                    <?
                }
                /* CAPTCHA */
                ?>

                <div class="clearfix"></div>

                <p>&nbsp;</p>

                <input type="submit" class="button" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" />

                <p>&nbsp;</p>

                <p><a href="<?=$arResult["AUTH_AUTH_URL"]?>"><b><?=GetMessage("AUTH_AUTH")?></b></a></p>

            </div>
        </div>
    </form>
    <script type="text/javascript">
        document.bform.USER_LOGIN.focus();
    </script>
</div>