<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$Burl=$arParams["FOLDER"].$arParams["URL_TEMPLATES"]["detail"];
$Burl=str_replace("#ELEMENT_ID#",'add',$Burl);
?>
<?$this->SetViewTarget('afterh1'); ?>
<div class="filter_wrap">
    <div class="jumbotron gallery_jumbo">
        <div class="row div-1520">
            <h1>Фотогалерея</h1>
        </div>
    </div>
    <div class="filter_btn">
        <a href="<?=$Burl?>" class="flaticon-add btn-add delay-1s a_button_block"></a>
    </div>
</div>
<?$this->EndViewTarget(); ?>
<?if($arResult["add_object"] && $arResult["add_object"]>0){?>
    <div class="photo-container row div-1520 gallery_cont">
        <div class="clearfix"></div>
        <div class="error_message">
            <?ShowMessage(Array("TYPE"=>"OK", "MESSAGE"=>"Ваш объект добавлен."));?>
        </div>
    </div>
<?}?>
<div class="photo-container row div-1520 gallery_cont">
    <?if(count($arResult["photo"])>0){?>
        <div class="object_ctr">
            <?foreach ($arResult["photo"] as $photo){
                $Object=$photo["object"];
                $url=$arParams["FOLDER"].$arParams["URL_TEMPLATES"]["detail"];
                $url=str_replace("#ELEMENT_ID#",$Object->getId(),$url);
                $bricks=$Object->getBrick();
                $arrName=[];
                $arrName=[];
                $arrBrand=[];
                foreach($bricks as $brick){
                    $arrName[]=$brick->getName().' ('.$brick->getVendor().')';
                    $arrSimpleName[]=$brick->getName();
                }
                foreach($bricks as $brick){
                    $arrBrand[]=$brick->getBrand()->getName();
                }
                ?>
                <div class="col-sm-6 col-md-3 ">
                    <div class="obj-photo-wrap">
                        <?if(count($photo["photo"])>0){
                            $PhotoId=$photo["photo"][0]->getPhotoId();
                            if($PhotoId>0){
                                $arFileTmp = CFile::ResizeImageGet(
                                    $PhotoId,
                                    array("width" => 250, "height" => 250),
                                    BX_RESIZE_IMAGE_EXACT,
                                    true
                                );
                                $arFileTmp2 = CFile::ResizeImageGet(
                                    $PhotoId,
                                    array("width" => 1000, "height" => 1000),
                                    BX_RESIZE_IMAGE_EXACT,
                                    true
                                );
                                ?><div class="image-wrap"><a data-fancybox="gallery" href="<?=$arFileTmp2["src"]?>"><img src="<?=$arFileTmp["src"]?>"><div class="overlay flaticon-zoom-in"></div></a></div><?
                            }
                        }?>
                        <a class="obj-edit flaticon-pencil" href="<?=$url?>">Редактировать</a>
                        <div class="description-poles">
                            <span>Название: <b><?=implode(', ',array_unique($arrSimpleName))?></b></span>
                            <span>Бренд: <b><?=implode(', ',array_unique($arrBrand))?></b></span>
                            <span>Регион: <b><?=$Object->getAddress()->getRegion()?></b></span>
                        </div>
                    </div>
                </div>
            <?}?>
        </div>
        <?
        $APPLICATION->IncludeComponent(
            "bitrix:main.pagenavigation",
            "catalog",
            array(
                "NAV_OBJECT" => $arResult["nav"],
                "SEF_MODE" => "Y",
            ),
            $component
        );
        ?>
    <?}else{?>
        <div class="clearfix"></div>
        <div class="error_message">
            В данный момент нет ни одного размещённого объекта.<br>
            Чтобы добавить объект, нажмите кнопку <a href="<?=$Burl?>">«Добавить объект»</a> выше.
        </div>
    <?}?>
</div>