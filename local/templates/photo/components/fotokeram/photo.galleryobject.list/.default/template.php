<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?use local\Helpers\Watermark;?>
<?//echo "<pre>";print_r($arResult);echo "</pre>"?>
<div class="photo-container row div-1520">
    <?foreach ($arResult["photo"] as $wrPhoto){
        $Object=$wrPhoto['object'];
        if(count($wrPhoto['photo'])<=0)
            continue;
        $photo=$wrPhoto['photo'][0];
        $url=$arParams["FOLDER"].$arParams["URL_TEMPLATES"]["detail"];
        $url=str_replace("#ELEMENT_ID#",$Object->getId(),$url);
        $UserId=$Object->getAuthor();
        $Watermark=Watermark::GetWatermark();
        ?>
        <?if($photo->getPhotoId()>0){
            $arFileTmp = CFile::ResizeImageGet(
                $photo->getPhotoId(),
                array("width" => 250, "height" => 250),
                BX_RESIZE_IMAGE_EXACT,
                true,$Watermark
            );

            $arFileTmp2 = CFile::ResizeImageGet(
                $photo->getPhotoId(),
                array("width" => 1000, "height" => 1000),
                BX_RESIZE_IMAGE_EXACT,
                true,$Watermark
            );
        }?>
        <?
        $bricks=$Object->getBrick();
        $arrName=[];
        $arrSimpleName=[];
        $arrBrand=[];
        foreach($bricks as $brick){
            $arrName[]=$brick->getName().' ('.$brick->getVendor().')';
            $arrSimpleName[]=$brick->getName();
        }
        foreach($bricks as $brick){
            $arrBrand[]=$brick->getBrand()->getName();
        }
        ?>
        <div class="col-md-5th images">
            <div class="obj-photo-wrap">
                <?if($photo->getPhotoId()>0){?>
                    <div class="image-wrap">
                        <a
                                class="fancybox"
                                data-photocart="<?=$photo->getId();?>"
                                data-url="<?=$url?>"
                                data-title-id="title-<?=$photo->getPhotoId()?>"
                                data-use-user-group="<?=$arParams["USE_USER_GROUP"]?>"
                                data-fancybox="gallery<?=$Object->getId()?>"
                                href="<?=$arFileTmp2["src"]?>"><img src="<?=$arFileTmp["src"]?>">
                            <div class="overlay flaticon-zoom-in"></div>
                        </a>
                        <div id="title-<?=$photo->getPhotoId()?>" class="hidden">
                            <?=implode(', ',$arrName)?>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?if(count($wrPhoto['photo'])>1){?>
                        <?for($i=1;$i<count($wrPhoto['photo']);$i++){?>
<?

                            $arFileTmp = CFile::ResizeImageGet(
                                $wrPhoto['photo'][$i]->getPhotoId(),
                                array("width" => 250, "height" => 250),
                                BX_RESIZE_IMAGE_EXACT,
                                true,$Watermark
                            );

                            $arFileTmp2 = CFile::ResizeImageGet(
                                $wrPhoto['photo'][$i]->getPhotoId(),
                                array("width" => 1000, "height" => 1000),
                                BX_RESIZE_IMAGE_EXACT,
                                true,$Watermark
                            );
                            ?>

                            <a
                                    class="fancybox"
                                    style="display:none;"
                                    data-photocart="<?=$wrPhoto['photo'][$i]->getId();?>"
                                    data-url="<?=$url?>"
                                    data-title-id="title-<?=$wrPhoto['photo'][$i]->getPhotoId()?>"
                                    data-use-user-group="<?=$arParams["USE_USER_GROUP"]?>"
                                    data-fancybox="gallery<?=$Object->getId()?>"
                                    href="<?=$arFileTmp2["src"]?>">

                                <label class="container-check">Выбрать
                                    <input type="checkbox"
                                           data-photocart="<?=$wrPhoto['photo'][$i]->getId();?>" value="<?=$wrPhoto['photo'][$i]->getId();?>" name="PhotoCart[]"
                                           <?if(in_array($wrPhoto['photo'][$i]->getId(),$arParams["PHOTOCART"])){?>checked<?}?>
                                    />
                                    <span class="checkmark"></span>
                                </label>

                            </a>
                            <div id="title-<?=$wrPhoto['photo'][$i]->getPhotoId()?>" class="hidden">
                                <?=implode(', ',array_unique($arrName))?>
                                <div class="clearfix"></div>
                            </div>
                        <?}?>
                    <?}?>
                <?}?>
                <?if($arParams["USE_USER_GROUP"]){?><label class="container-check">Выбрать
                    <input type="checkbox"
                           data-photocart="<?=$photo->getId();?>" value="<?=$photo->getId();?>" name="PhotoCart[]"
                           <?if(in_array($photo->getId(),$arParams["PHOTOCART"])){?>checked<?}?>
                    />
                    <span class="checkmark"></span>
                </label><?}?>
                <div class="description-poles">
                    <span>Название: <b><?=implode(', ',array_unique($arrSimpleName))?></b></span>
                    <span>Бренд: <b><?=implode(', ',array_unique($arrBrand))?></b></span>
                    <span>Регион: <b><?=$Object->getAddress()->getRegion()?></b></span>
                </div>
            </div>
        </div>
    <?}?>
    <?
    $APPLICATION->IncludeComponent(
        "bitrix:main.pagenavigation",
        "catalog",
        array(
            "NAV_OBJECT" => $arResult["nav"],
            //"SEF_MODE" => "Y",
        ),
        $component
    );
    ?>
</div>