<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?use Bitrix\Main\Config\Option;?>
<?use local\Helpers\Watermark;?>


<?
$yandexMapApiKey = Option::get('fileman', 'yandex_map_api_key', '');
if (strlen($yandexMapApiKey) >= 0) {
    $coords1 = 55.76;
    $coords2 = 37.64;

    ?>
<br>
    <div id="map" style="width: 100%; height: 1400px"></div>

<?
$сoords=[];
foreach ($arResult["photo"] as $wrPhoto){
    $Object=$wrPhoto['object'];
    if(count($wrPhoto['photo'])<=0)
        continue;
    $photo=$wrPhoto['photo'][0];
    $url=$arParams["FOLDER"].$arParams["URL_TEMPLATES"]["detail"];
    $url=str_replace("#ELEMENT_ID#",$Object->getId(),$url);
    $UserId=$Object->getAuthor();
    $Watermark=Watermark::GetWatermark();
    if($photo->getPhotoId()>0){
        $arFileTmp = CFile::ResizeImageGet(
            $photo->getPhotoId(),
            array("width" => 250, "height" => 250),
            BX_RESIZE_IMAGE_EXACT,
            true,$Watermark
        );

        $arFileTmp2 = CFile::ResizeImageGet(
            $photo->getPhotoId(),
            array("width" => 1000, "height" => 1000),
            BX_RESIZE_IMAGE_EXACT,
            true,$Watermark
        );
    }
    $bricks=$Object->getBrick();
    $arrName=[];
    $arrSimpleName=[];
    $arrBrand=[];
    foreach($bricks as $brick){
        $arrName[]=$brick->getName().' ('.$brick->getVendor().')';
        $arrSimpleName[]=$brick->getName();
    }
    foreach($bricks as $brick){
        $arrBrand[]=$brick->getBrand()->getName();
    }
    ?>

                <?if($photo->getPhotoId()>0){?>
                    <?if(count($wrPhoto['photo'])>1){?>
                        <?for($i=1;$i<count($wrPhoto['photo']);$i++){?>
<?

                            $arFileTmp = CFile::ResizeImageGet(
                                $wrPhoto['photo'][$i]->getPhotoId(),
                                array("width" => 250, "height" => 250),
                                BX_RESIZE_IMAGE_EXACT,
                                true,$Watermark
                            );

                            $arFileTmp2 = CFile::ResizeImageGet(
                                $wrPhoto['photo'][$i]->getPhotoId(),
                                array("width" => 1000, "height" => 1000),
                                BX_RESIZE_IMAGE_EXACT,
                                true,$Watermark
                            );
                            ?>

                            <a
                                    class="fancybox"
                                    style="display:none;"
                                    data-photocart="<?=$wrPhoto['photo'][$i]->getId();?>"
                                    data-url="<?=$url?>"
                                    data-title-id="title-<?=$wrPhoto['photo'][$i]->getPhotoId()?>"
                                    data-use-user-group="<?=$arParams["USE_USER_GROUP"]?>"
                                    data-fancybox="gallery<?=$Object->getId()?>"
                                    href="<?=$arFileTmp2["src"]?>">

                                <label class="container-check">Выбрать
                                    <input type="checkbox"
                                           data-photocart="<?=$wrPhoto['photo'][$i]->getId();?>" value="<?=$wrPhoto['photo'][$i]->getId();?>" name="PhotoCart[]"
                                           <?if(in_array($wrPhoto['photo'][$i]->getId(),$arParams["PHOTOCART"])){?>checked<?}?>
                                    />
                                    <span class="checkmark"></span>
                                </label>

                            </a>
                            <div id="title-<?=$wrPhoto['photo'][$i]->getPhotoId()?>" class="hidden">
                                <?=implode(', ',array_unique($arrName))?>
                                <div class="clearfix"></div>
                            </div>
                        <?}?>
                    <?}?>
                <?}?>
  <?

    $сoords[]=[
        "сoords"=>[$Object->getAddress()->getCoords1(),$Object->getAddress()->getCoords2()],
        "name"=>implode(', ',array_unique($arrSimpleName)).' '.implode(', ',array_unique($arrBrand)),
        "inner" =>
            "
                    <div class=\"image-wrap\">
                        <a
                                class=\"fancybox\"
                                data-photocart=\"".$photo->getId()."\"
    data-url=\"".$url."\"
    data-title-id=\"title-".$photo->getPhotoId()."\"
    data-use-user-group=\"".$arParams["USE_USER_GROUP"]."\"
    data-fancybox=\"gallery".$Object->getId()."\"
    href=\"".$arFileTmp2["src"]."\"><img src=\"".$arFileTmp["src"]."\">
    <div class=\"overlay flaticon-zoom-in\"></div>
    </a>
    <div id=\"title-".$photo->getPhotoId()."\" class=\"hidden\">
        ".implode(', ',$arrName)."
        <div class=\"clearfix\"></div>
    </div>
    </div>
<div class=\"description-poles\">
<span>Название: <b>".implode(', ',array_unique($arrSimpleName))."</b></span>
            <span>Бренд: <b>".implode(', ',array_unique($arrBrand))."</b></span>
            <span>Регион: <b>".$Object->getAddress()->getRegion()."</b></span>
            </div>
            ",
    ];

}

    ?>

    <script type="text/javascript">
        ymaps.ready(init);
        var map;
        var markers=[];

        function addMarker(map,сoords) {

            var marker = new ymaps.Placemark(сoords.сoords, {
                balloonContentHeader: сoords.name ,
                balloonContentBody: сoords.inner
            }, {
            });
            markers.push(marker);
        }


        function init() {
            map = new ymaps.Map("map", {
                center: [<?=$coords1?>, <?=$coords2?>],
                controls: [],
                zoom: 12
            });
            var сoords = <?=CUtil::PhpToJSObject($сoords)?>;
            for (var i = 0, l = сoords.length; i < l; i++) {
                addMarker(map,сoords[i]);
            }
            markerCluster = new ymaps.Clusterer({})
            markerCluster.add(markers);
            map.geoObjects.add(markerCluster);
            map.setBounds(markerCluster.getBounds(), {
                checkZoomRange: true
            });
        }
    </script>

    <?}?>




