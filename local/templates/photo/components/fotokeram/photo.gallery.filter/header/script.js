jQuery(document).ready(function() {



    function prepareData(data) {
        //console.log(data);
        var prav=[
            {text:'Бренд',name:'Brand',id:'Brand',idfilter:'Brand'},
            {text:'Название (артикул)',name:'BrickFormat',id:'BrickFormatVendor',idfilter:'BrickFormatVendor'},
            {text:'Формат',name:'BrickFormatFormat',id:'BrickFormatFormat',idfilter:'BrickFormatFormat'},
            {text:'Цвет кирпича',name:'BrickFormatСolor',id:'BrickFormatСolor',idfilter:'BrickFormatСolor'},
            {text:'Цвет шва',name:'SeamColor',id:'SeamColor',idfilter:'SeamColor'},
            {text:'Год',name:'Year',id:'Year',idfilter:'Year'},
            {text:'Регион',name:'Region',id:'Region',idfilter:'RegionId'},
            {text:'Район',name:'Area',id:'Area',idfilter:'AreaId'},
            {text:'Город',name:'City',id:'City',idfilter:'CityId'},
            {text:'Микс',name:'Mix',id:'Mix',idfilter:'Mix'},
            {text:'Тип объекта',name:'objecttype',id:'objecttype',idfilter:'objecttype'}
        ];
        jQuery('[data-filter-form]').each(function (i) {

            var form=jQuery(this);

            for (var i=0;i<prav.length;i++)
            {
                var
                    id=prav[i].id,
                    name=prav[i].name,
                    text=prav[i].text,
                    idfilter=prav[i].idfilter,
                    value=data[name],
                    el=form.find('[data-id='+id+']')
                ;
                el.empty();
                el.append($('<option value="" data-display="'+text+'">'+text+'</option>'));

                if(value.length==0)
                    el.attr('disabled', 'disabled');
                else
                    el.removeAttr('disabled');

                el.removeClass('active');

                switch (name) {
                    case "BrickFormat":
                        for (var k=0;k<value.length;k++) {
                            var
                                optionName = value[k].name + ' (' + value[k].vendor + ')',
                                optionId = value[k].id,
                                option = $('<option>' + optionName + '</option>')
                            ;
                            option.attr('value', optionId);
                            if (typeof (data.filter[idfilter])!="undefined" && data.filter[idfilter][0] == optionId){
                                option.attr('selected', 'selected');
                                el.addClass('active');
                            }
                            el.append(option);
                        }
                        break;
                    case "Region":
                    case "Area":
                    case "City":
                        $.each(value, function(index, value){
                            var
                                optionName = value,
                                optionId = index,
                                option = $('<option>' + optionName + '</option>')
                            ;
                            option.attr('value', optionId);
                            if (typeof (data.filter[idfilter])!="undefined" && data.filter[idfilter][0] == optionId){
                                option.attr('selected', 'selected');
                                el.addClass('active');
                            }
                            el.append(option);
                        });
                        break;
                    case "Year":
                        for (var k=0;k<value.length;k++) {
                            var
                                optionName = value[k].value,
                                optionId = value[k].id,
                                option = $('<option>' + optionName + '</option>')
                            ;
                            option.attr('value', optionId);
                            if (typeof (data.filter[idfilter])!="undefined" && data.filter[idfilter][0] == optionId){
                                option.attr('selected', 'selected');
                                el.addClass('active');
                            }
                            el.append(option);
                        }
                        break;
                    case "Mix":
                        for (var k=0;k<value.length;k++) {
                            var optionId = value[k], optionName = "", option;
                            if (optionId == "Y")
                                optionName = "Да";
                            else if (optionId == "N")
                                optionName = "Нет";
                            if(optionName.length>0){
                                option = $('<option>' + optionName + '</option>');
                                option.attr('value', optionId);
                                if (typeof (data.filter[idfilter])!="undefined" && data.filter[idfilter] == optionId){
                                    option.attr('selected', 'selected');
                                    el.addClass('active');
                                }
                                el.append(option);
                            }
                        }
                        break;
                    default:
                        for (var k=0;k<value.length;k++) {
                            var
                                optionName = value[k].name,
                                optionId = value[k].id,
                                option = $('<option>' + optionName + '</option>')
                            ;
                            option.attr('value', optionId);
                            if (typeof (data.filter[idfilter])!="undefined" && data.filter[idfilter][0] == optionId)
                            {
                                option.attr('selected', 'selected');
                                el.addClass('active');
                            }
                            el.append(option);
                        }
                        break;
                }

                //el.trigger('refresh');
                el.niceSelect('update');
            }

        });

    }

    jQuery('[data-filter-form]').each(function (i) {
        var form=jQuery(this),
            url=form.data('filterAjaxUrl')
        ;
        form.find('select').on('change',function (e) {
            var data=form.serializeArray();

            $.ajax({
                url: url,
                dataType: 'json',
                type: 'GET',
                data: data,
                success: function (data) {
                    prepareData(data);
                }
            });
        });
    });

});