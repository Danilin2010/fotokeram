<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<?
$class="col-md-5th";?>

<form data-filter-form data-filter-ajax-url="<?=$componentPath?>/ajax.php" class="" action="" method="get">
    <div class="filter_wrap">
        <div class="jumbotron">
            <div class="row div-1520">
                <div class="<?=$class?>">
                    <select class="niceselect <?if(count($arResult["filter"]["Brand"])>0 && $arResult["filter"]["Brand"][0]!==""){?>active<?}?>" data-id="Brand" name="filter[Brand][]" <?if(count($arResult["Brand"])==0){?>disabled<?}?>>
                        <option value="" data-display="Бренд">Бренд</option>
                        <?foreach ($arResult["Brand"] as $brand){?>
                            <option
                                <?if(in_array($brand->getId(),$arResult["filter"]["Brand"])){?>selected<?}?>
                                value="<?=$brand->getId();?>"><?=$brand->getName();?></option>
                        <?}?>
                    </select>
                </div>
                <div class="<?=$class?>">
                    <select class="niceselect <?if(count($arResult["filter"]["BrickFormatVendor"])>0 && $arResult["filter"]["BrickFormatVendor"][0]!==""){?>active<?}?>" data-id="BrickFormatVendor" name="filter[BrickFormatVendor][]" <?if(count($arResult["BrickFormat"])==0){?>disabled<?}?>>
                        <option value="" data-display="Название">Название</option>
                        <?foreach ($arResult["BrickFormat"] as $BrickFormat){?>
                            <option
                                <?if(in_array($BrickFormat->getId(),$arResult["filter"]["BrickFormatVendor"])){?>selected<?}?>
                                value="<?=$BrickFormat->getId();?>"><?=$BrickFormat->getName();?></option>
                        <?}?>
                    </select>
                </div>
                <div class="<?=$class?>">
                    <select class="niceselect <?if(count($arResult["filter"]["BrickFormatFormat"])>0 && $arResult["filter"]["BrickFormatFormat"][0]!==""){?>active<?}?>" data-id="BrickFormatFormat" name="filter[BrickFormatFormat][]" <?if(count($arResult["BrickFormatFormat"])==0){?>disabled<?}?>>
                        <option value="" data-display="Формат">Формат</option>
                        <?foreach ($arResult["BrickFormatFormat"] as $BrickFormat){?>
                            <option
                                <?if(in_array($BrickFormat->getId(),$arResult["filter"]["BrickFormatFormat"])){?>selected<?}?>
                                value="<?=$BrickFormat->getId();?>"><?=$BrickFormat->getName();?></option>
                        <?}?>
                    </select>
                </div>
                <div class="<?=$class?>">
                    <select class="niceselect <?if(count($arResult["filter"]["BrickFormatСolor"])>0 && $arResult["filter"]["BrickFormatСolor"][0]!==""){?>active<?}?>" data-id="BrickFormatСolor" name="filter[BrickFormatСolor][]" <?if(count($arResult["BrickFormatСolor"])==0){?>disabled<?}?>>
                        <option value="" data-display="Цвет кирпича">Цвет кирпича</option>
                        <?foreach ($arResult["BrickFormatСolor"] as $Color){?>
                            <option
                                <?if(in_array($Color->getId(),$arResult["filter"]["BrickFormatСolor"])){?>selected<?}?>
                                value="<?=$Color->getId();?>"><?=$Color->getName();?></option>
                        <?}?>
                    </select>
                </div>
                <div class="<?=$class?>">
                    <select class="niceselect <?if(count($arResult["filter"]["SeamColor"])>0 && $arResult["filter"]["SeamColor"][0]!==""){?>active<?}?>" data-id="SeamColor" name="filter[SeamColor][]" <?if(count($arResult["SeamColor"])==0){?>disabled<?}?>>
                        <option value="" data-display="Цвет шва">Цвет шва</option>
                        <?foreach ($arResult["SeamColor"] as $SeamColor){?>
                            <option
                                <?if(in_array($SeamColor->getId(),$arResult["filter"]["SeamColor"])){?>selected<?}?>
                                value="<?=$SeamColor->getId();?>"><?=$SeamColor->getName();?></option>
                        <?}?>
                    </select>
                </div>
                <div class="<?=$class?>">
                    <select class="niceselect <?if(count($arResult["filter"]["Year"])>0 && $arResult["filter"]["Year"][0]!==""){?>active<?}?>" data-id="Year" name="filter[Year][]" <?if(count($arResult["Year"])==0){?>disabled<?}?>>
                        <option value="" data-display="Год">Год</option>
                        <?foreach ($arResult["Year"] as $Year){?>
                            <option
                                <?if(in_array($Year->getId(),$arResult["filter"]["Year"])){?>selected<?}?>
                                value="<?=$Year->getId();?>"><?=$Year->getValue();?></option>
                        <?}?>
                    </select>
                </div>
                <div class="<?=$class?>">
                    <select class="niceselect <?if(count($arResult["filter"]["RegionId"])>0 && $arResult["filter"]["RegionId"][0]!==""){?>active<?}?>" data-id="Region" name="filter[RegionId][]" <?if(count($arResult["Region"])==0){?>disabled<?}?>>
                        <option value="" data-display="Регион">Регион</option>
                        <?foreach ($arResult["Region"] as $key=>$Region){?>
                            <option
                                <?if(in_array($key,$arResult["filter"]["RegionId"])){?>selected<?}?>
                                value="<?=$key?>"><?=$Region?></option>
                        <?}?>
                    </select>
                </div>
                <div class="<?=$class?>">
                    <select class="niceselect <?if(count($arResult["filter"]["AreaId"])>0 && $arResult["filter"]["AreaId"][0]!==""){?>active<?}?>" data-id="Area" name="filter[AreaId][]" <?if(count($arResult["Area"])==0){?>disabled<?}?>>
                        <option value="" data-display="Район">Район</option>
                        <?foreach ($arResult["Area"] as $key=>$Region){?>
                            <option
                                <?if(in_array($key,$arResult["filter"]["AreaId"])){?>selected<?}?>
                                value="<?=$key?>"><?=$Region?></option>
                        <?}?>
                    </select>
                </div>
                <div class="<?=$class?>">
                    <select class="niceselect <?if(count($arResult["filter"]["CityId"])>0 && $arResult["filter"]["CityId"][0]!==""){?>active<?}?>" data-id="City" name="filter[CityId][]" <?if(count($arResult["City"])==0){?>disabled<?}?>>
                        <option value="" data-display="Город">Город</option>
                        <?foreach ($arResult["City"] as $key=>$Region){?>
                            <option
                                <?if(in_array($key,$arResult["filter"]["CityId"])){?>selected<?}?>
                                value="<?=$key?>"><?=$Region?></option>
                        <?}?>
                    </select>
                </div>
                <div class="<?=$class?>">
                    <select class="niceselect <?if($arResult["filter"]["Mix"]){?>active<?}?>" data-id="Mix" name="filter[Mix]" <?if(count($arResult["Mix"])==0){?>disabled<?}?>>
                        <option value="" data-display="Микс">Микс</option>
                        <?foreach ($arResult["Mix"] as $Mix){?>
                            <option
                                <?if($Mix==$arResult["filter"]["Mix"]){?>selected<?}?>
                                value="<?=$Mix;?>"><?=Loc::getMessage("TEMPLATE_FILTER_MIX_".$Mix)?></option>
                        <?}?>
                    </select>
                </div>

                <div class="<?=$class?>">
                    <select class="niceselect <?if(count($arResult["filter"]["objecttype"])>0 && $arResult["filter"]["objecttype"][0]!==""){?>active<?}?>" data-id="objecttype" name="filter[objecttype][]" <?if(count($arResult["objecttype"])==0){?>disabled<?}?>>
                        <option value="" data-display="Тип объекта">Тип объекта</option>
                        <?foreach ($arResult["objecttype"] as $object){?>
                            <option
                                <?if(in_array($object->getId(),$arResult["filter"]["objecttype"])){?>selected<?}?>
                                value="<?=$object->getId()?>"><?=$object->getName()?></option>
                        <?}?>
                    </select>
                </div>

            </div>
        </div>
        <div class="filter_btn">
            <button class="flaticon-refresh-button" name="setFilter" type="submit"><span>Применить</span></button>
            <input type="hidden" name="setFilter" value="Y" />
            <?if($arResult["setFilter"]=="Y" && count($arResult["filter"])>0){?>
                <a class="flaticon-delete button" href="<?=$arResult["delFilter"]?>"><span>Сбросить</span></a>
            <?}?>
        </div>
    </div>
</form>