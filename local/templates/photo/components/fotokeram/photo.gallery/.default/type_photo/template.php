<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$APPLICATION->SetAdditionalCSS($this->GetFolder().'/type_photo/style.css');
include($_SERVER["DOCUMENT_ROOT"].$this->GetFolder()."/type_photo/function.php");
?>
<div class="sort row" style="position: relative;">
    <div class="sort-wrapper">
        <span>Отображать по -</span>
        <?foreach ($arResult["type_photo"] as $type){?>
        <a class="button_object <?=$type["code"]?> <?if($type["code"]==$arResult["type"]){?>active<?}?>" href="<?=$type["url"]?>"><?=$type["name"]?></a>
        <?}?>
    </div>
    <?if($arResult["type"]!=="bymap"){?>
    <?include($_SERVER["DOCUMENT_ROOT"].$this->GetFolder()."/sort_string/template.php");?>
    <?}?>
</div>