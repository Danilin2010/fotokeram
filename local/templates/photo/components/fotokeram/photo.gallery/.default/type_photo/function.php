<?
global $APPLICATION;
global $USER;

$arResult["ClearUrl"]=array("settype");
//**init arr sort**//
$arResult["type_photo"]=array();

$arResult["type_photo"]['byobject']=array(
    "code"=>"byobject",
    "name"=>'Объектам',
    "url"=>$APPLICATION->GetCurPageParam("settype=byobject",$arResult["ClearUrl"]),
);

$arResult["type_photo"]['byphoto']=array(
    "code"=>"byphoto",
    "name"=>'Фотографиям',
    "url"=>$APPLICATION->GetCurPageParam("settype=byphoto",$arResult["ClearUrl"]),
);

if ($USER->IsAdmin() || $USER->getID()==12){
    $arResult["type_photo"]['bymap']=array(
        "code"=>"bymap",
        "name"=>'Карта',
        "url"=>$APPLICATION->GetCurPageParam("settype=bymap",$arResult["ClearUrl"]),
    );
}

if(isset($_REQUEST["settype"]))
{
    $setsort=trim($_REQUEST["settype"]);
    if($arResult["type_photo"][$setsort])
        $APPLICATION->set_cookie("CATALOG_TYPE",$setsort);
    LocalRedirect($APPLICATION->GetCurPageParam("",$arResult["ClearUrl"]));
}

//**init get sort**//
$arResult["type"]=$APPLICATION->get_cookie("CATALOG_TYPE");
if(!$arResult["type_photo"][$arResult["type"]])
    $arResult["type"]="byphoto";