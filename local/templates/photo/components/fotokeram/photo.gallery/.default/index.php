<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?use \local\Services\PhotoCart;?>
<?$this->SetViewTarget('headerfilter'); ?>
<?$APPLICATION->IncludeComponent(
    "fotokeram:photo.gallery.filter",
    "header",
    Array(

    )
);?>
<?$this->EndViewTarget(); ?>
<?$this->SetViewTarget('headerfilterbutton'); ?>
<a class="navbar-brand" href="/" role="banner">
    <img src="<?=SITE_TEMPLATE_PATH?>/img/logo.svg">
</a>
<button class="showfilter">показать фильтр</button>
<?$this->EndViewTarget(); ?>
<?$type=$APPLICATION->get_cookie("CATALOG_TYPE");?>
<?$APPLICATION->IncludeComponent(
    "fotokeram:photo.gallery.filter",
    "header",
    Array(
        "USE_MAP"=>$type=="bymap"?"Y":"N",
    )
);?>
<?include($_SERVER["DOCUMENT_ROOT"].$this->GetFolder()."/type_photo/template.php");?>
<?
switch ($arResult["type"]){
    case "byphoto":
?>
        <?$APPLICATION->IncludeComponent(
        "fotokeram:photo.gallery.list",
        "",
        Array(
            "USE_USER_GROUP"=>USE_USER_GROUP,
            "PHOTOCART"=>PhotoCart::getPhoto(),
            "FOLDER" => $arResult["FOLDER"],
            "URL_TEMPLATES" => $arResult["URL_TEMPLATES"],
            "ELEMENT_SORT_FIELD" => $arResult["ElementSortField"],
            "ELEMENT_SORT_ORDER" => $arResult["ElementSortOrder"],
        )
    );?>
        <?
        break;
    case "byobject":
        ?>
        <?$APPLICATION->IncludeComponent(
        "fotokeram:photo.galleryobject.list",
        "",
        Array(
            "USE_USER_GROUP"=>USE_USER_GROUP,
            "PHOTOCART"=>PhotoCart::getPhoto(),
            "FOLDER" => $arResult["FOLDER"],
            "URL_TEMPLATES" => $arResult["URL_TEMPLATES"],
            "ELEMENT_SORT_FIELD" => $arResult["ElementSortField"],
            "ELEMENT_SORT_ORDER" => $arResult["ElementSortOrder"],
        )
    );?>
        <?
        break;
    case "bymap":
        ?>
        <?$APPLICATION->IncludeComponent(
        "fotokeram:photo.galleryobject.list",
        "bymap",
        Array(
            "USE_MAP"=>"Y",
            "USE_USER_GROUP"=>USE_USER_GROUP,
            "PHOTOCART"=>PhotoCart::getPhoto(),
            "FOLDER" => $arResult["FOLDER"],
            "URL_TEMPLATES" => $arResult["URL_TEMPLATES"],
            "ELEMENT_SORT_FIELD" => $arResult["ElementSortField"],
            "ELEMENT_SORT_ORDER" => $arResult["ElementSortOrder"],
        )
    );?>
        <?
        break;
}
?>


