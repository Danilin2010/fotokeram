<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?use \local\Services\PhotoCart;?>
<?
global $FotokeramPhotoGalleryFilter;
$FotokeramPhotoGalleryFilter=[
    "ObjectId"=>$arResult["VARIABLES"]["ELEMENT_ID"]
];
?>
<?
$APPLICATION->SetAdditionalCSS($this->GetFolder().'/type_photo/style.css');
?>
<div class="sort row" style="position: relative;">
    <?include($_SERVER["DOCUMENT_ROOT"].$this->GetFolder()."/sort_string/template.php");?>
</div>
<br/>
<?$APPLICATION->IncludeComponent(
    "fotokeram:photo.gallery.list",
    "",
    Array(
        "USE_USER_GROUP"=>USE_USER_GROUP,
        "PHOTOCART"=>PhotoCart::getPhoto(),
        "PAGE_SIZE"=>500,
        "FOLDER" => $arResult["FOLDER"],
        "URL_TEMPLATES" => $arResult["URL_TEMPLATES"],
        "ELEMENT_SORT_FIELD" => $arResult["ElementSortField"],
        "ELEMENT_SORT_ORDER" => $arResult["ElementSortOrder"],
    )
);?>

