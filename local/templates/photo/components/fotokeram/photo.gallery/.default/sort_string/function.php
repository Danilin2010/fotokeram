<?
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;
global $APPLICATION;
$arResult["ClearUrl"]=array("setsort");
//**init arr sort**//
$arResult["arrSort"]=array();

$arResult["arrSort"]["SORT"]=array(
    "ELEMENT_SORT_FIELD"=>"DATE_CREATE",
    "ELEMENT_SORT_ORDER"=>"DESC",
    "NAME"=>'Дате',
);

$arResult["arrSort"]["SHOWS"]=array(
    "ELEMENT_SORT_FIELD"=>"object_brickformat_name_sort",
    "ELEMENT_SORT_ORDER"=>"ASC",
    "NAME"=>'Алфавиту',
);

$arResult["arrSort"]["NEW"]=array(
    "ELEMENT_SORT_FIELD"=>"view_count",
    "ELEMENT_SORT_ORDER"=>"DESC",
    "NAME"=>'Популярности',
);


//**init set sort**//
if(isset($_REQUEST["setsort"]))
{
    $setsort=trim($_REQUEST["setsort"]);
    if($arResult["arrSort"][$setsort]){
        $cookie = new Cookie("CATALOG_SORT", $setsort);
        Application::getInstance()->getContext()->getResponse()->addCookie($cookie);
    }
    LocalRedirect($APPLICATION->GetCurPageParam("",$arResult["ClearUrl"]));
}


//**init get sort**//
$arResult["Sort"]=$APPLICATION->get_cookie("CATALOG_SORT");
if(!$arResult["arrSort"][$arResult["Sort"]])
    $arResult["Sort"]=key($arResult["arrSort"]);
$arResult["ElementSortField"]=$arResult["arrSort"][$arResult["Sort"]]["ELEMENT_SORT_FIELD"];
$arResult["ElementSortOrder"]=$arResult["arrSort"][$arResult["Sort"]]["ELEMENT_SORT_ORDER"];

?>