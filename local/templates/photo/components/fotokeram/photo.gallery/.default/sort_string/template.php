<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$APPLICATION->AddHeadScript($this->GetFolder().'/sort_string/script.js');
include($_SERVER["DOCUMENT_ROOT"].$this->GetFolder()."/sort_string/function.php");
?>

<div class="sorting">
    <form method="get" >
        Сортировать по:
        <select name="setsort" class="niceselect" id="setsort">
            <?foreach($arResult["arrSort"] as $key=>$value){?>
                <option value="<?=$key?>" <?if($key==$arResult["Sort"]){?> selected="selected" <?}?>><?=$value["NAME"]?></option>
            <?}?>
        </select>
    </form>
</div>

<style>
    .sorting{
        position: absolute;
        width: 280px;
        right: 0px;
        top: -9px;
    }
    .sorting .nice-select{
        float: initial;
        display: inline-block;
        width: 150px;
        vertical-align: middle;
    }
    @media (min-width: 810px) and (max-width: 1180px) {
        .sorting{
            position: relative;
            right: 0px;
            top: 0px;
        }
    }
    @media (max-width: 810px) {
        .sorting {
            position: relative;
            right: 0px;
            top: 0px;
            margin: 25px auto 0px 20px;
            display: block;
            width: auto;
        }
    }
</style>