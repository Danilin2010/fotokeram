jQuery(document).ready(function() {
    jQuery('[data-sendemail]').on('click',function (e){
        var
            url='/ajax/SendPhotoCart.php'
        ;
        $('.loader').addClass('show');
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: {},
            success: function (data) {
                $('.loader').removeClass('show');
                if(!data.success)
                    alert('Превышен объем попыток за день');
                jQuery('.photocartnum').text(data.count);
                if(data.count<=0)
                {
                    jQuery('.photocartnumvisible').hide();
                    jQuery('.photocartnumhidden').show();
                }else{
                    jQuery('.photocartnumvisible').show();
                    jQuery('.photocartnumhidden').hide();
                }
            }
        });
    });
});