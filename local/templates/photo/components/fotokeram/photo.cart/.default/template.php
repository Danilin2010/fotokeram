<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<div class="photo-checked active photocartnumvisible">
    <a class="basket" href="/basket/"><span class="flaticon-shopping-cart">Выбрано фотографий: <i class="photocartnum"><?=count($arResult["PhotoCart"])?></i></span></a>
    <a class="sendphotos button" href="/basket/">Отправить фотографии →</a>
</div>
<div class="photo-checked active photocartnumhidden">
    <span class="flaticon-shopping-cart">Фотографии отправлены</span>
</div>

