<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?use local\Helpers\Watermark;?>
<?if(count($arResult["photo"])>0){?>
    <div class="photo-container row div-1520 photocartnumvisible">
        <p>
            Проверьте ещё раз необходимые вам фотографии.
            При необходимости вы можете
            удалить не нужные для отправки
        </p>
    </div>
    <div data-basket class="photo-container row div-1520 photocartnumvisible">
        <?foreach ($arResult["photo"] as $photo){
            $Object=$photo->getObject()[0];
            $url=$arParams["FOLDER"].$arParams["URL_TEMPLATES"]["detail"];
            $url=str_replace("#ELEMENT_ID#",$Object->getId(),$url);
            $UserId=$Object->getAuthor();
            $Watermark=Watermark::GetWatermark();
            ?>
            <?if($photo->getPhotoId()>0){
                $arFileTmp = CFile::ResizeImageGet(
                    $photo->getPhotoId(),
                    array("width" => 250, "height" => 250),
                    BX_RESIZE_IMAGE_EXACT,
                    true,$Watermark
                );

                $arFileTmp2 = CFile::ResizeImageGet(
                    $photo->getPhotoId(),
                    array("width" => 1000, "height" => 1000),
                    BX_RESIZE_IMAGE_EXACT,
                    true,$Watermark
                );
            }?>
            <?
            $bricks=$Object->getBrick();
            $arrName=[];
            $arrBrand=[];
            foreach($bricks as $brick){
                $arrName[]=$brick->getName().' ('.$brick->getVendor().')';
            }
            foreach($bricks as $brick){
                $arrBrand[]=$brick->getBrand()->getName();
            }
            ?>
            <div class="col-md-5th images">
                <div class="obj-photo-wrap">
                    <?if($photo->getPhotoId()>0){?>
                        <div class="image-wrap">
                            <a
                                    class="fancybox"
                                    data-photocart="<?=$photo->getId();?>"
                                    data-url="<?=$url?>"
                                    data-title-id="title-<?=$photo->getPhotoId()?>"
                                    data-fancybox="gallery"
                                    href="<?=$arFileTmp2["src"]?>"><img src="<?=$arFileTmp["src"]?>">
                                <div class="overlay flaticon-zoom-in"></div>
                            </a>
                            <div id="title-<?=$photo->getPhotoId()?>" class="hidden">
                                <?=implode(', ',$arrName)?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    <?}?>
                    <label class="container-check">Выбрать
                        <input type="checkbox"
                               data-photocart="<?=$photo->getId();?>" value="<?=$photo->getId();?>" name="PhotoCart[]"
                               <?if(in_array($photo->getId(),$arParams["PHOTOCART"])){?>checked<?}?>
                        />
                        <span class="checkmark"></span>
                    </label>
                    <div class="description-poles">
                        <span>Название/артикул: <b><?=implode(', ',$arrName)?></b></span>
                        <?/*<span>Название объекта: <b><?=implode(', ',$arrName)?></b></span>*/?>
                        <span>Бренд: <b><?=implode(', ',$arrBrand)?></b></span>
                        <span>Регион: <b><?=$Object->getAddress()->getRegion()?></b></span>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
    <div class="sort row photocartnumvisible">
        <div class="sort-wrapper">
            <a class="button_object byobject " data-sendemail href="#" onclick="return false;">Подтверждаю запрос на отправку</a>
            <a class="button_object byobject " href="/photo-gallery/">Вернуться к выбору</a>
        </div>
    </div>
    <div class="photo-container row div-1520 photocartnumhidden">
        Фотографии отправлены
    </div>
<?}else{?>

    <div class="photo-container row div-1520 gallery_cont">
        <div class="error_message">
            В данный момент нет ни одной выбранной фотографии.
        </div>
    </div>

<?}?>



