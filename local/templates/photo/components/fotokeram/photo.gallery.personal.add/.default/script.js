
function AddContentForm( //Функция для авто добавления блоков данных в подсистеме
    PrevBlock, // Селектор блока перед которым надо вставить новый контент
    AddBlock, // Селектор кнопки добавления контента
    newhtml, // сырой контент
    list, // селектор элементов списка, нужен для подсчета количества и инициализации GUI
    regexpCount, // паттерн для подстановки количества
    regexpNum //  паттерн для подстановки номера
)
{

    if(!regexpCount)
        regexpCount=/#COUNT#/gim;

    if(!regexpNum)
        regexpNum=/#NUM#/gim;


    jQuery(AddBlock).on('click',function(){
        var html=newhtml;

        if(list)
        {
            var count=jQuery(list).length;
            count+=1;
            html=html.replace(regexpCount,count);
        }

        var addhtml=$(html);
        jQuery(PrevBlock).before(addhtml);
        if(list)
        {
            var l=$(list);
            if(l.length>0)
            {
                $(l[l.length-1]).find('.niceselect').niceSelect();
            }
        }
    });
}


jQuery(document).ready(function() {

    jQuery('.contact-page').on('click','.delformat',function() {
        var parent=jQuery(this).parents('.wrapper_brickformat').remove();
    });

    jQuery('.contact-page').on('change','select.brickformat',function() {
        var
            format,
            color,
            brand,
            value = $(this).val(),
            wrapper=$(this).parents('.wrapper_brickformat'),
            brickformatFormat=wrapper.find('.brickformat-format'),
            brickformatBrand=wrapper.find('select.brickformat-brand'),
            brickformatColor=wrapper.find('.brickformat-color')
        ;
        if(value>0){
            format=$(this).find('option:selected').data('format');
            color=$(this).find('option:selected').data('color');
            brand=$(this).find('option:selected').data('brand');
        }else{
            format="";
            color="";
            brand="";
        }
        brickformatFormat.val(format);
        brickformatColor.val(color);
        brickformatBrand.find('option[value='+brand+']').prop('selected', true);
        brickformatBrand.niceSelect('update');
    });

    jQuery('.contact-page').on('change','select.brickformat-brand',function() {
        var
            format,
            color,
            brand,
            value = $(this).val(),
            wrapper=$(this).parents('.wrapper_brickformat'),
            brickformat=wrapper.find('select.brickformat'),
            brickformatFormat=wrapper.find('.brickformat-format'),
            brickformatBrand=wrapper.find('.brickformat-brand'),
            brickformatColor=wrapper.find('.brickformat-color')
        ;
        brickformat.empty();
        brickformat.append($('<option value="">Выберите</option>'));
        for(i=0;i<BrickFormat.length;i++){
            if(parseInt(value) && value>0 && value!=BrickFormat[i].brand)
                continue;
            var option = $('<option>' + BrickFormat[i].name + '</option>');
            option.attr('value', BrickFormat[i].value);
            option.attr('data-format', BrickFormat[i].format);
            option.attr('data-color', BrickFormat[i].color);
            option.attr('data-brand', BrickFormat[i].brand);
            brickformat.append(option);
        }
        brickformat.niceSelect('update');
        brickformatFormat.val("");
        brickformatColor.val("");
    });

    jQuery('select#addressregionid').on('change',function() {
        var
            value = $(this).val()
        ;

        jQuery('select#addressareaid').empty();
        jQuery('select#addressareaid').append($('<option value="">Выберите</option>'));
        jQuery('select#addressareaid').niceSelect('update');

        if(value.length>0){
            $.ajax({
                url: '/ajax/DistrictGetListByIdRegion.php',
                dataType: 'json',
                type:'POST',
                data: {
                    'parentId':value
                },
                success: function (data){
                    for(i=0;i<data.length;i++){
                        var option = $('<option>' + data[i].name + ' ' + data[i].typeShort + '</option>');
                        option.attr('value', data[i].id);
                        jQuery('select#addressareaid').append(option);
                    }
                    jQuery('select#addressareaid').niceSelect('update');
                }
            });
        }


        jQuery('select#addresscityid').empty();
        jQuery('select#addresscityid').append($('<option value="">Выберите</option>'));
        jQuery('select#addresscityid').niceSelect('update');

        if(value.length>0){
            $.ajax({
                url: '/ajax/CityGetListByIdRegion.php',
                dataType: 'json',
                type:'POST',
                data: {
                    'parentId':value
                },
                success: function (data){
                    for(i=0;i<data.length;i++){
                        var option = $('<option>' + data[i].name + ' ' + data[i].typeShort + '</option>');
                        option.attr('value', data[i].id);
                        jQuery('select#addresscityid').append(option);
                    }
                    jQuery('select#addresscityid').niceSelect('update');
                }
            });
        }


    });

    jQuery('select#addressareaid').on('change',function() {
        var
            value = $(this).val()
        ;



        jQuery('select#addresscityid').empty();
        jQuery('select#addresscityid').append($('<option value="">Выберите</option>'));
        jQuery('select#addresscityid').niceSelect('update');
        if(value.length>0){
            $.ajax({
                url: '/ajax/CityGetListByIdDistrict.php',
                dataType: 'json',
                type:'POST',
                data: {
                    'parentId':value
                },
                success: function (data){
                    for(i=0;i<data.length;i++){
                        var option = $('<option>' + data[i].name + ' ' + data[i].typeShort + '</option>');
                        option.attr('value', data[i].id);
                        jQuery('select#addresscityid').append(option);
                    }
                    jQuery('select#addresscityid').niceSelect('update');
                }
            });
        }else{
            value = $("select#addressregionid").val();
            if(value.length>0){
                $.ajax({
                    url: '/ajax/CityGetListByIdRegion.php',
                    dataType: 'json',
                    type:'POST',
                    data: {
                        'parentId':value
                    },
                    success: function (data){
                        for(i=0;i<data.length;i++){
                            var option = $('<option>' + data[i].name + ' ' + data[i].typeShort + '</option>');
                            option.attr('value', data[i].id);
                            jQuery('select#addresscityid').append(option);
                        }
                        jQuery('select#addresscityid').niceSelect('update');
                    }
                });
            }
        }
    });

    jQuery('button.add-file').click(function() {
        var file = $('<input type="file" name="photo[]" />');
        $(this).before(file);
        file.wrap('<div></div>').styler();
    });

});

