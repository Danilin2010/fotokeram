<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$Burl = $arParams["FOLDER"] . $arParams["URL_TEMPLATES"]["detail"];
$Burl = str_replace("#ELEMENT_ID#", 'add', $Burl);

use GuzzleHttp\Client as GuzzleClient;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use Vdmkbu\Geocoder\Types\Address;
use Vdmkbu\Geocoder\Types\Point;
use Vdmkbu\Geocoder\YandexGeocoder;
use Bitrix\Main\Config\Option;

?>
<? $this->SetViewTarget('afterh1'); ?>
<div class="filter_wrap">
    <div class="jumbotron gallery_jumbo">
        <div class="row div-1520">
            <h1>Фотогалерея</h1>
        </div>
    </div>
    <div class="filter_btn">
        <a href="<?= $Burl ?>" class="flaticon-add btn-add delay-1s a_button_block"></a>
    </div>
</div>
<? $this->EndViewTarget(); ?>
<form action="<?= POST_FORM_ACTION_URI ?>" enctype="multipart/form-data" method="post" id="objectprops"
      class="form_reg contact-page">
    <div class="obj-props-form">

        <div class="col-md-6">
            <div class="parameters">
                <span class="title">Параметры объекта</span>

                <? if ($arResult["input"]["id"]) { ?>
                    <div class="cont-column-full">
                        <label>ID</label>
                        <div class="form-control"><?= $arResult["input"]["id"] ?></div>
                    </div>
                    <div class="cont-column-full">
                        <label>Название</label>
                        <div class="form-control"><?= $arResult["input"]["name"] ?></div>
                    </div>
                <? } ?>

                <label for="seamcolor">Цвет шва</label>
                <select id="seamcolor" class="niceselect" name="seamcolor">
                    <option></option>
                    <? foreach ($arResult["SeamColor"] as $brand) { ?>
                        <option <? if ($brand->getId() == $arResult["input"]["seamcolor"]){ ?>selected<? } ?>
                                value="<?= $brand->getId(); ?>"><?= $brand->getName(); ?></option>
                    <? } ?>
                </select>

                <label for="objecttype">Тип объекта</label>
                <select id="objecttype" class="niceselect" name="objecttype">
                    <option></option>
                    <? foreach ($arResult["ObjectType"] as $brand) { ?>
                        <option <? if ($brand->getId() == $arResult["input"]["objecttype"]){ ?>selected<? } ?>
                                value="<?= $brand->getId(); ?>"><?= $brand->getName(); ?></option>
                    <? } ?>
                </select>

                <label for="yearlist">Год</label>
                <select id="yearlist" class="niceselect year" name="yearlist">
                    <option></option>
                    <? foreach ($arResult["YearList"] as $brand) { ?>
                        <option <? if ($brand->getId() == $arResult["input"]["year"]){ ?>selected<? } ?>
                                value="<?= $brand->getId(); ?>"><?= $brand->getValue(); ?></option>
                    <? } ?>
                </select>

                <label class="container-check">Микс
                    <input <? if ($arResult["input"]["mix"] == "Y"){ ?>checked<? } ?> name="mix" value="Y"
                           type="checkbox">
                    <span class="checkmark"></span>
                </label>
            </div>


            <div class="parameters">
                <span class="title margin-lower">Фотографии объекта</span>

                <div class="input-file-container">
                    <p class="file-return">Файл не выбран</p>
                    <input name="photo[]" class="input-file" id="my-file" type="file">
                    <label tabindex="0" for="my-file" class="input-file-trigger">Обзор...</label>
                </div>

                <span class="addmorefile">Добавить еще файл</span>
            </div>

        </div>
        <div class="col-md-6">
            <div class="parameters colored">
                <span class="title">Адрес объекта</span>
                <? $address = []; ?>
                <label for="addressregionid">Регион</label>
                <select name="address[regionid]" id="addressregionid" class="niceselect">
                    <option value="">Выберите</option>
                    <? foreach ($arResult["Address"]["Region"] as $brand) { ?>
                        <option
                            <? if ($brand->getKladr() == $arResult["input"]['address']["regionid"]){ ?>selected<? } ?>
                            value="<?= $brand->getKladr(); ?>"><?= $brand->getName(); ?></option>
                        <?
                        if ($brand->getKladr() == $arResult["input"]['address']["regionid"]) {
                            $address[] = $brand->getName();
                        }
                        ?>
                    <? } ?>
                </select>

                <label for="addressareaid">Район</label>
                <select name="address[areaid]" id="addressareaid" class="niceselect region">
                    <option value="">Выберите</option>
                    <? foreach ($arResult["Address"]["District"] as $brand) { ?>
                        <option
                            <? if ($brand->getId() == $arResult["input"]['address']["areaid"]){ ?>selected<? } ?>
                            value="<?= $brand->getId(); ?>"><?= $brand->getName(); ?> <?= $brand->getTypeShort(); ?></option>
                        <?
                        if ($brand->getId() == $arResult["input"]['address']["areaid"]) {
                            $address[] = $brand->getName() . ' ' . $brand->getTypeShort();
                        }
                        ?>
                    <? } ?>
                </select>

                <label for="addresscityid">Город</label>
                <select name="address[cityid]" id="addresscityid" class="niceselect city">
                    <option value="">Выберите</option>
                    <? foreach ($arResult["Address"]["City"] as $brand) { ?>
                        <option
                            <? if ($brand->getId() == $arResult["input"]['address']["cityid"]){ ?>selected<? } ?>
                            value="<?= $brand->getId(); ?>"><?= $brand->getName(); ?> <?= $brand->getTypeShort(); ?></option>
                        <?
                        if ($brand->getId() == $arResult["input"]['address']["cityid"]) {
                            $address[] = $brand->getName() . ' ' . $brand->getTypeShort();
                        }
                        ?>
                    <? } ?>
                </select>

                <?
                $yandexMapApiKey = Option::get('fileman', 'yandex_map_api_key', '');
                if (strlen($yandexMapApiKey) >= 0) {
                    ?>


                    <label for="addresscityid1">Координаты</label>


                    <div id="map" style="width: 100%; height: 400px"></div>
                <?

                $coords1 = $arResult["input"]['address']["coords1"];
                $coords2 = $arResult["input"]['address']["coords2"];

                if (strlen($coords1) <= 0 || strlen($coords2) <= 0) {
                    $address = implode($address, ", ");

                    $config = [];
                    $guzzle = new GuzzleClient($config);
                    $client = new GuzzleAdapter($guzzle);
                    $api_key = $yandexMapApiKey;
                    $geocoder = new YandexGeocoder($client, $api_key);
                    $point = $geocoder->geocode(new Address($address));

                    $coords2 = $point->getLat();
                    $coords1 = $point->getLng();

                }

                if (strlen($coords1) <= 0 || strlen($coords2) <= 0) {
                    $coords1 = 55.76;
                    $coords2 = 37.64;
                }

                ?>
                    <input name="address[coords1]" id="coords1" type="hidden" value="<?= $coords1 ?>"/>
                    <input name="address[coords2]" id="coords2" type="hidden" value="<?= $coords2 ?>"/>

                    <script type="text/javascript">
                        ymaps.ready(init);
                        var marker;

                        function setCoord(coords) {
                            $('#coords1').val(coords[0].toPrecision(6));
                            $('#coords2').val(coords[1].toPrecision(6));
                            marker.geometry.setCoordinates(coords);
                        }

                        function init() {
                            var map = new ymaps.Map("map", {
                                center: [<?=$coords1?>, <?=$coords2?>],
                                controls: [],
                                zoom: 12
                            });
                            marker = new ymaps.GeoObject({
                                geometry: {
                                    type: "Point",
                                    coordinates: [<?=$coords1?>, <?=$coords2?>]
                                },
                                properties: {}
                            }, {
                                draggable: true
                            });
                            var searchControl = new ymaps.control.SearchControl({
                                options: {
                                    noPlacemark: true
                                }
                            });
                            map.geoObjects.add(marker);
                            map.controls.add(searchControl);
                            map.events.add('click', function (e) {
                                var coords = e.get('coords');
                                setCoord(coords);
                            });
                            marker.events.add("dragend", function (result) {
                                var coordinates = this.geometry.getCoordinates();
                                setCoord(coordinates);
                            }, marker);
                            searchControl.events.add('resultselect', function (e) {
                                var index = e.get('index');
                                searchControl.getResult(index).then(function (res) {
                                    setCoord(res.geometry.getCoordinates());
                                });
                            }).add('submit', function () {

                            })
                        }
                    </script>

                    <?
                }
                ?>


            </div>
        </div>

    </div>

    <? if (count($arResult["Photo"]) > 0) { ?>
        <div class="photo-container row div-1520">
            <? foreach ($arResult["Photo"] as $photo) { ?>
                <div class="col-md-5th images">
                    <div class="obj-photo-wrap">
                        <?
                        $PhotoId = $photo->getPhotoId();
                        if ($PhotoId > 0) {
                            $arFileTmp = CFile::ResizeImageGet(
                                $PhotoId,
                                array("width" => 250, "height" => 300),
                                BX_RESIZE_IMAGE_EXACT,
                                true//,GetWatermark()
                            );
                        }
                        ?>
                        <div class="image-wrap">
                            <a data-fancybox="gallery" href="<?= $arFileTmp["src"] ?>"><img
                                        src="<?= $arFileTmp["src"] ?>">
                                <div class="overlay flaticon-zoom-in"></div>
                            </a>
                        </div>
                        <label class="container-check">Удалить
                            <input name="delPhoto[]" value="<?= $photo->getId() ?>" type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            <? } ?>
        </div>
    <? } ?>

    <div class="clearfix"></div>


    <script>
        var BrickFormat = [
            <?foreach ($arResult["BrickFormat"] as $key=>$brand){?>
            {
                format: "<?=$brand->getFormat()->getName();?>",
                color: "<?=$brand->getColor()->getName();?>",
                brand: <?=$brand->getBrand()->getId();?>,
                value: <?=$brand->getId();?>,
                name: "<?=$brand->getName();?> (<?=$brand->getVendor();?>)"
            }<?if($key < count($arResult["BrickFormat"]))?>,<?}?>
        ];
    </script>

    <?

    $count = count($arResult["input"]["BrickFormat"]);
    if ($count == 0)
        $count = 1;
    ?>

    <div id="bricks">

        <? for ($i = 0; $i < $count; $i++) { ?>
            <? if ($i != 0) { ?><span class="delthis flaticon-delete"></span><? } ?>
            <div class="brick_wrap wrapper_brickformat">
                <p class="title">Кирпич <?= $i + 1 ?></p>

                <div class="row col-md-12">

                    <div class="col-md-3">
                        <label>Бренд</label>
                        <select name="BrickFormatBrand[]" class="niceselect brickformat-brand">
                            <option>Выберите</option>
                            <? foreach ($arResult["Brand"] as $brand) { ?>
                                <option
                                    <? if ($brand->getId() == $arResult["input"]["BrickFormatBrand"][$i]){ ?>selected<? } ?>
                                    value="<?= $brand->getId(); ?>"><?= $brand->getName(); ?></option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <label>Название (артикул)</label>
                        <select name="BrickFormat[]" class="niceselect brickformat" <? if ($i == 0){ ?>required<? } ?>>
                            <option>Выберите</option>
                            <? foreach ($arResult["BrickFormat"] as $brand) { ?>
                                <? if ($arResult["input"]["BrickFormatBrand"][$i] && $brand->getBrand()->getId() != $arResult["input"]["BrickFormatBrand"][$i]) {
                                    continue;
                                } ?>
                                <option
                                    <? if ($brand->getId() == $arResult["input"]["BrickFormat"][$i]){ ?>selected<? } ?>
                                    data-format="<?= $brand->getFormat()->getName(); ?>"
                                    data-color="<?= $brand->getColor()->getName(); ?>"
                                    data-brand="<?= $brand->getBrand()->getId(); ?>"
                                    value="<?= $brand->getId(); ?>"><?= $brand->getName(); ?>
                                    (<?= $brand->getVendor(); ?>)
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <label>Формат кирпича</label>
                        <input type="text" class="brickformat-format" disabled
                               value="<?= $arResult["input"]["BrickFormatFormat"][$i] ?>">
                    </div>
                    <div class="col-md-3">
                        <label>Цвет кирпича</label>
                        <input type="text" class="brickformat-color" disabled
                               value="<?= $arResult["input"]["BrickFormatColor"][$i] ?>">
                    </div>
                </div>

            </div>
        <? } ?>

        <span id="addformat" class="addmorefile">Добавить еще кирпич</span>


        <script>
            AddContentForm('#addformat', '#addformat', '' +
                '<span class="delthis flaticon-delete"></span>' +
                '<div class="brick_wrap wrapper_brickformat" >' +
                '   <p class="title">Кирпич #COUNT#</p>' +
                '   <div class = "row col-md-12">' +
                '       <div class="col-md-3">' +
                '           <label>Бренд</label>' +
                '               <select name="BrickFormatBrand[]" class="niceselect brickformat-brand">' +
                '                   <option>Выберите</option>' +
                '                   <?foreach ($arResult["Brand"] as $brand){?>\n' +
                '                   <option value="<?=$brand->getId();?>"><?=$brand->getName();?></option>\n' +
                '                   <?}?>\n' +
                '               </select>' +
                '       </div>' +
                '       <div class="col-md-3">' +
                '           <label>Название (артикул)</label>' +
                '           <select name="BrickFormat[]" class="niceselect brickformat" <?if($i == 0){?>required<?}?>>' +
                '               <option>Выберите</option>' +
                '               <?foreach ($arResult["BrickFormat"] as $brand){?>\n' +
                '               <option\n' +
                '                   data-format="<?=$brand->getFormat()->getName();?>"\n' +
                '                   data-color="<?=$brand->getColor()->getName();?>"\n' +
                '                   data-brand="<?=$brand->getBrand()->getId();?>"\n' +
                '                   value="<?=$brand->getId();?>"><?=$brand->getName();?> (<?=$brand->getVendor();?>)</option>\n' +
                '               <?}?>\n' +
                '           </select>' +
                '       </div>' +
                '       <div class="col-md-3">' +
                '           <label>Формат кирпича</label>' +
                '           <input type="text" class="brickformat-format" disabled value="">' +
                '       </div>' +
                '       <div class="col-md-3">' +
                '           <label>Цвет кирпича</label>' +
                '           <input type="text" class="brickformat-color" disabled value="">' +
                '       </div>' +
                '   </div>' +
                '</div>'
                , '.wrapper_brickformat');
        </script>

    </div>

    <div class="obj-props-form cont-column-full">
        <input name="save" type="submit" value="Сохранить" class="button">
    </div>

    <p>&nbsp;</p>

</form>