<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?global $USER;?>
<?$this->SetViewTarget('headerfilter'); ?>
<?$APPLICATION->IncludeComponent(
    "fotokeram:photo.gallery.filter",
    "min",
    Array(
        "USER"=>$USER->GetID()
    )
);?>
<?$this->EndViewTarget(); ?>
<?$this->SetViewTarget('headerfilterbutton'); ?>
<a class="navbar-brand" href="/" role="banner">
    <img src="<?=SITE_TEMPLATE_PATH?>/img/logo.svg">
</a>
<button class="showfilter">показать фильтр</button>
<?$this->EndViewTarget(); ?>
<?$APPLICATION->IncludeComponent(
    "fotokeram:photo.gallery.filter",
    "min",
    Array(
        "USER"=>$USER->GetID()
    )
);?>

<?$APPLICATION->IncludeComponent(
    "fotokeram:photo.gallery.personal.list",
    "",
    Array(
        "FOLDER" => $arResult["FOLDER"],
        "URL_TEMPLATES" => $arResult["URL_TEMPLATES"],
    )
);?>
