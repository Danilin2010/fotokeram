<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?use local\Helpers\Watermark;?>
<?//echo "<pre>";print_r($arResult);echo "</pre>"?>
<div class="photo-container row div-1520">
    <?foreach ($arResult["photo"] as $photo){
        if(count($Object=$photo->getObject())<=0){
            //echo "<pre>";print_r($photo);echo "</pre>";
            continue;
        }

        $Object=$photo->getObject()[0];
        $url=$arParams["FOLDER"].$arParams["URL_TEMPLATES"]["detail"];
        $url=str_replace("#ELEMENT_ID#",$Object->getId(),$url);
        $UserId=$Object->getAuthor();
        $Watermark=Watermark::GetWatermark();
        ?>
        <?if($photo->getPhotoId()>0){
            $arFileTmp = CFile::ResizeImageGet(
                $photo->getPhotoId(),
                array("width" => 250, "height" => 250),
                BX_RESIZE_IMAGE_EXACT,
                true,$Watermark
            );

            $arFileTmp2 = CFile::ResizeImageGet(
                $photo->getPhotoId(),
                array("width" => 1000, "height" => 1000),
                BX_RESIZE_IMAGE_EXACT,
                true,$Watermark
            );
        }?>
        <?
        $bricks=$Object->getBrick();
        $arrName=[];
        $arrSimpleName=[];
        $arrBrand=[];
        foreach($bricks as $brick){
            $arrName[]=$brick->getName().' ('.$brick->getVendor().')';
            $arrSimpleName[]=$brick->getName();
        }
        foreach($bricks as $brick){
            $arrBrand[]=$brick->getBrand()->getName();
        }
        ?>
        <div class="col-md-5th images">
            <div class="obj-photo-wrap">
                <?if($photo->getPhotoId()>0){?>
                    <div class="image-wrap">
                        <a
                                class="fancybox"
                                data-photocart="<?=$photo->getId();?>"
                                data-url="<?=$url?>"
                                data-title-id="title-<?=$photo->getPhotoId()?>"
                                data-photo-id="<?=$photo->getId()?>"
                                data-fancybox="gallery"
                                data-use-user-group="<?=$arParams["USE_USER_GROUP"]?>"
                                href="<?=$arFileTmp2["src"]?>"><img src="<?=$arFileTmp["src"]?>">
                            <div class="overlay flaticon-zoom-in"></div>
                        </a>
                        <div id="title-<?=$photo->getPhotoId()?>" class="hidden">
                            <?=implode(', ',array_unique($arrName))?>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                <?}?>
                <?if($arParams["USE_USER_GROUP"]){?><label class="container-check">Выбрать
                    <input type="checkbox"
                           data-photocart="<?=$photo->getId();?>" value="<?=$photo->getId();?>" name="PhotoCart[]"
                           <?if(in_array($photo->getId(),$arParams["PHOTOCART"])){?>checked<?}?>
                    />
                    <span class="checkmark"></span>
                </label><?}?>
                <div class="description-poles">
                    <span>Название: <b><?=implode(', ',array_unique($arrSimpleName))?></b></span>
                    <?/*<span>Название объекта: <b><?=implode(', ',$arrName)?></b></span>*/?>
                    <span>Бренд: <b><?=implode(', ',array_unique($arrBrand))?></b></span>
                    <span>Регион: <b><?=$Object->getAddress()->getRegion()?></b></span>
                </div>
            </div>
        </div>
    <?}?>
    <?
    $APPLICATION->IncludeComponent(
        "bitrix:main.pagenavigation",
        "catalog",
        array(
            "NAV_OBJECT" => $arResult["nav"],
            //"SEF_MODE" => "Y",
        ),
        $component
    );
    ?>
</div>







