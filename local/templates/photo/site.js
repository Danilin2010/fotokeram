function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}
$(document).ready(function () {
    $('.div-1480 .navbar-nav li:nth-child(1)').addClass('animated fadeInLeft');
    $('.div-1480 .navbar-nav li:nth-child(2)').addClass('animated fadeInRight');
    $('.div-1480 .navbar-nav li:nth-child(3)').addClass('animated fadeInLeft');
    $('.flaticon-add.btn-add').addClass('animated bounce');
    $('.niceselect').niceSelect();
    document.querySelector("html").classList.add('js');

    var fileInput = document.querySelector(".input-file"),
        button = document.querySelector(".input-file-trigger"),
        the_return = document.querySelector(".file-return");
    if(button!==null){
        button.addEventListener("keydown", function (event) {
            if (event.keyCode == 13 || event.keyCode == 32) {
                fileInput.focus();
            }
        });
        button.addEventListener("click", function (event) {
            fileInput.focus();
            return false;
        });
        fileInput.addEventListener("change", function (event) {
            var vls = this.value.split('\\');
            the_return.innerHTML = vls[2];
        });
    }

    var $win = $(window),
        $fixed = $(".header_fixed"),
        limit = 185;

    function tgl(state) {
        $fixed.toggleClass("hidden", state);
    }

    $win.on("scroll", function () {
        var top = $win.scrollTop();
        if (top < limit) {
            tgl(true);
        } else {
            tgl(false);
        }
    });
});

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

$(document).on('click', '.showfilter', function () {
    if ($('.header_fixed .filter_wrap').is(':hidden') === true) {
        $('.showfilter').text('скрыть фильтр');
    } else {
        $('.showfilter').text('показать фильтр');
    }
    $('.header_fixed .filter_wrap').slideToggle();
});

$(document).on('click', '.parameters .addmorefile', function () {
    var blocks = 0;
    $('.input-file-container').each(function () {
        blocks++;
    });
    $(this).prev().after('<span class = "delthis flaticon-delete"></span><div class="input-file-container">' + replaceAll($(this).prev().html(), 'my-file', 'my-file' + blocks) + '</div>');
});

/*$(document).on('click', '#bricks .addmorefile', function () {
    var blocks = 0;
    $('.brick_wrap').each(function () {
        blocks++;
    });
    var cnt = parseInt(blocks)+1;
    $(this).prev().after('<span class = "delthis flaticon-delete"></span><div class="brick_wrap">' + $(this).prev().html().replace('Кирпич '+blocks, 'Кирпич ' + cnt) + '</div>');
});*/

$(document).on('click', '.delthis', function () {
    $(this).next().remove();
    $(this).remove();
});