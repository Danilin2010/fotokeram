jQuery(document).ready(function() {
    jQuery(document).on('change','input[data-photocart]',function (e) {
        var
            checked=$(this).prop('checked'),
            photocart=$(this).data('photocart'),
            _this=this,
            input=$('input[data-photocart='+photocart+']').not(this),
            url='/ajax/DelPhotoCart.php'
        ;

        input.prop('checked',checked);

        if(checked)
            url='/ajax/AddPhotoCart.php';

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: {
                id: photocart
            },
            success: function (data) {
                jQuery('.photocartnum').text(data.count);
                jQuery('.photocartnumvisible').show();
                jQuery('.photocartnumhidden').hide();
                if(data.success){

                }else{
                    $(_this).prop('checked',false);
                    input.prop('checked',false);
                }
            }
        });

    });
});

jQuery(document).ready(function() {
    jQuery('.fancybox').fancybox({
        caption : function( instance, item ) {
            var caption='';
            var el,
                element=$(this),
                id=element.data('title-id'),
                useUserGroup=element.data('useUserGroup'),
                photocart=element.data('photocart'),
                url=element.data('url')
            ;


            if (id) {
                el = $('#' + id);
                if (el.length) {
                    var input=$('input[data-photocart='+photocart+']');
                    var checked=input.prop('checked');
                    var checkedtext='';
                    if(checked)
                        checkedtext=' checked ';
                    caption = caption + el.html();
                    if(useUserGroup){
                        caption+=' <label class="container-check">Выбрать<input '+checkedtext+' data-photocart="'+photocart+'" type="checkbox" value="'+photocart+'" name="PhotoCart[]"/><span class="checkmark"></span></label>';
                    }

                }
            }

            return caption;
        },
        beforeShow:function(instance,current){
            var id=current.opts.$orig.data('photo-id');
            $.ajax({
                url: '/ajax/AddCounter.php',
                dataType: 'json',
                type: 'POST',
                data: {
                    id: id
                },
                success: function (data) {

                }
            });
        },
    });
});