<?php
/**
 * @global \CMain $APPLICATION
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
</main>
<footer role="contentinfo" class="footer">
    <nav class="navbar navbar-expand-md navbar-dark" role="navigation">
        <div class="div-1480 row">
            <div class="col-md-3 no-left-padding logo">
                <a class="navbar-brand" href="/" role="banner">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/logo_footer.svg">
                </a>
            </div>
            <div class="col-md-5 copy">
                <p>Сайт носит исключительно информационный характер и ни при каких условиях не является публичной
                    офертой. Для получения дополнительной информации пишите на WhatsApp +79181315665</p>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsDefault"
                    aria-controls="navbarsDefault" aria-expanded="false" aria-label="Переключить навигацию">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="col-md-4 collapse navbar-collapse no-left-right-padding" id="navbarsDefault">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "top",
                    array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "top",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "top",
                        "USE_EXT" => "N",
                    ),
                    false
                );?>
            </div>
        </div>
    </nav>
</footer>
<div class="loader"></div>
</body>
</html>