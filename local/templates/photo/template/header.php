<?php
/**
 * @global \CMain $APPLICATION
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Page\Asset;
use \Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

$asset = Asset::getInstance();

?><!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php $APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->ShowHead()?>
    <?
    $asset->addCss(SITE_TEMPLATE_PATH."/css/bootstrap.min.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/font/flaticon.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/nice-select.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/animate.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/style.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/jquery.fancybox.min.css");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery-3.4.1.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/popper.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/bootstrap.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.nice-select.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.fancybox.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/site.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/cart.js");
    $arrUser=local\Services\UserServices::getUserProp();

    $yandexMapApiKey = Option::get('fileman', 'yandex_map_api_key', '');

    if(strlen($yandexMapApiKey)>=0){
        $http = (\CMain::IsHTTPS()) ? "https://" : "http://";
        $asset->addJs($http."api-maps.yandex.ru/2.1/?apikey=$yandexMapApiKey&lang=ru_RU");
    }

    ?>
</head>
<body>
<? $APPLICATION->ShowPanel() ?>

<div class="header_fixed hidden">
    <div class="div-1480 row">
        <div class="col-md-8 no-left-padding">
            <?$APPLICATION->ShowViewContent('headerfilterbutton');?>
        </div>
        <div class="col-md-4 mobail_frame">
            <div class="mobail_frame_name"><?=$arrUser["NAME"]?></div>
            <div class="mobail_frame_cart">
            <?$APPLICATION->IncludeComponent(
                "fotokeram:photo.cart",
                "mobail",
                Array()
            );?>
            </div>
        </div>
    </div>
    <?$APPLICATION->ShowViewContent('headerfilter');?>
</div>
<header class="header">
    <div class="top_line">
        <?if($arrUser["NAME"]){?><div class="left">
            <span>Пользователь: </span><?=$arrUser["NAME"]?>
        </div><?}?>
        <div class="right">
            <span>На портале </span><?=local\Services\CountMedia::getObject()?> объектов и <?=local\Services\CountMedia::getPhoto()?> фотографий
        </div>
    </div>
    <nav class="navbar navbar-expand-md navbar-dark" role="navigation">
        <div class="div-1480 row">
            <div class="col-md-3 no-left-padding logo">
                <a class="navbar-brand" href="/" role="banner">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/logo.svg">
                </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsDefault"
                    aria-controls="navbarsDefault" aria-expanded="false" aria-label="Переключить навигацию">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse no-left-right-padding" id="navbarsDefault">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "top",
                    array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "top",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "top",
                        "USE_EXT" => "N",
                    ),
                    false
                );?>
            </div>
            <div class="checked_photo_wrap">
                <div class="checked_photo_frame">
                    <?if(USE_USER_GROUP){?>
                        <?$APPLICATION->IncludeComponent(
                            "fotokeram:photo.cart",
                            "",
                            Array()
                        );?>
                    <?}?>
                </div>
            </div>
        </div>
    </nav>
</header>
<main role="main">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"person", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "top",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "person"
	),
	false
);?>
<? $APPLICATION->ShowViewContent('afterh1'); ?>
