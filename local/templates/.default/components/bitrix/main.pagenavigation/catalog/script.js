$(document).ready(function(){
    $('[data-loadmore] a').on('click',function(){
        var url=$(this).attr('href');
        var next=$(this).attr('data-next');
        var _this=this;
        $.ajax({
                url:url,
                success:function(result){
                    var html=$('<div>'+result+'</div>');
                    var item=$('[data-item]',html);
                    $('[data-prev-item]').before(item);
                    var index=$('[data-loadmore] a',html);
                    if(index.length>0){
                        var url=index.attr('href');
                        var next=index.attr('data-next');
                        $(_this).attr('href',url).attr('data-next',next);
                    }else{
                        $(_this).parent('[data-loadmore]').remove();
                    }
                }
            }
        );
        return false;
    });
});
