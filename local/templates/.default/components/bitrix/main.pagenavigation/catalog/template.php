<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["PAGE_COUNT"]>1){?>
    <?if($arResult["CURRENT_PAGE"]!=$arResult["END_PAGE"]){?>
        <div class="loadmore" data-loadmore><a href="<?=str_replace('--page--',$arResult["CURRENT_PAGE"]+1,$arResult["URL_TEMPLATE"])?>"
                                 data-next="<?=$arResult["END_PAGE"]?>"><i class="flaticon-curvedarrow2"></i>Показать еще</a></div>
    <?}?>
<?}?>
